#!/bin/bash

set echo off
# This shell script file install the driver for the connection to the CGI DB with the Sistema CAEPP.
mytitle="Instalando OJDBC version 7.0 ..."
echo -e '\033]2;'$mytitle'\007'

echo "Iniciando los servicios ..."
read -p "Presione [Enter] key para instalar el driver ..."
clear
# Section 1 Execute Maven to install the Driver.
mvn install:install-file -Dfile=./'oracle_JDBC'/ojdbc7.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0 -Dpackaging=jar
read -p "Presione [Enter] key para terminar ..."