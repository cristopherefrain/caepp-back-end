@echo off
:: This batch file install the driver for the connection to the CGI DB with the Sistema CAEPP.
title Instalando OJDBC version 7.0 ...

echo Iniciando los servicios ...
pause
cls
:: Section 1 Execute Maven to install the Driver.
mvn install:install-file -Dfile=.\oracle_JDBC\ojdbc7.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0 -Dpackaging=jar
pause