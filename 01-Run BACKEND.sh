#!/bin/bash

set echo off
# This batch file starts Caepp_Backend.
mytitle="Levantando BACKEND ..."
echo -e '\033]2;'$mytitle'\007' 

echo "Iniciando los servicios ..."
read -p "Presione [Enter] key para iniciar el backend ..."
clear
# Section 1 Execute Maven to cleand the packages and run Spring Boot.
# mvn clean package spring-boot:run
mvn spring-boot:run