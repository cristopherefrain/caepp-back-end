## Restful APIs para el Sistema CAEPP

###### Summary
Restful services en Spring Boot para sistema de Comunicado de ajustes del estudiantado del programa de psicopedagogía.

###### Prerequisites
JDK 11
Maven 3.6.2
oracle 12c

###### Setup
Windows
- Ejecutar archivo .bat 00-Install Driver para agregar el driver de oracle al proyecto (Solo si no esta instalado).
Linux
- Ejecutar archivo .sh 00-Install Driver para agregar el driver de oracle al proyecto (Solo si no esta instalado).
###### Run
Windows
- Ejecutar archivo .bat 01-Run BACKEND para ejecutar maven y spring boot.
Linux
- Ejecutar archivo .sh 01-Run BACKEND para ejecutar maven y spring boot.
Despues de la ejecucion el servidor correra en localhost:9898 

###### Testing (Postman)
- Abrir Postman importar la coleccion con ctrl+O y soltar el archivo `Restful API's Testing and Debugging.postman_collection`.