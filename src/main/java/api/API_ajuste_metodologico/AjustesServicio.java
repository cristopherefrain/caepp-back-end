package api.API_ajuste_metodologico;

import api.detalle_ajuste_metodologico.AjusteServicio;
import api.detalle_ajuste_metodologico.Ajuste_Metodologico;
import api.detalle_formulario_inscripcion.FormularioServicio;
import api.detalle_formulario_inscripcion.Formulario_Inscripcion;
import api.estudiante.Estudiante;
import api.estudiante.EstudianteServicio;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase AjustesServicio
public class AjustesServicio {

// Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    FormularioServicio formularioService;
    @Autowired
    EstudianteServicio estudianteService;
    @Autowired
    AjusteServicio ajusteService;

//===================================================   SISTEMA CAEPP   ======================================================
    public Formulario_Inscripcion getFormularioPorIdentificacion_CAEPP_Ajustes(String idenEstudiante) throws Exception {
        final int spridenEstudiante = estudianteService.getSpridenPidmEstudiante_CAEPP(idenEstudiante);

        final CompletableFuture<Formulario_Inscripcion> formularioAsync = formularioService.getFormularioPorSpridenPIDM_CAEPP(spridenEstudiante);
        final CompletableFuture<Estudiante> estudianteASYNC = estudianteService.getEstudiantePorSpridenPIDM_CAEPP(spridenEstudiante);
        final CompletableFuture<HashMap<Integer, Ajuste_Metodologico>> ajustesASYNC = ajusteService.getDetalleAjustes_CAEPP(spridenEstudiante);

        CompletableFuture.allOf(formularioAsync, estudianteASYNC, ajustesASYNC).join();

        final Formulario_Inscripcion formulario = formularioAsync.get();
        final Estudiante estudiante = estudianteASYNC.get();

        formulario.setSpriden_pidm(spridenEstudiante);
        formulario.setEstudiante(estudiante);
        formulario.setDetalle_ajustes_metodologicos(ajustesASYNC.get());
        return formulario;
    }

    // CRUD Ajustes_Metodologicos
    public Boolean agregarFormulario_CAEPP_Ajustes(Formulario_Inscripcion formulario) throws Exception {
        CompletableFuture<Boolean> addAjustes = ajusteService.agregarDetalleAjustes_CAEPP(formulario.getDetalle_ajustes_metodologicos());
        return addAjustes.get();
    }

    public Boolean actualizarFormulario_CAEPP_Ajustes(Formulario_Inscripcion formulario) throws Exception {
        CompletableFuture<Boolean> updateAjustes = ajusteService.actualizarDetalleAjustes_CAEPP(formulario.getDetalle_ajustes_metodologicos());
        return updateAjustes.get();
    }

    public Boolean eliminarFormularioPorIdentificacion_CAEPP_Ajustes(String idenEstudiante) throws Exception {
        CompletableFuture<Boolean> deleteAjustes = ajusteService.eliminarDetalleAjustesPorIdentificacion_CAEPP(idenEstudiante);
        return deleteAjustes.get();
    }
//===================================================   SISTEMA CAEPP   ======================================================
}
// Fin clase AjustesServicio
