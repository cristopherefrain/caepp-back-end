package api.API_ajuste_metodologico;

import api.detalle_formulario_inscripcion.Formulario_Inscripcion;
import static api.modelos.ConstantesGlobales.ACTUALIZAR;
import static api.modelos.ConstantesGlobales.CARGAR;
import static api.modelos.ConstantesGlobales.GUARDAR;
import static api.modelos.ConstantesGlobales.INFORME;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

//Anotacion @RestController indica a Spring Boot que la clase es un controlador web que maneja los Request HTTP
@RestController
@RequestMapping("/ajustes")
//Inicio clase AjustesControlador
public class AjustesControlador {
    
//Anotacion @Autowire indica a Spring Boot que debe inyectar una intancia cuando crea el controlador
    @Autowired
    AjustesServicio ajustesService;
//===================================================   SISTEMA CAEPP   ======================================================
    // url: http://localhost:9898/ajustes/caepp/{idenEstudiante}
    //Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /{idenEstudiante} al metodo getFormularioPorIdentificacion_JSON

    @RequestMapping(method = RequestMethod.GET, value = "/caepp/{idenEstudiante}")
    public Formulario_Inscripcion getFormularioPorIdentificacion_JSON_CAEPP_Ajustes(@PathVariable String idenEstudiante) {
        try {
            if (idenEstudiante != null && !idenEstudiante.isBlank()) {
                Formulario_Inscripcion formulario = ajustesService.getFormularioPorIdentificacion_CAEPP_Ajustes(idenEstudiante);
                if (formulario != null && !formulario.objetoIncompleto(INFORME, CARGAR)) {
                    return formulario;
                }
            }
            throw new Exception("Identificacion vacio");
        } catch (Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }

//CRUD Ajustes_Metodologicos
    // url: http://localhost:9898/ajustes/caepp
    //Anotacion @RequestMapping mapea el metodo HTTP.POST al metodo agregarFormulario_JSON
    @RequestMapping(method = RequestMethod.POST, value = "/caepp")
    public void agregarFormulario_JSON_CAEPP_Ajustes(@RequestBody Formulario_Inscripcion formulario) {
        try {
            if (formulario == null) {
                throw new Exception("Objeto null");
            }
            if (formulario != null && !formulario.objetoIncompleto(INFORME, GUARDAR)) {
                ajustesService.agregarFormulario_CAEPP_Ajustes(formulario);
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/ajustes/caepp
    //Anotacion @RequestMapping mapea el metodo HTTP.PUT al metodo actualizarFormulario_JSON
    @RequestMapping(method = RequestMethod.PUT, value = "/caepp")
    public void actualizarFormulario_JSON_CAEPP_Ajustes(@RequestBody Formulario_Inscripcion formulario) {
        try {
            if (formulario == null) {
                throw new Exception("Objeto null");
            }
            if (!formulario.objetoIncompleto(INFORME, ACTUALIZAR)) {
                ajustesService.actualizarFormulario_CAEPP_Ajustes(formulario);
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/ajustes/caepp/{idenEstudiante}
    //Anotacion @RequestMapping mapea el metodo HTTP.DELETE al metodo eliminarFormulario_JSON_CAEPP
    @RequestMapping(method = RequestMethod.DELETE, value = "/caepp/{idenEstudiante}")
    public void eliminarFormulario_JSON_CAEPP_Ajustes(@PathVariable String idenEstudiante) {
        try {
            if (idenEstudiante == null || idenEstudiante.isBlank()) {
                throw new Exception("Identificacion vacio");
            }
            ajustesService.eliminarFormularioPorIdentificacion_CAEPP_Ajustes(idenEstudiante);
        } catch (Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }
//===================================================   SISTEMA CAEPP   ======================================================
}
//Fin clase AjustesControlador
