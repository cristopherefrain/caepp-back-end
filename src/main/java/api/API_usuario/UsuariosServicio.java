package api.API_usuario;

import api.funcionario.Funcionario;
import api.funcionario.FuncionarioServicio;
import api.usuario.Usuario;
import api.usuario.UsuarioServicio;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase UsuariosServicio
public final class UsuariosServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    UsuarioServicio usuarioService;
    @Autowired
    FuncionarioServicio funcionarioService;

    // =========================================== CENTRO DE GESTION INFORMATICA ==============================================
    public Usuario getUsuarioPorIdentificacion_CGI(final String idenUsuario) throws Exception {
        final CompletableFuture<Funcionario> funcionarioASYNC = funcionarioService.getFuncionarioPorIdentificacion_CGI(idenUsuario);
        final Usuario usuario = new Usuario();
        Funcionario funcionario = funcionarioASYNC.get();
        usuario.setFuncionario(funcionario);
        usuario.setSpriden_pidm(funcionario.getSpriden_pidm());
        usuario.setIdentificacion(funcionario.getIdentificacion());
        return usuario;
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public List<Usuario> getAllUsuarios_CAEPP() throws Exception {
        final List<Usuario> usuarios = usuarioService.getAllUsuarios_CAEPP().get();
        CompletableFuture<Funcionario> funcionarioASYNC;
        for (final Usuario user : usuarios) {
            funcionarioASYNC = funcionarioService
                    .getFuncionarioPorIdentificacion_CGI(user.getIdentificacion());
            user.setFuncionario(funcionarioASYNC.get());
        }
        return usuarios;
    }

    public List<Usuario> getUsuariosFiltro_CAEPP(final String idenUsuario) throws Exception {
        final List<Usuario> usuarios = usuarioService.getUsuariosFiltro_CAEPP(idenUsuario).get();
        CompletableFuture<Funcionario> funcionarioASYNC;
        for (final Usuario user : usuarios) {
            funcionarioASYNC = funcionarioService
                    .getFuncionarioPorIdentificacion_CGI(user.getIdentificacion());
            user.setFuncionario(funcionarioASYNC.get());
        }
        return usuarios;
    }

    public Usuario getUsuarioPorIdentificacion_CAEPP(final String idenUsuario) throws Exception {
        final CompletableFuture<Usuario> usuarioASYNC = usuarioService.getUsuarioPorIdentificacion_CAEPP(idenUsuario);
        final CompletableFuture<Funcionario> funcionarioASYNC = funcionarioService.getFuncionarioPorIdentificacion_CGI(idenUsuario);

        CompletableFuture.allOf(usuarioASYNC, funcionarioASYNC).join();

        final Usuario usuario = usuarioASYNC.get();
        usuario.setFuncionario(funcionarioASYNC.get());
        return usuario;
    }

    // CRUD Usuario
    public Boolean agregarUsuario_CAEPP(final Usuario usuario) throws Exception {
        return usuarioService.agregarUsuario_CAEPP(usuario).get();
    }

    public Boolean actualizarUsuario_CAEPP(final Usuario usuario) throws Exception {
        return usuarioService.actualizarUsuario_CAEPP(usuario).get();
    }

    public Boolean eliminarUsuarioPorIdentificacion_CAEPP(final String idenUsuario) throws Exception {
        return usuarioService.eliminarUsuarioPorIdentificacion_CAEPP(idenUsuario).get();
    }

    // =================================================== SISTEMA CAEPP ======================================================
    public void purgar_db() throws Exception {
        usuarioService.purgar_db();
    }
}
// Fin clase UsuariosServicio
