package api.API_usuario;

import api.usuario.Usuario;
import static api.modelos.ConstantesGlobales.CARGAR;
import static api.modelos.ConstantesGlobales.CREAR;
import static api.modelos.ConstantesGlobales.GUARDAR;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

//Anotacion @RestController indica a Spring Boot que la clase es un controlador web que maneja los Request HTTP
@RestController
@RequestMapping("/usuario")
//Inicio clase UsuarioControlador
public final class UsuariosControlador {

//Anotacion @Autowire indica a Spring Boot que debe inyectar una intancia cuando crea el controlador
    @Autowired
    UsuariosServicio usuariosService;

//===========================================   CENTRO DE GESTION INFORMATICA   ==============================================
    // url: http://localhost:9898/usuario/cgi/{idenUsuario}
    //Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /{idenUsuario} al metodo getUsuarioPorIdentificacion_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/cgi/{idenUsuario}")
    public Usuario getUsuarioPorIdentificacion_JSON_CGI(@PathVariable final String idenUsuario) {
        try {
            if (idenUsuario != null && !idenUsuario.isBlank()) {
                final Usuario usuario = usuariosService.getUsuarioPorIdentificacion_CGI(idenUsuario);
                if (usuario != null && !usuario.objetoIncompleto(CREAR)) {
                    return usuario;
                }
            }
            throw new Exception("idenUsuario vacio");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    // url: http://localhost:9898/usuario/caepp/getAll
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getAll al
    // metodo getAllUsuarios_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/getAll")
    public List<Usuario> getAllUsuarios_JSON_CAEPP() {
        try {
            final List<Usuario> usuarios = usuariosService.getAllUsuarios_CAEPP();
            return usuarios;
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/usuario/caepp/getFilter?idenUsuario=
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getFilter al
    // metodo getUsuariosFiltro_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/getFilter")
    public List<Usuario> getUsuariosFiltro_JSON_CAEPP(@RequestParam final String idenUsuario) {
        try {
            if (idenUsuario != null && !idenUsuario.isBlank()) {
                final List<Usuario> usuarios = usuariosService.getUsuariosFiltro_CAEPP(idenUsuario);
                return usuarios;
            }
            throw new Exception("Identificacion vacio");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/usuario/caepp/{idenUsuario}
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /{idenUsuario}
    // al metodo getUsuarioPorIdentificacion_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/{idenUsuario}")
    public Usuario getUsuarioPorIdentificacion_JSON_CAEPP(@PathVariable final String idenUsuario) {
        try {
            if (idenUsuario != null && !idenUsuario.isBlank()) {
                final Usuario usuario = usuariosService.getUsuarioPorIdentificacion_CAEPP(idenUsuario);
                if (usuario != null && !usuario.objetoIncompleto(CARGAR)) {
                    return usuario;
                }
            }
            throw new Exception("idenUsuario vacio");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }

    // CRUD Usuario
    // url: http://localhost:9898/usuario/caepp
    // Anotacion @RequestMapping mapea el metodo HTTP.POST al metodo
    // agregarUsuario_JSON
    @RequestMapping(method = RequestMethod.POST, value = "/caepp")
    public void agregarUsuario_JSON(@RequestBody final Usuario usuario) {
        try {
            if (usuario == null) {
                throw new Exception("Objeto null");
            }
            if (!usuario.objetoIncompleto(GUARDAR)) {
                usuariosService.agregarUsuario_CAEPP(usuario);
            }
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/usuario/caepp
    // Anotacion @RequestMapping mapea el metodo HTTP.PUT al metodo
    // actualizarUsuario_JSON
    @RequestMapping(method = RequestMethod.PUT, value = "/caepp")
    public void actualizarUsuario_JSON(@RequestBody final Usuario usuario) {
        try {
            if (usuario == null) {
                throw new Exception("Objeto null");
            }
            if (!usuario.objetoIncompleto(GUARDAR)) {
                usuariosService.actualizarUsuario_CAEPP(usuario);
            }
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/usuario/caepp/{idenUsuario}
    // Anotacion @RequestMapping mapea el metodo HTTP.DELETE al metodo
    // eliminarUsuario_JSON
    @RequestMapping(method = RequestMethod.DELETE, value = "/caepp/{idenUsuario}")
    public void eliminarUsuario_JSON(@PathVariable final String idenUsuario) {
        try {
            if (idenUsuario == null || idenUsuario.isBlank()) {
                throw new Exception("Identificacion vacio");
            }
            usuariosService.eliminarUsuarioPorIdentificacion_CAEPP(idenUsuario);
        } catch (final Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }
    //===================================================   SISTEMA CAEPP   ======================================================
}
//Fin clase UsuarioControlador
