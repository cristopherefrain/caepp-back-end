package api.detalle_formulario_inscripcion;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase FormularioServicio
public class FormularioServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    FormularioRepositorio formularioRepo;
    Formulario_Inscripcion modelo;

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    @Async
    public CompletableFuture<Formulario_Inscripcion> getFormularioPorSpridenPIDM_CGI(final int spridenEstudiante) throws Exception {
        final Formulario_Inscripcion formulario = formularioRepo.getFormularioPorSpridenPIDM_CGI(spridenEstudiante);
        return CompletableFuture.completedFuture(formulario);
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    @Async
    public CompletableFuture<List<Formulario_Inscripcion>> getAllFormularios_CAEPP() throws Exception {
        final List<Formulario_Inscripcion> formularios = formularioRepo.getAllFormularios_CAEPP();
        return CompletableFuture.completedFuture(formularios);
    }

    @Async
    public CompletableFuture<List<Formulario_Inscripcion>> getFormulariosFiltro_CAEPP(final String idenEstudiante) throws Exception {
        final List<Formulario_Inscripcion> formularios = formularioRepo.getFormulariosFiltro_CAEPP(idenEstudiante);
        return CompletableFuture.completedFuture(formularios);
    }

    @Async
    public CompletableFuture<Formulario_Inscripcion> getFormularioPorSpridenPIDM_CAEPP(final int spridenEstudiante) throws Exception {
        modelo = formularioRepo.getFormularioPorSpriden_PIDM_CAEPP(spridenEstudiante);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Formulario_Inscripcion
    @Async
    public CompletableFuture<Boolean> agregarFormulario_CAEPP(final Formulario_Inscripcion formulario) throws Exception {
        final Boolean var = formularioRepo.agregarFormulario_CAEPP(formulario);
        return CompletableFuture.completedFuture(var);
    }

    @Async
    public CompletableFuture<Boolean> actualizarFormulario_CAEPP(final Formulario_Inscripcion formulario) throws Exception {
        Boolean var = true;
        if (modelo == null || !modelo.equals(formulario)) {
            modelo = formulario;
            var = formularioRepo.actualizarFormulario_CAEPP(formulario);
        }
        return CompletableFuture.completedFuture(var);
    }

    @Async
    public CompletableFuture<Boolean> eliminarFormularioPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        modelo = null;
        final Boolean var = formularioRepo.eliminarFormularioPorIdentificacion_CAEPP(idenEstudiante);
        return CompletableFuture.completedFuture(var);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase FormularioServicio
