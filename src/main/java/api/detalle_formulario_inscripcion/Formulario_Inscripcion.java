package api.detalle_formulario_inscripcion;

import api.detalle_ajuste_metodologico.Ajuste_Metodologico;
import api.detalle_correo.Correo_Electronico;
import api.detalle_direccion.Direccion_Geografica;
import api.detalle_equipo_apoyo.Equipo_Apoyo;
import api.detalle_telefono.Telefono;
import api.estudiante.Estudiante;
import static api.modelos.ConstantesGlobales.ACTUALIZAR;
import static api.modelos.ConstantesGlobales.CARGAR;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static api.modelos.ConstantesGlobales.CREAR;
import static api.modelos.ConstantesGlobales.EQUIPOS;
import static api.modelos.ConstantesGlobales.GUARDAR;
import static api.modelos.ConstantesGlobales.INFORME;
import static api.modelos.ConstantesGlobales.INSCRIPCION;
import java.util.HashMap;
import java.util.Objects;

//Inicio clase Formulario_Inscripcion
public final class Formulario_Inscripcion {

    // Atributos
    private int id_formulario_inscripcion;
    private int spriden_pidm;
    private Date fecha_inscripcion;
    private Date fecha_ingreso_una;
    private String campus;
    private String carrera;
    private String tipo_beca;
    private int id_tipo_estado;
    private int id_tipo_nivel;
    private int id_tipo_ciclo;

    private String titulo_tipo_estado;
    private String titulo_tipo_nivel;
    private String titulo_tipo_ciclo;

    private Estudiante estudiante;
    private HashMap<Integer, Ajuste_Metodologico> detalle_ajustes_metodologicos;
    private HashMap<Integer, Equipo_Apoyo> detalle_equipos_apoyo;
    private HashMap<Integer, Telefono> detalle_telefonos;
    private HashMap<Integer, Correo_Electronico> detalle_correos;
    private HashMap<Integer, Direccion_Geografica> detalle_direcciones;

    public Formulario_Inscripcion() {
        this.id_formulario_inscripcion = 0;
        this.spriden_pidm = 0;
        this.fecha_inscripcion = Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        this.fecha_ingreso_una = Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        this.campus = "";
        this.carrera = "";
        this.tipo_beca = "";
        this.id_tipo_estado = 0;
        this.id_tipo_nivel = 0;
        this.id_tipo_ciclo = 0;

        this.titulo_tipo_estado = "";
        this.titulo_tipo_nivel = "";
        this.titulo_tipo_ciclo = "";

        this.estudiante = new Estudiante();
        this.detalle_ajustes_metodologicos = new HashMap<>();
        this.detalle_equipos_apoyo = new HashMap<>();
        this.detalle_telefonos = new HashMap<>();
        this.detalle_correos = new HashMap<>();
        this.detalle_direcciones = new HashMap<>();
    }

    public Formulario_Inscripcion(final int id_formulario_inscripcion, final int spriden_pidm, final Date fecha_inscripcion, final Date fecha_ingreso_una, final String campus, final String carrera, final String tipo_beca,
            final int id_tipo_estado, final int id_tipo_nivel, final int id_tipo_ciclo,
            final Estudiante estudiante,
            final HashMap<Integer, Ajuste_Metodologico> detalle_ajustes_metodologicos, final HashMap<Integer, Equipo_Apoyo> detalle_equipos_apoyo, final HashMap<Integer, Telefono> detalle_telefonos, final HashMap<Integer, Correo_Electronico> detalle_correos, final HashMap<Integer, Direccion_Geografica> detalle_direcciones) {
        this.id_formulario_inscripcion = id_formulario_inscripcion;
        this.spriden_pidm = spriden_pidm;
        this.fecha_inscripcion = fecha_inscripcion;
        this.fecha_ingreso_una = fecha_ingreso_una;
        this.campus = campus;
        this.carrera = carrera;
        this.tipo_beca = tipo_beca;
        this.id_tipo_estado = id_tipo_estado;
        this.id_tipo_nivel = id_tipo_nivel;
        this.id_tipo_ciclo = id_tipo_ciclo;

        this.titulo_tipo_estado = "";
        this.titulo_tipo_nivel = "";
        this.titulo_tipo_ciclo = "";

        this.estudiante = estudiante;
        this.detalle_ajustes_metodologicos = detalle_ajustes_metodologicos;
        this.detalle_equipos_apoyo = detalle_equipos_apoyo;
        this.detalle_telefonos = detalle_telefonos;
        this.detalle_correos = detalle_correos;
        this.detalle_direcciones = detalle_direcciones;
    }

    public Formulario_Inscripcion(final int id_formulario_inscripcion, final int spriden_pidm, final Date fecha_inscripcion, final Date fecha_ingreso_una, final String campus, final String carrera, final String tipo_beca,
            final int id_tipo_estado, final int id_tipo_nivel, final int id_tipo_ciclo) {
        this.id_formulario_inscripcion = id_formulario_inscripcion;
        this.spriden_pidm = spriden_pidm;
        this.fecha_inscripcion = fecha_inscripcion;
        this.fecha_ingreso_una = fecha_ingreso_una;
        this.campus = campus;
        this.carrera = carrera;
        this.tipo_beca = tipo_beca;
        this.id_tipo_estado = id_tipo_estado;
        this.id_tipo_nivel = id_tipo_nivel;
        this.id_tipo_ciclo = id_tipo_ciclo;

        this.titulo_tipo_estado = "";
        this.titulo_tipo_nivel = "";
        this.titulo_tipo_ciclo = "";

        this.estudiante = new Estudiante();
        this.detalle_ajustes_metodologicos = new HashMap<>();
        this.detalle_equipos_apoyo = new HashMap<>();
        this.detalle_telefonos = new HashMap<>();
        this.detalle_correos = new HashMap<>();
        this.detalle_direcciones = new HashMap<>();
    }

    public void setId_formulario_inscripcion(final int id_formulario_inscripcion) {
        this.id_formulario_inscripcion = id_formulario_inscripcion;
    }

    public void setSpriden_pidm(final int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public void setFecha_inscripcion(final Date fecha_inscripcion) {
        this.fecha_inscripcion = fecha_inscripcion;
    }

    public void setFecha_ingreso_una(final Date fecha_ingreso_una) {
        this.fecha_ingreso_una = fecha_ingreso_una;
    }

    public void setCampus(final String campus) {
        this.campus = campus;
    }

    public void setCarrera(final String carrera) {
        this.carrera = carrera;
    }

    public void setTipo_beca(final String tipo_beca) {
        this.tipo_beca = tipo_beca;
    }

    public void setId_tipo_estado(final int id_tipo_estado) {
        this.id_tipo_estado = id_tipo_estado;
    }

    public void setId_tipo_nivel(final int id_tipo_nivel) {
        this.id_tipo_nivel = id_tipo_nivel;
    }

    public void setId_tipo_ciclo(final int id_tipo_ciclo) {
        this.id_tipo_ciclo = id_tipo_ciclo;
    }

    public void setTitulo_tipo_estado(String titulo_tipo_estado) {
        this.titulo_tipo_estado = titulo_tipo_estado;
    }

    public void setTitulo_tipo_nivel(String titulo_tipo_nivel) {
        this.titulo_tipo_nivel = titulo_tipo_nivel;
    }

    public void setTitulo_tipo_ciclo(String titulo_tipo_ciclo) {
        this.titulo_tipo_ciclo = titulo_tipo_ciclo;
    }

    public void setEstudiante(final Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public void setDetalle_ajustes_metodologicos(
            final HashMap<Integer, Ajuste_Metodologico> detalle_ajustes_metodologicos) {
        this.detalle_ajustes_metodologicos = detalle_ajustes_metodologicos;
    }

    public void setDetalle_equipos_apoyo(final HashMap<Integer, Equipo_Apoyo> detalle_equipos_apoyo) {
        this.detalle_equipos_apoyo = detalle_equipos_apoyo;
    }

    public void setDetalle_telefonos(final HashMap<Integer, Telefono> detalle_telefonos) {
        this.detalle_telefonos = detalle_telefonos;
    }

    public void setDetalle_correos(final HashMap<Integer, Correo_Electronico> detalle_correos) {
        this.detalle_correos = detalle_correos;
    }

    public void setDetalle_direcciones(final HashMap<Integer, Direccion_Geografica> detalle_direcciones) {
        this.detalle_direcciones = detalle_direcciones;
    }

    public int getId_formulario_inscripcion() {
        return id_formulario_inscripcion;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public Date getFecha_inscripcion() {
        return fecha_inscripcion;
    }

    public Date getFecha_ingreso_una() {
        return fecha_ingreso_una;
    }

    public String getCampus() {
        return campus;
    }

    public String getCarrera() {
        return carrera;
    }

    public String getTipo_beca() {
        return tipo_beca;
    }

    public int getId_tipo_estado() {
        return id_tipo_estado;
    }

    public int getId_tipo_nivel() {
        return id_tipo_nivel;
    }

    public int getId_tipo_ciclo() {
        return id_tipo_ciclo;
    }

    public String getTitulo_tipo_estado() {
        return titulo_tipo_estado;
    }

    public String getTitulo_tipo_nivel() {
        return titulo_tipo_nivel;
    }

    public String getTitulo_tipo_ciclo() {
        return titulo_tipo_ciclo;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public HashMap<Integer, Ajuste_Metodologico> getDetalle_ajustes_metodologicos() {
        return detalle_ajustes_metodologicos;
    }

    public HashMap<Integer, Equipo_Apoyo> getDetalle_equipos_apoyo() {
        return detalle_equipos_apoyo;
    }

    public HashMap<Integer, Telefono> getDetalle_telefonos() {
        return detalle_telefonos;
    }

    public HashMap<Integer, Correo_Electronico> getDetalle_correos() {
        return detalle_correos;
    }

    public HashMap<Integer, Direccion_Geografica> getDetalle_direcciones() {
        return detalle_direcciones;
    }

    @Override
    public String toString() {
        return "Formulario_Inscripcion{" + "id_formulario_inscripcion=" + id_formulario_inscripcion + ", spriden_pidm=" + spriden_pidm + ", fecha_inscripcion=" + fecha_inscripcion + ", fecha_ingreso_una=" + fecha_ingreso_una + ", campus=" + campus + ", carrera=" + carrera + ", tipo_beca=" + tipo_beca + ", id_tipo_estado=" + id_tipo_estado + ", id_tipo_nivel=" + id_tipo_nivel + ", id_tipo_ciclo=" + id_tipo_ciclo + ", titulo_tipo_estado=" + titulo_tipo_estado + ", titulo_tipo_nivel=" + titulo_tipo_nivel + ", titulo_tipo_ciclo=" + titulo_tipo_ciclo + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + this.id_formulario_inscripcion;
        hash = 61 * hash + Objects.hashCode(this.fecha_inscripcion);
        hash = 61 * hash + Objects.hashCode(this.fecha_ingreso_una);
        hash = 61 * hash + Objects.hashCode(this.campus);
        hash = 61 * hash + Objects.hashCode(this.carrera);
        hash = 61 * hash + Objects.hashCode(this.tipo_beca);
        hash = 61 * hash + this.id_tipo_estado;
        hash = 61 * hash + this.id_tipo_nivel;
        hash = 61 * hash + this.id_tipo_ciclo;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Formulario_Inscripcion other = (Formulario_Inscripcion) obj;
        if (this.id_formulario_inscripcion != other.id_formulario_inscripcion) {
            return false;
        }
        if (this.spriden_pidm != other.spriden_pidm) {
            return false;
        }
        if (this.id_tipo_nivel != other.id_tipo_nivel) {
            return false;
        }
        if (this.id_tipo_ciclo != other.id_tipo_ciclo) {
            return false;
        }
        if (!Objects.equals(this.campus, other.campus)) {
            return false;
        }
        if (!Objects.equals(this.carrera, other.carrera)) {
            return false;
        }
        if (!Objects.equals(this.tipo_beca, other.tipo_beca)) {
            return false;
        }
        // if (!Objects.equals(this.fecha_inscripcion, other.fecha_inscripcion)) {
        // return false;
        // }
        // if (!Objects.equals(this.fecha_ingreso_una, other.fecha_ingreso_una)) {
        // return false;
        // }
        return true;
    }

    public Boolean objetoIncompleto(final int modo, final int opcion) throws Exception {
        switch (opcion) {
            case ACTUALIZAR: {
                if (modo == INFORME) {
                    if (detalle_ajustes_metodologicos.isEmpty()) {
                        throw new Exception("detalle_ajustes_metodologicos vacio");
                    }
                }
                if (modo == EQUIPOS) {
                    if (detalle_equipos_apoyo.isEmpty()) {
                        throw new Exception("detalle_ajustes_metodologicos vacio");
                    }
                }
            }
            case CARGAR: {
                if (id_formulario_inscripcion == 0) {
                    throw new Exception("id_formulario_inscripcion default");
                }
            }
            case GUARDAR: {
                if (fecha_inscripcion
                        .after(Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))) {
                    throw new Exception("fecha_inscripcion default");
                }
                if (id_tipo_estado == 0) {
                    throw new Exception("id_tipo_estado default");
                }
                if (id_tipo_nivel == 0) {
                    throw new Exception("id_tipo_nivel default");
                }
                if (id_tipo_ciclo == 0) {
                    throw new Exception("id_tipo_ciclo default");
                }
            }
            case CREAR: {
                if (spriden_pidm == 0) {
                    throw new Exception("spriden_pidm default");
                }
                if (fecha_ingreso_una
                        .after(Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))) {
                    throw new Exception("fecha_ingreso_una default");
                }
                if (campus.isBlank()) {
                    throw new Exception("campus default");
                }
                if (carrera.isBlank()) {
                    throw new Exception("carrera default");
                }
                if (tipo_beca.isBlank()) {
                    throw new Exception("tipo_beca default");
                }
                estudiante.objetoIncompleto();
                if (modo == INSCRIPCION) {
                    if (detalle_telefonos.isEmpty()) {
                        throw new Exception("detalle_telefonos vacio");
                    }
                    if (detalle_correos.isEmpty()) {
                        throw new Exception("detalle_correos vacio");
                    }
                    if (detalle_direcciones.isEmpty()) {
                        throw new Exception("detalle_direcciones vacio");
                    }
                }
            }
            break;
            default:
                return true;
        }
        return false;
    }

}
// Fin clase Formulario_Inscripcion
