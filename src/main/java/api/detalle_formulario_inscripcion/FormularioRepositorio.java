package api.detalle_formulario_inscripcion;

import api.modelos.Repositorio_Principal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion 
//(Capa de Persistencia)
@Repository
// Inicio clase FormularioRepositorio
public class FormularioRepositorio extends Repositorio_Principal {

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    private Formulario_Inscripcion cargaCampus_AñoIngreso_CGI(final int spridenEstudiante) throws Exception {
        String Query = "SELECT " + "(SELECT MIN(SGBSTDN_TERM_CODE_ADMIT) "
                + "FROM SGBSTDN a, STVCAMP WHERE a.SGBSTDN_PIDM = %d "
                + "AND a.SGBSTDN_TERM_CODE_ADMIT = (SELECT MIN(SGBSTDN_TERM_CODE_ADMIT) FROM SGBSTDN b WHERE a.SGBSTDN_PIDM = b.SGBSTDN_PIDM) "
                + "AND STVCAMP_CODE = a.SGBSTDN_CAMP_CODE) as FECHA_INGRESO_UNA," + "STVCAMP_CODE, STVCAMP_DESC CAMPUS "
                + "FROM SGBSTDN a, STVCAMP " + "WHERE a.SGBSTDN_PIDM = %d " + "AND SGBSTDN_STST_CODE = 'AS' "
                + "AND a.SGBSTDN_TERM_CODE_EFF = " + "(SELECT MAX(SGBSTDN_TERM_CODE_EFF) " + "FROM SGBSTDN b "
                + "WHERE a.SGBSTDN_PIDM = b.SGBSTDN_PIDM AND a.SGBSTDN_STST_CODE = 'AS') "
                + "AND STVCAMP_CODE = a.SGBSTDN_CAMP_CODE";
        Query = String.format(Query, spridenEstudiante, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final Formulario_Inscripcion formulario = new Formulario_Inscripcion();
        if (rowset.next()) {
            String campus = null;
            Integer año_ingreso = null;
            try {
                campus = rowset.getString("CAMPUS");
                año_ingreso = rowset.getInt("FECHA_INGRESO_UNA");
            } catch (final Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
            }
            validarCampo(campus, "CAMPUS");
            validarCampo(año_ingreso, "FECHA_INGRESO_UNA");

            formulario.setCampus(campus.toUpperCase());
            final String strDate = año_ingreso / 100 + "-02-01"; // Devuelve solo el año de ingreso en formato xxxx00 ->
            // 201600
            final Date fecha_ingreso = java.sql.Date.valueOf(strDate);
            formulario.setFecha_ingreso_una(fecha_ingreso);
        }
        return formulario;
    }

    private String cargarCarrera_CGI(final int spridenEstudiante) throws Exception {
        String Query = "select SOVLCUR_PROGRAM CARRERA, SOVLCUR_CAMP_CODE CAMPUS " + "from sovlcur a "
                + "where (a.sovlcur_key_seqno is null or a.sovlcur_key_seqno = 99) "
                + "and ((a.sovlcur_cact_code = 'ACTIVE') or (a.sovlcur_cact_code    = 'ACTIVO')) "
                + "and  a.sovlcur_current_ind = 'Y' " + "and  a.sovlcur_program    != 'NO-DECLAR' "
                + "and  a.sovlcur_pidm        =  %d " + "and  a.sovlcur_lmod_code   = 'LEARNER'";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        String carrera = null;
        if (rowset.next()) {
            try {
                carrera = rowset.getString("CARRERA");
            } catch (final Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
            }
            validarCampo(carrera, "CARRERA");
        }
        return carrera.toUpperCase();
    }

    private String cargarTipoBeca_CGI(final int spridenEstudiante) throws Exception { // REVIZAR
        String Query = "SELECT NULL FROM DUAL"; // select de Beca ? :v condenado Jorge tsss
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        String tipo_beca = null;
        if (rowset.next()) {
            // tipo_beca= rowset.getString("BECA");
            tipo_beca = "N.A";
        }
        validarCampo(tipo_beca, "BECA");
        return tipo_beca.toUpperCase();
    }

    public Formulario_Inscripcion getFormularioPorSpridenPIDM_CGI(final int spridenEstudiante) throws Exception {
        final Formulario_Inscripcion formulario = cargaCampus_AñoIngreso_CGI(spridenEstudiante);
        formulario.setCarrera(cargarCarrera_CGI(spridenEstudiante));
        formulario.setTipo_beca(cargarTipoBeca_CGI(spridenEstudiante));
        return formulario;
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    // Metodo para construir un objeto Formulario_Inscripcion apartir de una tupla
    // de la base de datos
    private Formulario_Inscripcion crearFormulario_CAEPP(final CachedRowSet rowset) throws Exception {
        Integer id_formulario_inscripcion = null;
        Integer spriden_pidm = null;
        Date fecha_inscripcion = null;
        Date fecha_ingreso_una = null;
        String campus = null;
        String carrera = null;
        String tipo_beca = null;
        Integer id_tipo_estado = null;
        Integer id_tipo_nivel = null;
        Integer id_tipo_ciclo = null;

        String titulo_tipo_estado = null;
        String titulo_tipo_nivel = null;
        String titulo_tipo_ciclo = null;
        try {
            id_formulario_inscripcion = rowset.getInt("ID_FORMULARIO_INSCRIPCION");
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            fecha_inscripcion = rowset.getDate("FECHA_INSCRIPCION");
            fecha_ingreso_una = rowset.getDate("FECHA_INGRESO_UNA");
            campus = rowset.getString("CAMPUS");
            carrera = rowset.getString("CARRERA");
            tipo_beca = rowset.getString("TIPO_BECA");
            id_tipo_estado = rowset.getInt("ID_TIPO_ESTADO");
            id_tipo_nivel = rowset.getInt("ID_TIPO_NIVEL");
            id_tipo_ciclo = rowset.getInt("ID_TIPO_CICLO");

            titulo_tipo_estado = rowset.getString("DESCRIPCION_ESTADO");
            titulo_tipo_nivel = rowset.getString("DESCRIPCION_NIVEL");
            titulo_tipo_ciclo = rowset.getString("DESCRIPCION_CICLO");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        validarCampo(id_formulario_inscripcion, "ID_FORMULARIO_INSCRIPCION");
        validarCampo(spriden_pidm, "SPRIDEN_PIDM");
        validarCampo(fecha_inscripcion, "FECHA_INSCRIPCION");
        validarCampo(fecha_ingreso_una, "FECHA_INGRESO_UNA");
        validarCampo(campus, "CAMPUS");
        validarCampo(carrera, "CARRERA");
        validarCampo(tipo_beca, "TIPO_BECA");
        validarCampo(id_tipo_estado, "ID_TIPO_ESTADO");
        validarCampo(id_tipo_nivel, "DESCRIPCION_NIVEL");
        validarCampo(id_tipo_ciclo, "ID_TIPO_CICLO");

        validarCampo(id_tipo_estado, "DESCRIPCION_ESTADO");
        validarCampo(id_tipo_nivel, "DESCRIPCION_NIVEL");
        validarCampo(id_tipo_ciclo, "DESCRIPCION_CICLO");

        final Formulario_Inscripcion formulario = new Formulario_Inscripcion(id_formulario_inscripcion, spriden_pidm, fecha_inscripcion, fecha_ingreso_una, campus, carrera, tipo_beca,
                id_tipo_estado, id_tipo_nivel, id_tipo_ciclo);
        formulario.setTitulo_tipo_estado(titulo_tipo_estado);
        formulario.setTitulo_tipo_nivel(titulo_tipo_nivel);
        formulario.setTitulo_tipo_ciclo(titulo_tipo_ciclo);
        return formulario;
    }

    public List<Formulario_Inscripcion> getAllFormularios_CAEPP() throws Exception {
        String Query = "SELECT ID_FORMULARIO_INSCRIPCION, SPRIDEN_PIDM, FECHA_INSCRIPCION, FECHA_INGRESO_UNA, CAMPUS,CARRERA, TIPO_BECA, "
                + "ID_ESTUDIANTE, ID_TIPO_ESTADO, ID_TIPO_NIVEL, ID_TIPO_CICLO "
                + ",DESCRIPCION_ESTADO, DESCRIPCION_NIVEL, DESCRIPCION_CICLO "
                + "FROM FORMULARIO_INSCRIPCION "
                + "NATURAL JOIN TIPO_ESTADO NATURAL JOIN TIPO_NIVEL NATURAL JOIN TIPO_CICLO ";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var formularios = new ArrayList<Formulario_Inscripcion>();
        Formulario_Inscripcion formulario = null;
        while (rowset.next()) {
            formulario = crearFormulario_CAEPP(rowset);
            formularios.add(formulario);
        }
        return formularios;
    }

    public List<Formulario_Inscripcion> getFormulariosFiltro_CAEPP(final String idenEstudiante) throws Exception {
        String Query = "SELECT ID_FORMULARIO_INSCRIPCION, SPRIDEN_PIDM, FECHA_INSCRIPCION, FECHA_INGRESO_UNA, CAMPUS,CARRERA, TIPO_BECA,"
                + " ID_ESTUDIANTE, ID_TIPO_ESTADO, ID_TIPO_NIVEL, ID_TIPO_CICLO "
                + "NATURAL JOIN TIPO_ESTADO NATURAL JOIN TIPO_NIVEL NATURAL JOIN TIPO_CICLO "
                + "WHERE IDENTIFICACION LIKE '%s%%' ";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var formularios = new ArrayList<Formulario_Inscripcion>();
        Formulario_Inscripcion formulario = null;
        while (rowset.next()) {
            formulario = crearFormulario_CAEPP(rowset);
            formularios.add(formulario);
        }
        return formularios;
    }

    public Formulario_Inscripcion getFormularioPorSpriden_PIDM_CAEPP(final int spridenEstudiante) throws Exception {
        String Query = "SELECT ID_FORMULARIO_INSCRIPCION, SPRIDEN_PIDM, FECHA_INSCRIPCION, FECHA_INGRESO_UNA, CAMPUS, CARRERA, TIPO_BECA,"
                + "ID_ESTUDIANTE, ID_TIPO_ESTADO, ID_TIPO_NIVEL, ID_TIPO_CICLO "
                + ",DESCRIPCION_ESTADO, DESCRIPCION_NIVEL, DESCRIPCION_CICLO "
                + "FROM FORMULARIO_INSCRIPCION "
                + "NATURAL JOIN TIPO_ESTADO NATURAL JOIN TIPO_NIVEL NATURAL JOIN TIPO_CICLO "
                + "WHERE SPRIDEN_PIDM = %d ";
        // + "AND ESTADO = 'A'";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        Formulario_Inscripcion formulario = null;
        if (rowset.next()) {
            formulario = crearFormulario_CAEPP(rowset);
        }
        if (formulario == null) {
            throw new Exception("No existe un Formulario asociado con el SPRIDEN_PIDM " + spridenEstudiante);
        }
        return formulario;
    }

    // CRUD Formulario_Inscripcion
    public Boolean agregarFormulario_CAEPP(final Formulario_Inscripcion formulario) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_FOR_INS(PSPRIDEN_PIDM IN INT, PFECHA_INSCRIPCION IN
        // DATE,
        // PFECHA_INGRESO_UNA IN DATE, PCAMPUS IN VARCHAR2, PCARRERA IN VARCHAR2,
        // PTIPO_BECA IN VARCHAR2,
        // PID_ESTUDIANTE IN INT, PID_TIPO_ESTADO IN INT, PID_TIPO_NIVEL IN INT,
        // PID_TIPO_CICLO IN INT)
        // UNA_PSICOL.FUN_ID_EST_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_INS_FOR_INS(%d,to_date('%s','yyyy-mm-dd'),to_date('%s','yyyy-mm-dd'),'%s','%s','%s',FUN_ID_EST_CAEPP_SPR(%d),%d,%d,%d)}";
        Query = String.format(Query, formulario.getSpriden_pidm(), formulario.getFecha_inscripcion().toString(),
                formulario.getFecha_ingreso_una().toString(), formulario.getCampus(), formulario.getCarrera(),
                formulario.getTipo_beca(), formulario.getEstudiante().getSpriden_pidm(), formulario.getId_tipo_estado(),
                formulario.getId_tipo_nivel(), formulario.getId_tipo_ciclo());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarFormulario_CAEPP(final Formulario_Inscripcion formulario) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_FOR_INS(PID_FORMULARIO_INSCRIPCION IN INT,
        // PSPRIDEN_PIDM IN INT, PFECHA_INSCRIPCION IN DATE, PCAMPUS IN VARCHAR2,
        // PCARRERA IN VARCHAR2, PTIPO_BECA IN VARCHAR2, PID_ESTUDIANTE IN INT,
        // PID_TIPO_ESTADO IN INT, PID_TIPO_NIVEL IN INT, PID_TIPO_CICLO IN INT)
        String Query = "{call PRC_CAEPP_ACT_FOR_INS(%d,%d,to_date('%s','yyyy-mm-dd'),to_date('%s','yyyy-mm-dd'),'%s','%s','%s',%d,%d,%d,%d)}";
        Query = String.format(Query, formulario.getId_formulario_inscripcion(), formulario.getSpriden_pidm(),
                formulario.getFecha_inscripcion().toString(), formulario.getFecha_ingreso_una().toString(),
                formulario.getCampus(), formulario.getCarrera(), formulario.getTipo_beca(),
                formulario.getEstudiante().getId_estudiante(), formulario.getId_tipo_estado(),
                formulario.getId_tipo_nivel(), formulario.getId_tipo_ciclo());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarFormularioPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_FOR_INS(PID_FORMULARIO_INSCRIPCION IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_IDE(PIDENTIFICACION IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ELI_FOR_INS(FUN_ID_FOR_INS_CAEPP_IDE('%s'))}";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase FormularioRepositorio
