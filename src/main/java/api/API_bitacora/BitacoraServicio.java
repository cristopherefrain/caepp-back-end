package api.API_bitacora;

import api.bitacora.Bitacora_Formulario;
import api.bitacora.Bitacora_Ajuste_Metodologico;
import api.bitacora.Bitacora_Equipo_Apoyo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase BitacoraServicio
public class BitacoraServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    BitacoraRepositorio bitacoraRepo;

    public List<Bitacora_Formulario> getAllBitacorasFormularios_CAEPP() throws Exception {
        return bitacoraRepo.getAllBitacorasFormularios_CAEPP();
    }

    public List<Bitacora_Ajuste_Metodologico> getAllBitacorasAjustes_CAEPP() throws Exception {
        return bitacoraRepo.getAllBitacorasAjustes_CAEPP();
    }

    public List< Bitacora_Equipo_Apoyo> getAllBitacorasEquipos_CAEPP() throws Exception {
        return bitacoraRepo.getAllBitacorasEquipos_CAEPP();
    }
}
// Fin clase BitacoraServicio
