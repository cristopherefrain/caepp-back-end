package api.API_bitacora;

import api.bitacora.Bitacora_Ajuste_Metodologico;
import api.bitacora.Bitacora_Equipo_Apoyo;
import api.bitacora.Bitacora_Formulario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

//Anotacion @RestController indica a Spring Boot que la clase es un controlador web que maneja los Request HTTP
@RestController
@RequestMapping("/bitacora")
//Inicio clase BitacoraControlador
public final class BitacoraControlador {

    //Anotacion @Autowire indica a Spring Boot que debe inyectar una intancia cuando crea el controlador
    @Autowired
    BitacoraServicio bitacoraService;

    // =================================================== SISTEMA CAEPP
    // ======================================================
    // url: http://localhost:9898/bitacora/formulario/getAll
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getAll al
    // metodo getAllBitacorasFormularios_CAEPP_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/formulario/getAll")
    public List<Bitacora_Formulario> getAllBitacorasFormularios_CAEPP_JSON() {
        try {
            final List<Bitacora_Formulario> formularios = bitacoraService.getAllBitacorasFormularios_CAEPP();
            return formularios;
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/bitacora/ajuste/getAll
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getAll al
    // metodo getAllBitacorasAjustes_CAEPP_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/ajustes/getAll")
    public List<Bitacora_Ajuste_Metodologico> getAllBitacorasAjustes_CAEPP_JSON() {
        try {
            final List<Bitacora_Ajuste_Metodologico> ajustes = bitacoraService.getAllBitacorasAjustes_CAEPP();
            return ajustes;
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/bitacora/equipo/getAll
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getAll al
    // metodo getAllBitacorasEquipos_CAEPP_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/equipos/getAll")
    public List<Bitacora_Equipo_Apoyo> getAllBitacorasEquipos_CAEPP_JSON() {
        try {
            final List<Bitacora_Equipo_Apoyo> ajustes = bitacoraService.getAllBitacorasEquipos_CAEPP();
            return ajustes;
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }
}
//Fin clase BitacoraControlador
