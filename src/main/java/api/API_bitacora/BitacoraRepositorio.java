package api.API_bitacora;

import api.bitacora.Bitacora_Formulario;
import api.bitacora.Bitacora_Ajuste_Metodologico;
import api.bitacora.Bitacora_Equipo_Apoyo;
import api.modelos.Repositorio_Principal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion 
//(Capa de Persistencia)
@Repository
//Inicio clase BitacoraRepositorio
public class BitacoraRepositorio extends Repositorio_Principal {

    // =================================================== SISTEMA CAEPP
    // ======================================================
    // Metodo para construir un objeto Formulario_Inscripcion apartir de una tupla
    // de la base de datos
    private Bitacora_Formulario crearBitacoraFormulario_CAEPP(final CachedRowSet rowset) throws Exception {
        Integer id_bitacora;
        String operacion;
        Date fecha_operacion;
        String usuario;

        Integer id_formulario_inscripcion;
        Integer spriden_pidm;
        Date fecha_inscripcion;
        Date fecha_ingreso_una;
        String campus;
        String carrera;
        String tipo_beca;

        String titulo_tipo_estado;
        String titulo_tipo_nivel;
        String titulo_tipo_ciclo;
        String estudiante;

        try {
            id_bitacora = rowset.getInt("ID_BITACORA");
            operacion = rowset.getString("OPERACION");
            fecha_operacion = rowset.getDate("FECHA_OPERACION");
            usuario = rowset.getString("USUARIO");

            id_formulario_inscripcion = rowset.getInt("ID_FORMULARIO_INSCRIPCION");
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            fecha_inscripcion = rowset.getDate("FECHA_INSCRIPCION");
            fecha_ingreso_una = rowset.getDate("FECHA_INGRESO_UNA");
            campus = rowset.getString("CAMPUS");
            carrera = rowset.getString("CARRERA");
            tipo_beca = rowset.getString("TIPO_BECA");

            titulo_tipo_estado = rowset.getString("DESCRIPCION_ESTADO");
            titulo_tipo_nivel = rowset.getString("DESCRIPCION_NIVEL");
            titulo_tipo_ciclo = rowset.getString("DESCRIPCION_CICLO");

            estudiante = rowset.getString("ESTUDIANTE");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }

        return new Bitacora_Formulario(id_bitacora, operacion, fecha_operacion, usuario,
                id_formulario_inscripcion, spriden_pidm, fecha_inscripcion, fecha_ingreso_una, campus, carrera, tipo_beca,
                titulo_tipo_estado, titulo_tipo_nivel, titulo_tipo_ciclo, estudiante);
    }

    private Bitacora_Ajuste_Metodologico crearBitacoraAjuste_CAEPP(final CachedRowSet rowset) throws Exception {
        Integer id_bitacora;
        String operacion;
        Date fecha_operacion;
        String usuario;

        Integer id_detalle_ajuste = null;
        Date fecha = null;
        String observaciones = null;
        Integer spriden_pidm = null;

        String titulo_tipo_ajuste = null;
        String estudiante = null;
        try {
            id_bitacora = rowset.getInt("ID_BITACORA");
            operacion = rowset.getString("OPERACION");
            fecha_operacion = rowset.getDate("FECHA_OPERACION");
            usuario = rowset.getString("USUARIO");

            id_detalle_ajuste = rowset.getInt("ID_DETALLE_AJUSTE");
            fecha = rowset.getDate("FECHA");
            observaciones = rowset.getString("OBSERVACIONES");
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");

            titulo_tipo_ajuste = rowset.getString("DESCRIPCION_AJUSTE");
            estudiante = rowset.getString("ESTUDIANTE");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        return new Bitacora_Ajuste_Metodologico(id_bitacora, operacion, fecha_operacion, usuario, id_detalle_ajuste, fecha, observaciones, spriden_pidm, titulo_tipo_ajuste, estudiante);
    }

    private Bitacora_Equipo_Apoyo crearBitacoraEquipo_CAEPP(final CachedRowSet rowset) throws Exception {
        Integer id_bitacora;
        String operacion;
        Date fecha_operacion;
        String usuario;

        Integer id_detalle_equipo = null;
        Date fecha_prestamo = null;
        Date fecha_devolucion = null;
        Integer numero_activo = null;
        String observaciones = null;
        Integer spriden_pidm = null;

        String titulo_tipo_equipo_apoyo = null;
        String titulo_tipo_ciclo = null;
        String titulo_tipo_estado = null;
        String estudiante = null;
        try {
            id_bitacora = rowset.getInt("ID_BITACORA");
            operacion = rowset.getString("OPERACION");
            fecha_operacion = rowset.getDate("FECHA_OPERACION");
            usuario = rowset.getString("USUARIO");

            id_detalle_equipo = rowset.getInt("ID_DETALLE_EQUIPO");
            fecha_prestamo = rowset.getDate("FECHA_PRESTAMO");
            fecha_devolucion = rowset.getDate("FECHA_DEVOLUCION");
            numero_activo = rowset.getInt("NUMERO_ACTIVO");
            observaciones = rowset.getString("OBSERVACIONES");
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");

            titulo_tipo_equipo_apoyo = rowset.getString("DESCRIPCION_EQUIPO");
            titulo_tipo_ciclo = rowset.getString("DESCRIPCION_CICLO");
            titulo_tipo_estado = rowset.getString("DESCRIPCION_CONDICION");
            estudiante = rowset.getString("ESTUDIANTE");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        return new Bitacora_Equipo_Apoyo(id_bitacora, operacion, fecha_operacion, usuario, id_detalle_equipo, fecha_prestamo, fecha_devolucion, numero_activo, observaciones, spriden_pidm,
                titulo_tipo_equipo_apoyo, titulo_tipo_ciclo, titulo_tipo_estado, estudiante);
    }

    public List<Bitacora_Formulario> getAllBitacorasFormularios_CAEPP() throws Exception {
        String Query = "SELECT F.ID_BITACORA, F.OPERACION, F.FECHA_OPERACION, F.USUARIO,"
                + "F.ID_FORMULARIO_INSCRIPCION, F.SPRIDEN_PIDM, F.FECHA_INSCRIPCION, F.FECHA_INGRESO_UNA, F.CAMPUS,CARRERA, F.TIPO_BECA,"
                + "F.ID_ESTUDIANTE, F.ID_TIPO_ESTADO, F.ID_TIPO_NIVEL, F.ID_TIPO_CICLO,"
                + "E.DESCRIPCION_ESTADO, N.DESCRIPCION_NIVEL, DESCRIPCION_CICLO, ES.NOMBRE || ' ' ||ES.APELLIDOS ESTUDIANTE "
                + "FROM BITACORA_FORMULARIO F, BITACORA_TIPO_ESTADO E, BITACORA_TIPO_NIVEL N, BITACORA_TIPO_CICLO C, BITACORA_ESTUDIANTE ES "
                + "WHERE F.ID_TIPO_ESTADO = E.ID_TIPO_ESTADO "
                + "AND F.ID_TIPO_NIVEL = N.ID_TIPO_NIVEL "
                + "AND F.ID_TIPO_CICLO = C.ID_TIPO_CICLO "
                + "AND F.SPRIDEN_PIDM = ES.SPRIDEN_PIDM";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var formularios = new ArrayList<Bitacora_Formulario>();
        Bitacora_Formulario formulario;
        while (rowset.next()) {
            formulario = crearBitacoraFormulario_CAEPP(rowset);
            formularios.add(formulario);
        }
        return formularios;
    }

    public List<Bitacora_Ajuste_Metodologico> getAllBitacorasAjustes_CAEPP() throws Exception {
        String Query = "SELECT A.ID_BITACORA, A.OPERACION, A.FECHA_OPERACION, A.USUARIO,"
                + "A.ID_DETALLE_AJUSTE,A.OBSERVACIONES,A.FECHA,A.SPRIDEN_PIDM,A.ID_TIPO_AJUSTE,"
                + "T.DESCRIPCION_AJUSTE, ES.NOMBRE || ' ' ||ES.APELLIDOS ESTUDIANTE "
                + "FROM BITACORA_DETALLE_AJUSTE A, BITACORA_TIPO_AJUSTE T, BITACORA_ESTUDIANTE ES "
                + "WHERE A.ID_TIPO_AJUSTE = T.ID_TIPO_AJUSTE "
                + "AND A.SPRIDEN_PIDM = ES.SPRIDEN_PIDM";

        Query = String.format(Query, "");
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var ajustes = new ArrayList<Bitacora_Ajuste_Metodologico>();
        Bitacora_Ajuste_Metodologico ajuste;
        while (rowset.next()) {
            ajuste = crearBitacoraAjuste_CAEPP(rowset);
            ajustes.add(ajuste);
        }
        return ajustes;
    }

    public List< Bitacora_Equipo_Apoyo> getAllBitacorasEquipos_CAEPP() throws Exception {
        String Query = "SELECT E.ID_BITACORA, E.OPERACION, E.FECHA_OPERACION, E.USUARIO,"
                + "E.ID_DETALLE_EQUIPO, E.FECHA_PRESTAMO, E.SPRIDEN_PIDM, E.FECHA_DEVOLUCION, E.NUMERO_ACTIVO, E.OBSERVACIONES, E.ID_TIPO_EQUIPO, E.ID_TIPO_CICLO, E.ID_TIPO_CONDICION,"
                + "TE.DESCRIPCION_EQUIPO, C.DESCRIPCION_CICLO, TC.DESCRIPCION_CONDICION, ES.NOMBRE || ' ' ||ES.APELLIDOS ESTUDIANTE "
                + "FROM BITACORA_DETALLE_EQUIPO_APOYO E, BITACORA_TIPO_EQUIPO_APOYO TE, BITACORA_TIPO_CICLO C, BITACORA_TIPO_CONDICION_EQUIPO TC, BITACORA_ESTUDIANTE ES "
                + "WHERE E.ID_TIPO_EQUIPO = TE.ID_TIPO_EQUIPO "
                + "AND E.ID_TIPO_CICLO = C.ID_TIPO_CICLO "
                + "AND E.ID_TIPO_CONDICION = TC.ID_TIPO_CONDICION "
                + "AND E.SPRIDEN_PIDM = ES.SPRIDEN_PIDM";
        // + "AND ESTADO = 'A'";
        Query = String.format(Query, "");
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var equipos = new ArrayList<Bitacora_Equipo_Apoyo>();
        Bitacora_Equipo_Apoyo equipo;
        while (rowset.next()) {
            equipo = crearBitacoraEquipo_CAEPP(rowset);
            equipos.add(equipo);
        }
        return equipos;
    }
}
//Fin clase BitacoraRepositorio
