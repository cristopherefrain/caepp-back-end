package api.estudiante;

import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicioF
@Service
// Inicio clase EstudianteServicio
public class EstudianteServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    EstudianteRepositorio estudianteRepo;
    Estudiante modelo;

    // =========================================== CENTRO DE GESTION INFORMATICA ==============================================
    public int getSpridenPidmEstudiante_CGI(final String idenEstudiante) throws Exception {
        return estudianteRepo.getSpridenPidmEstudiante_CGI(idenEstudiante);
    }

    @Async
    public CompletableFuture<Estudiante> getEstudiantePorSpridenPIDM_CGI(final int spridenEstudiante) throws Exception {
        modelo = estudianteRepo.getEstudiantePorSpridenPIDM_CGI(spridenEstudiante);
        return CompletableFuture.completedFuture(modelo);
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public int getSpridenPidmEstudiante_CAEPP(final String idenEstudiante) throws Exception {
        return estudianteRepo.getSpridenPidmEstudiante_CAEPP(idenEstudiante);
    }

    @Async
    public CompletableFuture<Estudiante> getEstudiantePorSpridenPIDM_CAEPP(final int spridenEstudiante) throws Exception {
        modelo = estudianteRepo.getEstudiantePorSpridenPIDM_CAEPP(spridenEstudiante);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Estudiante
    @Async
    public CompletableFuture<Boolean> agregarEstudiante_CAEPP(final Estudiante estudiante) throws Exception {
        if (modelo.equals(estudiante)) {
            Boolean var = estudianteRepo.agregarEstudiante_CAEPP(estudiante);
            return CompletableFuture.completedFuture(var);
        }
        throw new Exception("La informacion del estudiante no coincide con la del sistema");
    }

    @Async
    public CompletableFuture<Boolean> actualizarEstudiante_CAEPP(final Estudiante estudiante) throws Exception {
        Boolean var = true;
        if (modelo == null || !modelo.equals(estudiante)) {
            modelo = estudiante;
            var = estudianteRepo.actualizarEstudiante_CAEPP(estudiante);
        }
        return CompletableFuture.completedFuture(var);
    }

    @Async
    public CompletableFuture<Boolean> eliminarEstudiantePorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        modelo = null;
        final Boolean var = estudianteRepo.eliminarEstudiantePorIdentificacion_CAEPP(idenEstudiante);
        return CompletableFuture.completedFuture(var);
    }
    // =================================================== SISTEMA CAEPP ======================================================
}
// Fin clase EstudianteServicio
