package api.estudiante;

import api.modelos.Persona;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

//Inicio clase Estudiante
public final class Estudiante extends Persona {

    // Atributos
    private int id_estudiante;
    private String genero;
    private String estado_civil;
    private Date fecha_nacimiento;

    public Estudiante() {
        super();
        this.id_estudiante = 0;
        this.genero = "";
        this.estado_civil = "";
        this.fecha_nacimiento = Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }

    public Estudiante(final int id_estudiante, final String genero, final String estado_civil,
            final Date fecha_nacimiento, final int spriden_pidm, final String identificacion, final String nombre,
            final String apellidos) {
        super(spriden_pidm, identificacion, nombre, apellidos);
        this.id_estudiante = id_estudiante;
        this.genero = genero;
        this.estado_civil = estado_civil;
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public void setId_estudiante(final int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }

    public void setGenero(final String genero) {
        this.genero = genero;
    }

    public void setEstado_civil(final String estado_civil) {
        this.estado_civil = estado_civil;
    }

    public void setFecha_nacimiento(final Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public int getId_estudiante() {
        return id_estudiante;
    }

    public String getGenero() {
        return genero;
    }

    public String getEstado_civil() {
        return estado_civil;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "id_estudiante=" + id_estudiante + ", genero=" + genero + ", estado_civil="
                + estado_civil + ", fecha_nacimiento=" + fecha_nacimiento + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.genero);
        hash = 97 * hash + Objects.hashCode(this.estado_civil);
        hash = 97 * hash + Objects.hashCode(this.fecha_nacimiento);
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        if (!Objects.equals(this.genero, other.genero)) {
            return false;
        }
        if (!Objects.equals(this.estado_civil, other.estado_civil)) {
            return false;
        }
        // if (!Objects.equals(this.fecha_nacimiento, other.fecha_nacimiento)) {
        //     return false;
        // }
        return true;
    }

    public Boolean objetoIncompleto() throws Exception {
        super.objetoIncompleto();
        if (id_estudiante == 0) {
            throw new Exception("id_estudiante default");
        }
        if (genero.isBlank()) {
            throw new Exception("genero default");
        }
        if (estado_civil.isBlank()) {
            throw new Exception("estado_civil default");
        }
        if (fecha_nacimiento.after(Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))) {
            throw new Exception("fecha_nacimiento default");
        }
        return false;
    }

}
// Fin clase Estudiante
