package api.estudiante;

import api.modelos.Repositorio_Principal;
import java.sql.Date;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion 
//(Capa de Persistencia)
@Repository
// Inicio clase EstudianteRepositorio
public class EstudianteRepositorio extends Repositorio_Principal {

    // Metodo para construir un objeto Estudiante apartir de una tupla de la base de
    // datos
    private Estudiante crearEstudiante(final CachedRowSet rowset) throws Exception {
        Integer spriden_pidm = null;
        String identificacion = null;
        String nombre = null;
        String apellidos = null;

        Integer id_estudiante = null;
        String genero = null;
        String estado_civil = null;
        Date fecha_nacimiento = null;
        try {
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            identificacion = rowset.getString("IDENTIFICACION");
            nombre = rowset.getString("NOMBRE");
            apellidos = rowset.getString("APELLIDOS");

            id_estudiante = rowset.getInt("ID_ESTUDIANTE");
            genero = rowset.getString("GENERO");
            estado_civil = rowset.getString("ESTADO_CIVIL");
            fecha_nacimiento = rowset.getDate("FECHA_NACIMIENTO");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        // Valida campos de Persona
        validarCampo(spriden_pidm, "SPRIDEN_PIDM");
        validarCampo(identificacion, "IDENTIFICACION");
        validarCampo(nombre, "NOMBRE");
        validarCampo(apellidos, "APELLIDOS");
        // Valida campos de Estudiante
        validarCampo(id_estudiante, "ID_ESTUDIANTE");
        validarCampo(genero, "GENERO");
        validarCampo(estado_civil, "ESTADO_CIVIL");
        validarCampo(fecha_nacimiento, "FECHA_NACIMIENTO");

        final Estudiante estudiante = new Estudiante();

        estudiante.setSpriden_pidm(spriden_pidm);
        estudiante.setIdentificacion(identificacion);
        estudiante.setNombre(nombre.toUpperCase());
        estudiante.setApellidos(apellidos.toUpperCase());

        estudiante.setId_estudiante(id_estudiante);
        estudiante.setGenero(genero.equals("M") ? "MASCULINO" : "FEMENINO");
        estudiante.setEstado_civil(estado_civil.equals("S") ? "SOLTERO(a)"
                : estado_civil.equals("C") ? "CASADO(a)" : estado_civil.equals("V") ? "VIUDO(a)" : "INDEFINIDO");
        estudiante.setFecha_nacimiento(fecha_nacimiento);
        return estudiante;
    }

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    public int getSpridenPidmEstudiante_CGI(final String idenEstudiante) throws Exception {
        String Query = "SELECT SPRIDEN_PIDM " + "FROM SPRIDEN " + "WHERE SPRIDEN_ID = '%s' "
                + "AND SPRIDEN_CHANGE_IND IS NULL";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        Integer spriden_pidm = null;
        try {
            if (rowset.next()) {
                spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            }
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        validarCampo(spriden_pidm, "SPRIDEN_PIDM");
        return spriden_pidm;
    }

    public Estudiante getEstudiantePorSpridenPIDM_CGI(final int spridenEstudiante) throws Exception {
        String Query = "SELECT -1 ID_ESTUDIANTE, SPRIDEN_PIDM, SPRIDEN_ID IDENTIFICACION, SPRIDEN_FIRST_NAME||' '||SPRIDEN_MI NOMBRE, SPRIDEN_LAST_NAME APELLIDOS, "
                + "SPBPERS_SEX GENERO, SPBPERS_BIRTH_DATE FECHA_NACIMIENTO, SPBPERS_MRTL_CODE ESTADO_CIVIL "
                + "FROM SPRIDEN INNER JOIN SPBPERS ON SPRIDEN_PIDM = SPBPERS_PIDM " + "WHERE SPRIDEN_PIDM = %d "
                + "AND SPRIDEN_CHANGE_IND IS NULL";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        Estudiante estudiante = null;
        if (rowset.next()) {
            estudiante = crearEstudiante(rowset);// Agrega el primer registro que devuelve la ejecucion del Query
        }
        if (estudiante == null) {
            throw new Exception("No existe un Estudiante con la identificacion " + spridenEstudiante);
        }
        return estudiante;
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    // Metodo para construir un objeto Estudiante apartir de una tupla de la base de
    // datos
    public int getSpridenPidmEstudiante_CAEPP(final String idenEstudiante) throws Exception {
        String Query = "SELECT SPRIDEN_PIDM " + "FROM ESTUDIANTE " + "WHERE IDENTIFICACION = '%s' ";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        Integer spriden_pidm = null;
        try {
            if (rowset.next()) {
                spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            }
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        if (spriden_pidm == null || spriden_pidm == 0) {
            throw new Exception("No existen un Estudiante con la identificacion "+idenEstudiante+" en el sistema");
        }
        validarCampo(spriden_pidm, "SPRIDEN_PIDM");
        return spriden_pidm;
    }

    public Estudiante getEstudiantePorSpridenPIDM_CAEPP(final int spridenEstudiante) throws Exception {
        String Query = "SELECT ID_ESTUDIANTE, SPRIDEN_PIDM, IDENTIFICACION, NOMBRE, APELLIDOS, GENERO, ESTADO_CIVIL, FECHA_NACIMIENTO "
                + "FROM ESTUDIANTE " + "WHERE SPRIDEN_PIDM = %d ";
        // + "AND ESTADO = 'A'";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        Estudiante estudiante = null;
        if (rowset.next()) {
            estudiante = crearEstudiante(rowset);// Agrega el primer registro que devuelve la ejecucion del Query
        }
        if (estudiante == null) {
            throw new Exception("No existe un Estudiante con la identificacion " + spridenEstudiante);
        }
        return estudiante;
    }

    // CRUD Estudiante
    public Boolean agregarEstudiante_CAEPP(final Estudiante estudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_EST(PSPRIDEN_PIDM IN INT ,PIDENTIFICACION IN
        // VARCHAR2, PNOMBRE IN VARCHAR2, PAPELLIDOS IN VARCHAR2,
        // PGENERO IN VARCHAR2, PESTADO_CIVIL IN VARCHAR2, PFECHA_NACIMIENTO IN DATE)
        String Query = "{call PRC_CAEPP_INS_EST(%d,'%s','%s','%s','%s','%s',to_date('%s','yyyy-mm-dd'))}";
        Query = String.format(Query, estudiante.getSpriden_pidm(), estudiante.getIdentificacion(),
                estudiante.getNombre(), estudiante.getApellidos(), estudiante.getGenero().substring(0, 1),
                estudiante.getEstado_civil().substring(0, 1), estudiante.getFecha_nacimiento().toString());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarEstudiante_CAEPP(final Estudiante estudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_EST(ID_ESTUDIANTE IN INT, PSPRIDEN_PIDM IN INT
        // ,PIDENTIFICACION IN VARCHAR2, PNOMBRE IN VARCHAR2,
        // PAPELLIDOS IN VARCHAR2, PGENERO IN VARCHAR2, PESTADO_CIVIL IN VARCHAR2,
        // PFECHA_NACIMIENTO IN DATE)
        // UNA_PSICOL.FUN_ID_EST_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_ACT_EST(FUN_ID_EST_CAEPP_SPR(%d),%d,'%s','%s','%s','%s','%s',to_date('%s','yyyy-mm-dd'))}";
        Query = String.format(Query, estudiante.getSpriden_pidm(), estudiante.getSpriden_pidm(),
                estudiante.getIdentificacion(), estudiante.getNombre(), estudiante.getApellidos(),
                estudiante.getGenero().substring(0, 1), estudiante.getEstado_civil().substring(0, 1),
                estudiante.getFecha_nacimiento().toString());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarEstudiantePorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_EST(PID_ESTUDIANTE IN INT)
        // UNA_PSICOL.FUN_ID_EST_CAEPP_IDE(PIDENTIFICACION IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ELI_EST(FUN_ID_EST_CAEPP_IDE('%s'))}";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase EstudianteRepositorio
