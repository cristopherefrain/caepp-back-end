package api.funcionario;

import api.modelos.Repositorio_Principal;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion 
//(Capa de Persistencia)
@Repository
// Inicio clase FuncionarioRepositorio
public class FuncionarioRepositorio extends Repositorio_Principal {

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    // Metodo para construir un objeto Funcionario apartir de una tupla de la base
    // de datos
    private Funcionario crearFuncionario_CGI(final CachedRowSet rowset) throws Exception {
        Integer spriden_pidm = null;
        String identificacion = null;
        String nombre = null;
        String apellidos = null;
        try {
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            identificacion = rowset.getString("IDENTIFICACION");
            nombre = rowset.getString("NOMBRE");
            apellidos = rowset.getString("APELLIDOS");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        validarCampo(spriden_pidm, "SPRIDEN_PIDM");
        validarCampo(identificacion, "IDENTIFICACION");
        validarCampo(nombre, "NOMBRE");
        validarCampo(apellidos, "APELLIDOS");

        final Funcionario funcionario = new Funcionario("", spriden_pidm, identificacion, nombre, apellidos);
        return funcionario;
    }

    private String cargarCorreoFuncionario_CGI(final int spridenFuncionario) throws Exception {
        String Query = "SELECT GOREMAL_EMAIL_ADDRESS CORREO " + "FROM GOREMAL " + "WHERE GOREMAL_PIDM = %d "
                + "AND GOREMAL_STATUS_IND = 'A' " + "AND GOREMAL_EMAL_CODE = 'CES' "
                + "AND TRIM(GOREMAL_EMAIL_ADDRESS) IS NOT NULL";
        Query = String.format(Query, spridenFuncionario);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        String correo_institucional = "";
        if (rowset.next()) {
            correo_institucional = rowset.getString("CORREO");
            validarCampo(correo_institucional, "CORREO");
        }
        return correo_institucional;
    }

    public Funcionario getFuncionarioPorIdentificacion_CGI(final String idenFuncionario) throws Exception {
        String Query = "SELECT SPRIDEN_PIDM, SPRIDEN_ID IDENTIFICACION, SPRIDEN_FIRST_NAME||' '||SPRIDEN_MI NOMBRE, SPRIDEN_LAST_NAME APELLIDOS "
                + "FROM SPRIDEN " + "WHERE SPRIDEN_ID = '%s' " + "AND SPRIDEN_CHANGE_IND IS NULL";
        Query = String.format(Query, idenFuncionario);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        Funcionario funcionario = null;
        if (rowset.next()) {
            funcionario = crearFuncionario_CGI(rowset);// Agrega el primer registro que devuelve la ejecucion del Query
            funcionario.setCorreo_institucional(cargarCorreoFuncionario_CGI(funcionario.getSpriden_pidm()));// Agrega
            // informacion
            // propia de
            // funcionario
        }
        if (funcionario == null) {
            throw new Exception("No existe un Funcionario con la identificacion " + idenFuncionario);
        }
        return funcionario;
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
}
// Fin clase FuncionarioRepositorio
