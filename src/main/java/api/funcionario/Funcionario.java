package api.funcionario;

import api.modelos.Persona;

//Inicio clase Funcionario
public final class Funcionario extends Persona {

//Atributos
    String correo_institucional;

    public Funcionario() {
        super();
        this.correo_institucional = "";
    }

    public Funcionario(final String correo_institucional, final int spriden_pidm, final String identificacion,
            final String nombre, final String apellidos) {
        super(spriden_pidm, identificacion, nombre, apellidos);
        this.correo_institucional = correo_institucional;
    }

    public void setCorreo_institucional(final String correo_institucional) {
        this.correo_institucional = correo_institucional;
    }

    public String getCorreo_institucional() {
        return correo_institucional;
    }

    @Override
    public String toString() {
        return super.toString() + " Funcionario{" + "correo_institucional=" + correo_institucional + '}';
    }

    public Boolean objetoIncompleto() throws Exception {
        super.objetoIncompleto();
        if (correo_institucional.isBlank()) {
            throw new Exception("correo_institucional default");
        }
        return false;
    }

}
//Fin clase Funcionario
