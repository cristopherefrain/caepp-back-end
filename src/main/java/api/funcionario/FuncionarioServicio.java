package api.funcionario;

import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
//Inicio clase FuncionarioServicio
public class FuncionarioServicio {

//Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia FuncionarioRepositorio cuando crea el servicio
    @Autowired
    FuncionarioRepositorio funcionarioRepo;

//===========================================   CENTRO DE GESTION INFORMATICA   ==============================================
    @Async
    public CompletableFuture<Funcionario> getFuncionarioPorIdentificacion_CGI(final String idenFuncionario) throws Exception {
        Funcionario funcionario = funcionarioRepo.getFuncionarioPorIdentificacion_CGI(idenFuncionario);
        return CompletableFuture.completedFuture(funcionario);
    }
//===========================================   CENTRO DE GESTION INFORMATICA   ==============================================
}
//Fin clase FuncionarioServicio
