package api.API_autentificacion;

import api.API_usuario.UsuariosServicio;
import api.usuario.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase AutentificacionServicio
public final class AutentificacionServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    UsuariosServicio usuariosService;

    public Usuario autenticarCredenciales(final Auth auth) throws Exception {
        return usuariosService.getUsuarioPorIdentificacion_CAEPP(auth.getIdentificacion());
    }

    public void purgar_db() throws Exception {
        usuariosService.purgar_db();
    }
}
// Fin clase AutentificacionServicio
