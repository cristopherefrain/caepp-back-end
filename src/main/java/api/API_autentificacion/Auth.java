package api.API_autentificacion;

//Inicio clase Auth
public final class Auth {

//Atributos
    private String identificacion;
    private String clave;

    public Auth() {
        this.identificacion = "";
        this.clave = "";
    }

    public Auth(final String identificacion, final String clave) {
        this.identificacion = identificacion;
        this.clave = clave;
    }

    public void setIdentificacion(final String identificacion) {
        this.identificacion = identificacion;
    }

    public void setClave(final String clave) {
        this.clave = clave;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getClave() {
        return clave;
    }

    @Override
    public String toString() {
        return "Auth{" + "identificacion=" + identificacion + ", clave=" + clave + '}';
    }

    public Boolean objetoIncompleto() throws Exception {
        if (identificacion.isBlank()) {
            throw new Exception("identificacion default");
        }
        if (clave.isBlank()) {
            throw new Exception("clave default");
        }
        return false;
    }

}
//Fin clase Auth
