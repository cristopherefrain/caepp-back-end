package api.API_autentificacion;

import static api.modelos.ConstantesGlobales.GUARDAR;
import api.usuario.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

//Anotacion @RestController indica a Spring Boot que la clase es un controlador web que maneja los Request HTTP
@RestController
@RequestMapping("/autentificacion")
//Inicio clase AutentificacionControlador
public final class AutentificacionControlador {

    //Anotacion @Autowire indica a Spring Boot que debe inyectar una intancia cuando crea el controlador
    @Autowired
    AutentificacionServicio autentificacionService;

    // url: http://localhost:9898/autentificacion/auth
    //Anotacion @RequestMapping mapea el metodo HTTP.POST y la ruta /auth al metodo autenticarCredenciales
    @RequestMapping(method = RequestMethod.POST, value = "/auth")
    public Usuario autenticarCredenciales(@RequestBody final Auth auth) {
        try {
            if (auth == null) {
                throw new Exception("Objeto null");
            }
            if (!auth.objetoIncompleto()) {
                final Usuario usuario = autentificacionService.autenticarCredenciales(auth);
                if (usuario != null && !usuario.objetoIncompleto(GUARDAR)) {
                    return usuario;
                }
            }
            throw new Exception("Error en autentificacion");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 403 FORBIDDEN
        }
    }

    // url: http://localhost:9898/autentificacion/purgar
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /auth al metodo
    // autenticarCredenciales
    @RequestMapping(method = RequestMethod.GET, value = "/purgar")
    public void purgar_db() {
        try {
            autentificacionService.purgar_db();
        } catch (final Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD_REQUEST
        }
    }

}

//Fin clase AutentificacionControlador
