package api.envio_correos;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import static com.itextpdf.text.Chunk.NEWLINE;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.lowagie.text.Rectangle;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/email")
public class Controller {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private Servicio servicio;
    @Autowired
    private JavaMailSender mailSender;

    @PostMapping(value = "/send")
    public String enviarCorreo(@RequestBody Correo correo) throws FileNotFoundException, DocumentException, IOException {
        String mjs = "Enviado con exito";
        try {
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("Ajuste.pdf"));

            document.open();
            agregarEncabezado(document);
            agregarcuerpo(correo.getContenido(), document);
            document.close();
            Session session = Session.getDefaultInstance(props);

            String passwordRemitente = "ProyectoInge2019";

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(correo.getDe()));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(correo.getPara()));
            message.setSubject(correo.getAsunto());
            message.setText(correo.getContenido());

            Transport t = session.getTransport("smtp");
            t.connect(correo.getDe(), passwordRemitente);

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.attachFile("Ajuste.pdf");;
            // Crear el multipart para agregar la parte del mensaje anterior
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            // Agregar el multipart al cuerpo del mensaje
            message.setContent(multipart);

            t.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            t.close();

            String url = "Ajuste.pdf";
            ProcessBuilder p = new ProcessBuilder();
            p.command("cmd.exe", "/c", url);
            p.start();

        } catch (AddressException ex) {
            mjs = ex.getMessage();
        } catch (MessagingException ex) {
            mjs = ex.getMessage();
        }
        return mjs;
        //fin aqui

    }

    private void agregarEncabezado(Document document) {
        try {
            Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, BaseColor.BLACK);
            String mjs = "VICERRECTORÍA DE VIDA ESTUDIANTIL\n\tUnidad de Servicios de Apoyo\n\tPrograma de Psicopedagogía\n\tTeléfono: 2277-3198 ";
            Image img1 = Image.getInstance("Imagen1.jpg");
            Image img2 = Image.getInstance("Imagen2.png");
            img1.scalePercent(45);
            img2.scalePercent(45);
            PdfPTable tabla = new PdfPTable(3);
            tabla.setWidthPercentage(100);
            PdfPCell imageCel1 = new PdfPCell(img1);
            PdfPCell imageCel2 = new PdfPCell(img2);
            PdfPCell Cel3 = new PdfPCell(new Phrase(mjs, font));
            imageCel1.setBorder(Rectangle.NO_BORDER);
            imageCel2.setBorder(Rectangle.NO_BORDER);
            Cel3.setBorder(Rectangle.NO_BORDER);
            imageCel1.setHorizontalAlignment(Element.ALIGN_LEFT);
            imageCel2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            Cel3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(imageCel1);
            tabla.addCell(Cel3);
            tabla.addCell(imageCel2);
            document.add(tabla);
        } catch (Exception e) {
        }
    }

    private void agregarcuerpo(String contenido, Document document) throws DocumentException {
        document.add(new Paragraph(""));
        document.add(NEWLINE ); 
        document.add(NEWLINE );
        document.add( Chunk.NEWLINE ); 
        document.add( Chunk.NEWLINE );
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, BaseColor.BLACK);
        Paragraph para = new Paragraph(contenido, font);
        para.setAlignment(Element.ALIGN_JUSTIFIED);
        try {
            document.add(para);
        } catch (DocumentException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
