/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.envio_correos;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity 
public class Correo {
    @Id 
    private Integer id;
    private String de;
    private String para;
    private String asunto;
    private String contenido;

    public Correo(Integer id, String de, String para, String asunto, String contenido) {
        this.id = id;
        this.de = de;
        this.para = para;
        this.asunto = asunto;
        this.contenido = contenido;
    }

    public Correo() {
        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDe() {
        return de;
    }

    public void setDe(String de) {
        this.de = de;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
    
    
    
}
