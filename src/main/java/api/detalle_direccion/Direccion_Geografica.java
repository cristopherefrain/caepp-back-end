package api.detalle_direccion;

import java.util.Objects;

//Inicio clase Direccion_Geografica
public final class Direccion_Geografica {

//Atributos
    private int id_detalle_direccion;
    private String provincia;
    private String canton;
    private int spriden_pidm;
    private int id_tipo_direccion;

    public Direccion_Geografica() {
        this.id_detalle_direccion = 0;
        this.provincia = "";
        this.canton = "";
        this.spriden_pidm = 0;
        this.id_tipo_direccion = 0;
    }

    public Direccion_Geografica(final int id_detalle_direccion, final String provincia, final String canton,
            final int spriden_pidm, final int id_tipo_direccion) {
        this.id_detalle_direccion = id_detalle_direccion;
        this.provincia = provincia;
        this.canton = canton;
        this.spriden_pidm = spriden_pidm;
        this.id_tipo_direccion = id_tipo_direccion;
    }

    public void setId_detalle_direccion(final int id_detalle_direccion) {
        this.id_detalle_direccion = id_detalle_direccion;
    }

    public void setProvincia(final String provincia) {
        this.provincia = provincia;
    }

    public void setCanton(final String canton) {
        this.canton = canton;
    }

    public void setSpriden_pidm(final int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public void setId_tipo_direccion(final int id_tipo_direccion) {
        this.id_tipo_direccion = id_tipo_direccion;
    }

    public int getId_detalle_direccion() {
        return id_detalle_direccion;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getCanton() {
        return canton;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public int getId_tipo_direccion() {
        return id_tipo_direccion;
    }

    @Override
    public String toString() {
        return "Direccion_Geografica{" + "id_detalle_direccion=" + id_detalle_direccion + ", provincia=" + provincia
                + ", canton=" + canton + ", spriden_pidm=" + spriden_pidm + ", id_tipo_direccion=" + id_tipo_direccion
                + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.id_detalle_direccion;
        hash = 79 * hash + Objects.hashCode(this.provincia);
        hash = 79 * hash + Objects.hashCode(this.canton);
        hash = 79 * hash + this.spriden_pidm;
        hash = 79 * hash + this.id_tipo_direccion;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Direccion_Geografica other = (Direccion_Geografica) obj;
        if (this.id_detalle_direccion != other.id_detalle_direccion) {
            return false;
        }
        if (this.spriden_pidm != other.spriden_pidm) {
            return false;
        }
        if (this.id_tipo_direccion != other.id_tipo_direccion) {
            return false;
        }
        if (!Objects.equals(this.provincia, other.provincia)) {
            return false;
        }
        if (!Objects.equals(this.canton, other.canton)) {
            return false;
        }
        return true;
    }

    public Boolean objetoIncompleto() throws Exception {
        if (id_detalle_direccion == 0) {
            throw new Exception("id_detalle_direccion default");
        }
        if (provincia.isBlank()) {
            throw new Exception("provincia default");
        }
        if (canton.isBlank()) {
            throw new Exception("canton default");
        }
        if (id_tipo_direccion == 0) {
            throw new Exception("id_tipo_direccion default");
        }
        if (spriden_pidm == 0) {
            throw new Exception("spriden_pidm default");
        }
        if (id_tipo_direccion == 0) {
            throw new Exception("id_tipo_direccion default");
        }
        return false;
    }
}
//Fin clase Direccion_Geografica
