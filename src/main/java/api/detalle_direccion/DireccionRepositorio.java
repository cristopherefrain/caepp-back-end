package api.detalle_direccion;

import api.modelos.Repositorio_Principal;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;
import static api.modelos.ConstantesGlobales.DIR_PROCEDENCIA;
import static api.modelos.ConstantesGlobales.DIR_RESIDENCIA;
import java.util.HashMap;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion
//(Capa de Persistencia)
@Repository
// Inicio clase DireccionRepositorio
public class DireccionRepositorio extends Repositorio_Principal {

    // Metodo para construir un objeto Direccion_Geografica apartir de una tupla de
    // la base de datos
    private Direccion_Geografica crearDireccion(final CachedRowSet rowset) throws Exception {
        try {

        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        final Integer id_detalle_direccion = rowset.getInt("ID_DETALLE_DIRECCION");
        final String codigo = rowset.getString("CODIGO");
        final String provincia = rowset.getString("PROVINCIA");
        final String canton = rowset.getString("CANTON");
        final Integer spriden_pidm = rowset.getInt("SPRIDEN_PIDM");

        validarCampo(id_detalle_direccion, "ID_DETALLE_DIRECCION");
        validarCampo(codigo, "CODIGO");
        validarCampo(provincia, "PROVINCIA " + codigo);
        validarCampo(canton, "CANTON " + codigo);
        validarCampo(spriden_pidm, "SPRIDEN_PIDM " + codigo);

        final Direccion_Geografica direccion = new Direccion_Geografica(id_detalle_direccion, provincia.toUpperCase(),
                canton.toUpperCase(), spriden_pidm, 0);
        switch (codigo) {
            case "MA":
                direccion.setId_tipo_direccion(DIR_PROCEDENCIA);
                break;
            case "BE":
                direccion.setId_tipo_direccion(DIR_RESIDENCIA);
                break;
            default:
                throw new Exception("Error al cargar Direccion, CODIGO desconocido: " + codigo);
        }
        return direccion;
    }

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    public HashMap<Integer, Direccion_Geografica> getDetalleDirecciones_CGI(final int spridenEstudiante)
            throws Exception {
        String Query = "SELECT DISTINCT SPRADDR_PIDM SPRIDEN_PIDM, -1 ID_DETALLE_DIRECCION, STVATYP_CODE CODIGO, STVATYP_DESC DESC_TIPO, "
                + "STVSTAT_DESC PROVINCIA, STVCNTY_DESC CANTON, SPRADDR_CITY DISTRITO, SPRADDR_ZIP COD_DISTRITO, "
                + "SPRADDR_STREET_LINE1||' '||SPRADDR_STREET_LINE2||' '||SPRADDR_STREET_LINE3||'.' DIRECCION "
                + "FROM SPRADDR INNER JOIN STVATYP ON SPRADDR_ATYP_CODE = STVATYP_CODE "
                + "INNER JOIN STVCNTY ON SPRADDR_CNTY_CODE = STVCNTY_CODE "
                + "INNER JOIN STVSTAT ON SPRADDR_STAT_CODE = STVSTAT_CODE "
                + "INNER JOIN GTVZIPC ON SPRADDR_ZIP = GTVZIPC_CODE " + "WHERE SPRADDR_PIDM = %d "
                + "AND SPRADDR_STATUS_IND IS NULL";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var direcciones = new HashMap<Integer, Direccion_Geografica>();
        Direccion_Geografica direccion;
        Integer key = -1;
        while (rowset.next()) {
            direccion = crearDireccion(rowset);
            direcciones.put(key--, direccion);
        }
        return direcciones;
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public HashMap<Integer, Direccion_Geografica> getDetalleDirecciones_CAEPP(final int spridenEstudiante)
            throws Exception { 
        String Query = "SELECT DD.ID_DETALLE_DIRECCION,DD.PROVINCIA,DD.CANTON,DD.SPRIDEN_PIDM,TD.NOMBRE_DIRECCION CODIGO "
                + "FROM DETALLE_DIRECCION DD, TIPO_DIRECCION TD " + "WHERE DD.SPRIDEN_PIDM = %d "
                + "AND DD.ID_TIPO_DIRECCION = TD.ID_TIPO_DIRECCION ";
        // + "AND DD.ESTADO = 'A' " + "AND TD.ESTADO = 'A'";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var direcciones = new HashMap<Integer, Direccion_Geografica>();
        Direccion_Geografica direccion;
        while (rowset.next()) {
            direccion = crearDireccion(rowset);
            direcciones.put(direccion.getId_detalle_direccion(), direccion);
        }
        return direcciones;
    }

    // CRUD Direcciones
    public Boolean agregarDetalleDireccion_CAEPP(final Direccion_Geografica direccion) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_DET_DIR(PPROVINCIA IN VARCHAR2, PCANTON IN VARCHAR2,
        // PSPRIDEN_PIDM IN INT, PID_FORMULARIO_INSCRIPCION IN INT,
        // PID_TIPO_DIRECCION IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_INS_DET_DIR('%s','%s',%d,FUN_ID_FOR_INS_CAEPP_SPR(%d),%d)}";
        Query = String.format(Query, direccion.getProvincia(), direccion.getCanton(), direccion.getSpriden_pidm(),
                direccion.getSpriden_pidm(), direccion.getId_tipo_direccion());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarDetalleDireccion_CAEPP(final Direccion_Geografica direccion) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_DET_DIR(PID_DETALLE_DIRECCION IN INT, PPROVINCIA IN
        // VARCHAR2, PCANTON IN VARCHAR2, PSPRIDEN_PIDM IN INT,
        // PID_FORMULARIO_INSCRIPCION IN INT, PID_TIPO_DIRECCION IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_INS_DET_DIR(%d,'%s','%s',%d,FUN_ID_FOR_INS_CAEPP_SPR(%d),%d)}";
        Query = String.format(Query, direccion.getId_detalle_direccion(), direccion.getProvincia(),
                direccion.getCanton(), direccion.getSpriden_pidm(), direccion.getSpriden_pidm(),
                direccion.getId_tipo_direccion());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleDireccion_CAEPP(final int id_detalle_direccion) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_DET_DIR(PID_DETALLE_DIRECCION IN INT)
        String Query = "{call PRC_CAEPP_ELI_DET_DIR(%d)}";
        Query = String.format(Query, id_detalle_direccion);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleDireccionesPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_TOD_DET_DIR(SPRIDEN_PIDM IN INT)
        // UNA_PSICOL.FUN_SPR_EST_CAEPP_IDE(PIDENTIFICACION IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ELI_TOD_DET_DIR(FUN_SPR_EST_CAEPP_IDE('%s'))}";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase DireccionRepositorio
