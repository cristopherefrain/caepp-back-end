package api.detalle_direccion;

import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase DireccionServicio
public class DireccionServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    DireccionRepositorio direccionRepo;
    HashMap<Integer, Direccion_Geografica> modelo;

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    @Async
    public CompletableFuture<HashMap<Integer, Direccion_Geografica>> getDetalleDirecciones_CGI(final int spridenEstudiante) throws Exception {
        final HashMap<Integer, Direccion_Geografica> direcciones = direccionRepo.getDetalleDirecciones_CGI(spridenEstudiante);
        return CompletableFuture.completedFuture(direcciones);
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    @Async
    public CompletableFuture<HashMap<Integer, Direccion_Geografica>> getDetalleDirecciones_CAEPP(final int spridenEstudiante) throws Exception {
        modelo = direccionRepo.getDetalleDirecciones_CAEPP(spridenEstudiante);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Direcciones
    @Async
    public CompletableFuture<Boolean> agregarDetalleDirecciones_CAEPP(final HashMap<Integer, Direccion_Geografica> direcciones)
            throws Exception {
        for (final Direccion_Geografica direccion : direcciones.values()) {
            // Agrega todos los Objetos
            if (!direccionRepo.agregarDetalleDireccion_CAEPP(direccion)) {
                return CompletableFuture.completedFuture(false);
            }
        }
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> actualizarDetalleDirecciones_CAEPP(final HashMap<Integer, Direccion_Geografica> direcciones)
            throws Exception {
        Direccion_Geografica value_direcciones;
        Direccion_Geografica value_modelo;
        for (final Integer key : modelo.keySet()) {
            value_direcciones = direcciones.get(key);
            // Si no devuelve un Objeto con el id del modelo entonces fue eliminado y se
            // manda a eliminar de la base
            if (value_direcciones == null) {
                if (!direccionRepo.eliminarDetalleDireccion_CAEPP(key)) {
                    return CompletableFuture.completedFuture(false);
                }
            }else
            // Si devuelve un Objeto y es diferente al del modelo viejo se manda a
            // actualizar
            if (!value_direcciones.equals(modelo.get(key))) {
                if (!direccionRepo.actualizarDetalleDireccion_CAEPP(value_direcciones)) {
                    return CompletableFuture.completedFuture(false);
                }
            }
        }
        if (modelo != null) {
            for (final Integer key : direcciones.keySet()) {
                value_modelo = modelo.get(key);
                // Si no devuelve un Objeto con el id del nuevo modelo entonces fue agregado y
                // hay que insertarlo en la base
                if (value_modelo == null) {
                    if (!direccionRepo.agregarDetalleDireccion_CAEPP(direcciones.get(key))) {
                        return CompletableFuture.completedFuture(false);
                    }
                }
            }
        }
        modelo = direcciones;
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> eliminarDetalleDireccionesPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        if (!direccionRepo.eliminarDetalleDireccionesPorIdentificacion_CAEPP(idenEstudiante)) {
            return CompletableFuture.completedFuture(false);
        }
        modelo = null;
        return CompletableFuture.completedFuture(true);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase DireccionServicio
