package api.API_formulario_inscripcion;

import api.detalle_formulario_inscripcion.Formulario_Inscripcion;
import static api.modelos.ConstantesGlobales.ACTUALIZAR;
import static api.modelos.ConstantesGlobales.CARGAR;
import static api.modelos.ConstantesGlobales.CREAR;
import static api.modelos.ConstantesGlobales.GUARDAR;
import static api.modelos.ConstantesGlobales.INSCRIPCION;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

//Anotacion @RestController indica a Spring Boot que la clase es un controlador web que maneja los Request HTTP
@RestController
@RequestMapping("/formulario")
//Inicio clase InscripcionControlador
public final class InscripcionControlador {

//Anotacion @Autowire indica a Spring Boot que debe inyectar una intancia cuando crea el controlador
    @Autowired
    InscripcionServicio inscripcionService;
//===========================================   CENTRO DE GESTION INFORMATICA   ==============================================
    // url: http://localhost:9898/formulario/cgi/{idenEstudiante}
    //Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /{idenEstudiante} al metodo getFormularioPorIdentificacion_JSON

    @RequestMapping(method = RequestMethod.GET, value = "/cgi/{idenEstudiante}")
    public Formulario_Inscripcion getFormularioPorIdentificacion_JSON_CGI_Inscripcion(@PathVariable final String idenEstudiante) {
        try {
            if (idenEstudiante != null && !idenEstudiante.isBlank()) {
                final Formulario_Inscripcion formulario = inscripcionService
                        .getFormularioPorIdentificacion_CGI_Inscripcion(idenEstudiante);
                if (formulario != null && !formulario.objetoIncompleto(INSCRIPCION, CREAR)) {
                    return formulario;
                }
            }
            throw new Exception("Identificacion vacio");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    
        // url: http://localhost:9898/formulario/caepp/getAll
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getAll al
    // metodo getAllUsuarios_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/getAll")
    public List<Formulario_Inscripcion> getAllFormularios_JSON_CAEPP_Inscripcion() {
        try {
            final List<Formulario_Inscripcion> formularios = inscripcionService.getAllFormularios_CAEPP_Inscripcion();
            return formularios;
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/formulario/caepp/getFilter?idenEstudiante=
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getFilter al
    // metodo getUsuariosFiltro_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/getFilter")
    public List<Formulario_Inscripcion> getFormulariosFiltro_JSON_CAEPP_Inscripcion(@RequestParam final String idenEstudiante) {
        try {
            if (idenEstudiante != null && !idenEstudiante.isBlank()) {
                final List<Formulario_Inscripcion> formularios = inscripcionService.getFormulariosFiltro_CAEPP_Inscripcion(idenEstudiante);
                return formularios;
            }
            throw new Exception("Identificacion vacio");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }
    // url: http://localhost:9898/formulario/caepp/{idenEstudiante}
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta
    // /{idenEstudiante} al metodo getFormularioPorIdentificacion_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/{idenEstudiante}")
    public Formulario_Inscripcion getFormularioPorIdentificacion_JSON_CAEPP_Inscripcion(
            @PathVariable final String idenEstudiante) {
        try {
            if (idenEstudiante != null && !idenEstudiante.isBlank()) {
                final Formulario_Inscripcion formulario = inscripcionService
                        .getFormularioPorIdentificacion_CAEPP_Inscripcion(idenEstudiante);
                if (formulario != null && !formulario.objetoIncompleto(INSCRIPCION, CARGAR)) {
                    return formulario;
                }
            }
            throw new Exception("Identificacion vacio");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }

    // CRUD Formulario_Inscripcion
    // url: http://localhost:9898/formulario/caepp
    // Anotacion @RequestMapping mapea el metodo HTTP.POST al metodo
    // agregarFormulario_JSON
    @RequestMapping(method = RequestMethod.POST, value = "/caepp")
    public void agregarFormulario_JSON_CAEPP_Inscripcion(@RequestBody final Formulario_Inscripcion formulario) {
        try {
            if (formulario == null) {
                throw new Exception("Objeto null");
            }
            if (formulario != null && !formulario.objetoIncompleto(INSCRIPCION,GUARDAR)) {
                inscripcionService.agregarFormulario_CAEPP_Inscripcion(formulario);
            }
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/formulario/caepp
    // Anotacion @RequestMapping mapea el metodo HTTP.PUT al metodo
    // actualizarFormulario_JSON
    @RequestMapping(method = RequestMethod.PUT, value = "/caepp")
    public void actualizarFormulario_JSON_CAEPP_Inscripcion(@RequestBody final Formulario_Inscripcion formulario) {
        try {
            if (formulario == null) {
                throw new Exception("Objeto null");
            }
            if (!formulario.objetoIncompleto(INSCRIPCION,ACTUALIZAR)) {
                inscripcionService.actualizarFormulario_CAEPP_Inscripcion(formulario);
            }
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/formulario/caepp/{idenEstudiante}
    // Anotacion @RequestMapping mapea el metodo HTTP.DELETE al metodo
    // eliminarFormulario_JSON_CAEPP
    @RequestMapping(method = RequestMethod.DELETE, value = "/caepp/{idenEstudiante}")
    public void eliminarFormulario_JSON_CAEPP_Inscripcion(@PathVariable final String idenEstudiante) {
        try {
            if (idenEstudiante == null || idenEstudiante.isBlank()) {
                throw new Exception("Identificacion vacio");
            }
            inscripcionService.eliminarFormularioPorIdentificacion_CAEPP_Inscripcion(idenEstudiante);
        } catch (final Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }
}
//Fin clase InscripcionControlador
