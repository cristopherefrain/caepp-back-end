package api.API_formulario_inscripcion;

import api.detalle_correo.CorreoServicio;
import api.detalle_correo.Correo_Electronico;
import api.detalle_direccion.DireccionServicio;
import api.detalle_direccion.Direccion_Geografica;
import api.detalle_formulario_inscripcion.FormularioServicio;
import api.detalle_formulario_inscripcion.Formulario_Inscripcion;
import api.detalle_telefono.Telefono;
import api.detalle_telefono.TelefonoServicio;
import api.estudiante.Estudiante;
import api.estudiante.EstudianteServicio;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase InscripcionServicio
public final class InscripcionServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    FormularioServicio formularioService;
    @Autowired
    EstudianteServicio estudianteService;
    @Autowired
    CorreoServicio correoService;
    @Autowired
    DireccionServicio direccionService;
    @Autowired
    TelefonoServicio telefonoService;

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    public Formulario_Inscripcion getFormularioPorIdentificacion_CGI_Inscripcion(final String idenEstudiante)
            throws Exception {
        final int spridenEstudiante = estudianteService.getSpridenPidmEstudiante_CGI(idenEstudiante);

        final CompletableFuture<Formulario_Inscripcion> formularioAsync = formularioService.getFormularioPorSpridenPIDM_CGI(spridenEstudiante);
        final CompletableFuture<Estudiante> estudianteASYNC = estudianteService.getEstudiantePorSpridenPIDM_CGI(spridenEstudiante);
        final CompletableFuture<HashMap<Integer, Correo_Electronico>> correoASYNC = correoService.getDetalleCorreos_CGI(spridenEstudiante);
        final CompletableFuture<HashMap<Integer, Direccion_Geografica>> direccionesASYNC = direccionService.getDetalleDirecciones_CGI(spridenEstudiante);
        final CompletableFuture<HashMap<Integer, Telefono>> telefonoASYNC = telefonoService.getDetalleTelefonos_CGI(spridenEstudiante);

        CompletableFuture.allOf(formularioAsync, estudianteASYNC, correoASYNC, direccionesASYNC, telefonoASYNC).join();

        final Formulario_Inscripcion formulario = formularioAsync.get();
        final Estudiante estudiante = estudianteASYNC.get();

        formulario.setSpriden_pidm(estudiante.getSpriden_pidm());
        formulario.setEstudiante(estudiante);
        formulario.setDetalle_correos(correoASYNC.get());
        formulario.setDetalle_direcciones(direccionesASYNC.get());
        formulario.setDetalle_telefonos(telefonoASYNC.get());

        return formulario;
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public List<Formulario_Inscripcion> getAllFormularios_CAEPP_Inscripcion() throws Exception {
        final List<Formulario_Inscripcion> formularios = formularioService.getAllFormularios_CAEPP().get();
        CompletableFuture<Estudiante> estudianteASYNC;
        CompletableFuture<HashMap<Integer, Correo_Electronico>> correoASYNC;
        CompletableFuture<HashMap<Integer, Direccion_Geografica>> direccionesASYNC;
        CompletableFuture<HashMap<Integer, Telefono>> telefonoASYNC;
        for (final Formulario_Inscripcion formulario : formularios) {
            estudianteASYNC = estudianteService.getEstudiantePorSpridenPIDM_CAEPP(formulario.getSpriden_pidm());
            correoASYNC = correoService.getDetalleCorreos_CAEPP(formulario.getSpriden_pidm());
            direccionesASYNC = direccionService.getDetalleDirecciones_CAEPP(formulario.getSpriden_pidm());
            telefonoASYNC = telefonoService.getDetalleTelefonos_CAEPP(formulario.getSpriden_pidm());

            CompletableFuture.allOf(estudianteASYNC, correoASYNC, direccionesASYNC, telefonoASYNC).join();

            formulario.setEstudiante(estudianteASYNC.get());
            formulario.setDetalle_correos(correoASYNC.get());
            formulario.setDetalle_direcciones(direccionesASYNC.get());
            formulario.setDetalle_telefonos(telefonoASYNC.get());
        }
        return formularios;
    }

    public List<Formulario_Inscripcion> getFormulariosFiltro_CAEPP_Inscripcion(final String idenEstudiante) throws Exception {
        final List<Formulario_Inscripcion> formularios = formularioService.getFormulariosFiltro_CAEPP(idenEstudiante).get();
        CompletableFuture<Estudiante> estudianteASYNC;
        CompletableFuture<HashMap<Integer, Correo_Electronico>> correoASYNC;
        CompletableFuture<HashMap<Integer, Direccion_Geografica>> direccionesASYNC;
        CompletableFuture<HashMap<Integer, Telefono>> telefonoASYNC;
        for (final Formulario_Inscripcion formulario : formularios) {
            estudianteASYNC = estudianteService.getEstudiantePorSpridenPIDM_CAEPP(formulario.getSpriden_pidm());
            correoASYNC = correoService.getDetalleCorreos_CAEPP(formulario.getSpriden_pidm());
            direccionesASYNC = direccionService.getDetalleDirecciones_CAEPP(formulario.getSpriden_pidm());
            telefonoASYNC = telefonoService.getDetalleTelefonos_CAEPP(formulario.getSpriden_pidm());

            CompletableFuture.allOf(estudianteASYNC, correoASYNC, direccionesASYNC, telefonoASYNC).join();

            formulario.setEstudiante(estudianteASYNC.get());
            formulario.setDetalle_correos(correoASYNC.get());
            formulario.setDetalle_direcciones(direccionesASYNC.get());
            formulario.setDetalle_telefonos(telefonoASYNC.get());
        }
        return formularios;
    }

    public Formulario_Inscripcion getFormularioPorIdentificacion_CAEPP_Inscripcion(final String idenEstudiante)
            throws Exception {
        final int spridenEstudiante = estudianteService.getSpridenPidmEstudiante_CAEPP(idenEstudiante);

        final CompletableFuture<Formulario_Inscripcion> formularioAsync = formularioService.getFormularioPorSpridenPIDM_CAEPP(spridenEstudiante);
        final CompletableFuture<Estudiante> estudianteASYNC = estudianteService.getEstudiantePorSpridenPIDM_CAEPP(spridenEstudiante);
        final CompletableFuture<HashMap<Integer, Correo_Electronico>> correoASYNC = correoService.getDetalleCorreos_CAEPP(spridenEstudiante);
        final CompletableFuture<HashMap<Integer, Direccion_Geografica>> direccionesASYNC = direccionService.getDetalleDirecciones_CAEPP(spridenEstudiante);
        final CompletableFuture<HashMap<Integer, Telefono>> telefonoASYNC = telefonoService.getDetalleTelefonos_CAEPP(spridenEstudiante);

        CompletableFuture.allOf(formularioAsync, estudianteASYNC, correoASYNC, direccionesASYNC, telefonoASYNC).join();

        final Formulario_Inscripcion formulario = formularioAsync.get();

        formulario.setEstudiante(estudianteASYNC.get());
        formulario.setDetalle_correos(correoASYNC.get());
        formulario.setDetalle_direcciones(direccionesASYNC.get());
        formulario.setDetalle_telefonos(telefonoASYNC.get());
        return formulario;
    }

    // CRUD Formulario_Inscripcion
    public Boolean agregarFormulario_CAEPP_Inscripcion(final Formulario_Inscripcion formulario) throws Exception {
        CompletableFuture<Boolean> addEstudiante = estudianteService.agregarEstudiante_CAEPP(formulario.getEstudiante());
        CompletableFuture.allOf(addEstudiante).join();

        CompletableFuture<Boolean> addFormulario = formularioService.agregarFormulario_CAEPP(formulario);
        CompletableFuture.allOf(addFormulario).join();

        CompletableFuture<Boolean> addCorreos = correoService.agregarDetalleCorreos_CAEPP(formulario.getDetalle_correos());
        CompletableFuture<Boolean> addDirecciones = direccionService.agregarDetalleDirecciones_CAEPP(formulario.getDetalle_direcciones());
        CompletableFuture<Boolean> addTelefonos = telefonoService.agregarDetalleTelefonos_CAEPP(formulario.getDetalle_telefonos());

        CompletableFuture.allOf(addCorreos, addDirecciones, addTelefonos).join();

        return addEstudiante.get() && addFormulario.get() && addCorreos.get() && addDirecciones.get() && addTelefonos.get();
    }

    public Boolean actualizarFormulario_CAEPP_Inscripcion(final Formulario_Inscripcion formulario) throws Exception {
        CompletableFuture<Boolean> updateEstudiante = estudianteService.actualizarEstudiante_CAEPP(formulario.getEstudiante());
        CompletableFuture<Boolean> updateFormulario = formularioService.actualizarFormulario_CAEPP(formulario);
        CompletableFuture<Boolean> updateCorreos = correoService.actualizarDetalleCorreos_CAEPP(formulario.getDetalle_correos());
        CompletableFuture<Boolean> updateDirecciones = direccionService.actualizarDetalleDirecciones_CAEPP(formulario.getDetalle_direcciones());
        CompletableFuture<Boolean> updateTelefonos = telefonoService.actualizarDetalleTelefonos_CAEPP(formulario.getDetalle_telefonos());

        CompletableFuture.allOf(updateEstudiante, updateFormulario, updateCorreos, updateDirecciones, updateTelefonos).join();
        return updateEstudiante.get() && updateFormulario.get() && updateCorreos.get() && updateDirecciones.get() && updateTelefonos.get();
    }

    public Boolean eliminarFormularioPorIdentificacion_CAEPP_Inscripcion(final String idenEstudiante) throws Exception {
        CompletableFuture<Boolean> deleteCorreos = correoService.eliminarDetalleCorreosPorIdentificacion_CAEPP(idenEstudiante);
        CompletableFuture<Boolean> deleteDirecciones = direccionService.eliminarDetalleDireccionesPorIdentificacion_CAEPP(idenEstudiante);
        CompletableFuture<Boolean> deleteTelefonos = telefonoService.eliminarDetalleTelefonosPorIdentificacion_CAEPP(idenEstudiante);
        CompletableFuture.allOf(deleteCorreos, deleteDirecciones, deleteTelefonos).join();

        CompletableFuture<Boolean> deleteFormulario = formularioService.eliminarFormularioPorIdentificacion_CAEPP(idenEstudiante);
        CompletableFuture.allOf(deleteFormulario).join();

        CompletableFuture<Boolean> deleteEstudiante = estudianteService.eliminarEstudiantePorIdentificacion_CAEPP(idenEstudiante);
        CompletableFuture.allOf(deleteEstudiante).join();

        return deleteEstudiante.get() && deleteFormulario.get() && deleteCorreos.get() && deleteDirecciones.get() && deleteTelefonos.get();
    }
}
// Fin clase InscripcionServicio
