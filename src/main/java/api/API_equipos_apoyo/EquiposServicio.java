package api.API_equipos_apoyo;

import api.detalle_equipo_apoyo.EquipoServicio;
import api.detalle_equipo_apoyo.Equipo_Apoyo;
import api.detalle_formulario_inscripcion.FormularioServicio;
import api.detalle_formulario_inscripcion.Formulario_Inscripcion;
import api.estudiante.Estudiante;
import api.estudiante.EstudianteServicio;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase EquiposServicio
public class EquiposServicio {

// Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    FormularioServicio formularioService;
    @Autowired
    EstudianteServicio estudianteService;
    @Autowired
    EquipoServicio equipoService;

//===================================================   SISTEMA CAEPP   ======================================================
    public Formulario_Inscripcion getFormularioPorIdentificacion_CAEPP_Equipos(String idenEstudiante) throws Exception {
        final int spridenEstudiante = estudianteService.getSpridenPidmEstudiante_CAEPP(idenEstudiante);

        final CompletableFuture<Formulario_Inscripcion> formularioAsync = formularioService.getFormularioPorSpridenPIDM_CAEPP(spridenEstudiante);
        final CompletableFuture<Estudiante> estudianteASYNC = estudianteService.getEstudiantePorSpridenPIDM_CAEPP(spridenEstudiante);
        final CompletableFuture<HashMap<Integer, Equipo_Apoyo>> equiposASYNC = equipoService.getDetalleEquipos_CAEPP(spridenEstudiante);

        CompletableFuture.allOf(formularioAsync, estudianteASYNC, equiposASYNC).join();

        final Formulario_Inscripcion formulario = formularioAsync.get();
        final Estudiante estudiante = estudianteASYNC.get();

        formulario.setSpriden_pidm(spridenEstudiante);
        formulario.setEstudiante(estudiante);
        formulario.setDetalle_equipos_apoyo(equiposASYNC.get());
        return formulario;
    }

    // CRUD Ajustes_Metodologicos
    public Boolean agregarFormulario_CAEPP_Equipos(Formulario_Inscripcion formulario) throws Exception {
        CompletableFuture<Boolean> addEquipos = equipoService.agregarDetalleEquipos_CAEPP(formulario.getDetalle_equipos_apoyo());
        return addEquipos.get();
    }

    public Boolean actualizarFormulario_CAEPP_Equipos(Formulario_Inscripcion formulario) throws Exception {
        CompletableFuture<Boolean> updateEquipos = equipoService.actualizarDetalleEquipos_CAEPP(formulario.getDetalle_equipos_apoyo());
        return updateEquipos.get();
    }

    public Boolean eliminarFormularioPorIdentificacion_CAEPP_Equipos(String idenEstudiante) throws Exception {
        CompletableFuture<Boolean> deleteEquipos = equipoService.eliminarDetalleEquiposPorIdentificacion_CAEPP(idenEstudiante);
        return deleteEquipos.get();
    }
//===================================================   SISTEMA CAEPP   ======================================================
}
// Fin clase EquiposServicio
