package api.usuario;

import api.funcionario.Funcionario;
import static api.modelos.ConstantesGlobales.CARGAR;
import static api.modelos.ConstantesGlobales.CREAR;
import static api.modelos.ConstantesGlobales.GUARDAR;
import java.util.Objects;

//Inicio de la clase Usuario
public final class Usuario {

    // Atributos
    private int id_usuario;
    private String identificacion;
    private int spriden_pidm;
    private int id_tipo_rol;

    private String titulo_tipo_rol;

    private Funcionario funcionario;

    public Usuario() {
        this.id_usuario = 0;
        this.identificacion = "";
        this.spriden_pidm = 0;
        this.id_tipo_rol = 0;
        this.titulo_tipo_rol = "";
        this.funcionario = new Funcionario();
    }

    public Usuario(final int id_usuario, final String identificacion, final int spriden_pidm, final int id_rol, final String titulo_tipo_rol, final Funcionario funcionario) {
        this.id_usuario = id_usuario;
        this.identificacion = identificacion;
        this.spriden_pidm = spriden_pidm;
        this.id_tipo_rol = id_rol;
        this.titulo_tipo_rol = titulo_tipo_rol;
        this.funcionario = funcionario;
    }

    public Usuario(final int id_usuario, final String identificacion, final int spriden_pidm, final int id_rol, final String titulo_tipo_rol) {
        this.id_usuario = id_usuario;
        this.identificacion = identificacion;
        this.spriden_pidm = spriden_pidm;
        this.id_tipo_rol = id_rol;
        this.titulo_tipo_rol = titulo_tipo_rol;
        this.funcionario = new Funcionario();
    }

    public void setId_usuario(final int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public void setIdentificacion(final String identificacion) {
        this.identificacion = identificacion;
    }

    public void setSpriden_pidm(final int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public void setId_tipo_rol(final int id_tipo_rol) {
        this.id_tipo_rol = id_tipo_rol;
    }

    public void setTitulo_tipo_rol(String titulo_tipo_rol) {
        this.titulo_tipo_rol = titulo_tipo_rol;
    }

    public void setFuncionario(final Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public int getId_tipo_rol() {
        return id_tipo_rol;
    }

    public String getTitulo_tipo_rol() {
        return titulo_tipo_rol;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id_usuario=" + id_usuario + ", identificacion=" + identificacion + ", spriden_pidm=" + spriden_pidm + ", id_tipo_rol=" + id_tipo_rol + ", titulo_tipo_rol=" + titulo_tipo_rol + ", funcionario=" + funcionario + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + this.id_usuario;
        hash = 13 * hash + Objects.hashCode(this.identificacion);
        hash = 13 * hash + this.spriden_pidm;
        hash = 13 * hash + this.id_tipo_rol;
        hash = 13 * hash + Objects.hashCode(this.funcionario);
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.id_usuario != other.id_usuario) {
            return false;
        }
        if (this.spriden_pidm != other.spriden_pidm) {
            return false;
        }
        if (this.id_tipo_rol != other.id_tipo_rol) {
            return false;
        }
        if (!Objects.equals(this.identificacion, other.identificacion)) {
            return false;
        }
        if (!Objects.equals(this.funcionario, other.funcionario)) {
            return false;
        }
        return true;
    }

    public Boolean objetoIncompleto(final int opcion) throws Exception {
        switch (opcion) {
            case CARGAR: {
                if (id_usuario == 0) {
                    throw new Exception("id_usuario default");
                }
            }
            case GUARDAR: {
                if (id_tipo_rol == 0) {
                    throw new Exception("id_rol default");
                }
            }
            case CREAR: {
                if (identificacion.isBlank()) {
                    throw new Exception("identificacion default");
                }
                if (spriden_pidm == 0) {
                    throw new Exception("spriden_pidm default");
                }
                funcionario.objetoIncompleto();
            }
            break;
            default:
                return true;
        }
        return false;
    }

}
// Fin de la clase Usuario
