package api.usuario;

import api.modelos.Repositorio_Principal;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion 
//(Capa de Persistencia)
@Repository
// Inicio clase UsuarioRepositorio
public class UsuarioRepositorio extends Repositorio_Principal {

    // =================================================== SISTEMA CAEPP
    // ======================================================
    // Metodo para construir un objeto Usuario apartir de una tupla de la base de
    // datos
    private Usuario crearUsuario_CAEPP(final CachedRowSet rowset) throws Exception {
        Integer id_usuario = null;
        String identificacion = null;
        Integer spriden_pidm = null;
        Integer id_rol = null;

        String titulo_tipo_rol = null;
        try {
            id_usuario = rowset.getInt("ID_USUARIO");
            identificacion = rowset.getString("IDENTIFICACION");
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            id_rol = rowset.getInt("ID_ROL");

            titulo_tipo_rol = rowset.getString("DESCRIPCION_ROL");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        validarCampo(id_usuario, "ID_USUARIO");
        validarCampo(identificacion, "IDENTIFICACION");
        validarCampo(spriden_pidm, "SPRIDEN_PIDM");
        validarCampo(id_rol, "ID_ROL");

        validarCampo(titulo_tipo_rol, "DESCRIPCION_ROL");

        return new Usuario(id_usuario, identificacion, spriden_pidm, id_rol, titulo_tipo_rol, null);
    }

    public List<Usuario> getAllUsuarios_CAEPP() throws Exception {
        String Query = "SELECT ID_USUARIO, IDENTIFICACION, SPRIDEN_PIDM, ID_ROL"
                + ", DESCRIPCION_ROL "
                + "FROM USUARIO NATURAL "
                + "JOIN ROL ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var usuarios = new ArrayList<Usuario>();
        Usuario usuario = null;
        while (rowset.next()) {
            usuario = crearUsuario_CAEPP(rowset);// Agrega cada registro que devuelve la ejecucion del Query
            usuarios.add(usuario);
        }
        return usuarios;
    }

    public List<Usuario> getUsuariosFiltro_CAEPP(final String idenUsuario) throws Exception {
        String Query = "SELECT ID_USUARIO, IDENTIFICACION, SPRIDEN_PIDM, ID_ROL"
                + ", DESCRIPCION_ROL "
                + "FROM USUARIO NATURAL "
                + "JOIN ROL "
                + "WHERE IDENTIFICACION LIKE '%s%%' ";
        // + "AND ESTADO = 'A'";
        Query = String.format(Query, idenUsuario);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var usuarios = new ArrayList<Usuario>();
        while (rowset.next()) {
            usuarios.add(crearUsuario_CAEPP(rowset));// Agrega cada registro que devuelve la ejecucion del Query
        }
        return usuarios;
    }

    public Usuario getUsuarioPorIdentificacion_CAEPP(final String idenUsuario) throws Exception {
        String Query = "SELECT ID_USUARIO, IDENTIFICACION, SPRIDEN_PIDM, ID_ROL"
                + ", DESCRIPCION_ROL "
                + "FROM USUARIO NATURAL "
                + "JOIN ROL "
                + "WHERE IDENTIFICACION = '%s' ";
        // + "AND ESTADO = 'A'";
        Query = String.format(Query, idenUsuario);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        Usuario usuario = null;
        if (rowset.next()) {
            usuario = crearUsuario_CAEPP(rowset);// Agrega el primer registro que devuelve la ejecucion del Query
        }
        if (usuario == null) {
            throw new Exception("No existe un Usuario con la identificacion " + idenUsuario + " registrado en el sistema");
        }
        return usuario;
    }

    // CRUD Usuario
    public Boolean agregarUsuario_CAEPP(final Usuario usuario) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_USU(PIDENTIFICACION IN VARCHAR2, PSPRIDEN_PIDM IN
        // INT, PID_ROL IN INT)
        String Query = "{call PRC_CAEPP_INS_USU('%s',%d,%d)}";
        Query = String.format(Query, usuario.getIdentificacion(), usuario.getSpriden_pidm(), usuario.getId_tipo_rol());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarUsuario_CAEPP(final Usuario usuario) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_USU(PID_USUARIO IN INT, PIDENTIFICACION IN VARCHAR2
        // ,PSPRIDEN_PIDM IN INT, PID_ROL IN INT)
        // UNA_PSICOL.FUN_ID_USU_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_ACT_USU(%d,'%s',%d,%d)}";
        Query = String.format(Query, usuario.getId_usuario(), usuario.getIdentificacion(), usuario.getSpriden_pidm(),
                usuario.getId_tipo_rol());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarUsuarioPorIdentificacion_CAEPP(final String idenUsuario) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_USU(PID_USUARIO IN INT)
        // UNA_PSICOL.FUN_ID_USU_CAEPP_IDE(PIDENTIFICACION IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ELI_USU(FUN_ID_USU_CAEPP_IDE('%s'))}";
        Query = String.format(Query, idenUsuario);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================

    public void purgar_db() throws Exception {
        String Query = "call PRC_CAEPP_PURGAR()";
        Query = String.format(Query);
        this.callableQuery(Query);
    }

}
// Fin clase UsuarioRepositorio
