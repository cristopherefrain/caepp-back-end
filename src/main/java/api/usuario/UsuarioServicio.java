package api.usuario;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase UsuarioServicio
public class UsuarioServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    UsuarioRepositorio usuarioRepo;
    Usuario modelo;

    // =================================================== SISTEMA CAEPP
    // ======================================================
    @Async
    public CompletableFuture<List<Usuario>> getAllUsuarios_CAEPP() throws Exception {
        final List<Usuario> usuarios = usuarioRepo.getAllUsuarios_CAEPP();
        return CompletableFuture.completedFuture(usuarios);
    }

    @Async
    public CompletableFuture<List<Usuario>> getUsuariosFiltro_CAEPP(final String idenUsuario) throws Exception {
        final List<Usuario> usuarios = usuarioRepo.getUsuariosFiltro_CAEPP(idenUsuario);
        return CompletableFuture.completedFuture(usuarios);
    }

    @Async
    public CompletableFuture<Usuario> getUsuarioPorIdentificacion_CAEPP(final String idenUsuario) throws Exception {
        modelo = usuarioRepo.getUsuarioPorIdentificacion_CAEPP(idenUsuario);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Usuario
    @Async
    public CompletableFuture<Boolean> agregarUsuario_CAEPP(final Usuario usuario) throws Exception {
        final Boolean var = usuarioRepo.agregarUsuario_CAEPP(usuario);
        return CompletableFuture.completedFuture(var);
    }

    @Async
    public CompletableFuture<Boolean> actualizarUsuario_CAEPP(final Usuario usuario) throws Exception {
        Boolean var = true;
        if (modelo == null || !modelo.equals(usuario)) {
            modelo = usuario;
            var = usuarioRepo.actualizarUsuario_CAEPP(usuario);
        }
        return CompletableFuture.completedFuture(var);
    }

    @Async
    public CompletableFuture<Boolean> eliminarUsuarioPorIdentificacion_CAEPP(final String idenUsuario) throws Exception {
        modelo = null;
        final Boolean var = usuarioRepo.eliminarUsuarioPorIdentificacion_CAEPP(idenUsuario);
        return CompletableFuture.completedFuture(var);
    }

    // =================================================== SISTEMA CAEPP ======================================================
    public void purgar_db() throws Exception {
        usuarioRepo.purgar_db();
    }
}
// Fin clase UsuarioServicio
