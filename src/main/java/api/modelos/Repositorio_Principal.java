package api.modelos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion (Capa de Persistencia)
@Repository
//Inicio clase Repositorio_Principal
public class Repositorio_Principal {

//Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia cuando crea la instancia
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public CachedRowSet executeQuery(String Query) throws Exception {
        try {
            //Se obtiene una conexion a la base de datos con los parametros del archivo application.properties
            Connection conexion = jdbcTemplate.getDataSource().getConnection();
            System.out.println(Query);
//Prepara el Query para ejecutarlo
            PreparedStatement pstmt = conexion.prepareStatement(Query);
//Ejecuta el Query tipo Select
            ResultSet rs = pstmt.executeQuery();
//Crea un CachedRowSet para almacenar la data en memoria y poder cerrar la conexion
            RowSetFactory factory = RowSetProvider.newFactory();
            CachedRowSet rowset = factory.createCachedRowSet();
//Almacena la data del ResultSet
            rowset.populate(rs);

//Cierra la conexion luego de ejecutar el Query y haber usado el ResultSet
            conexion.close();
            pstmt.close();
//Devuelve las tuplas de la base de datos en un Rowset
            return rowset;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo ejecutar el Query (Revizar Consola Server)");
        }
    }

    public Boolean callableQuery(String Query) throws Exception {
        try {
//Se obtiene una conexion a la base de datos con los parametros del archivo application.properties
            Connection conexion = jdbcTemplate.getDataSource().getConnection();
            System.out.println(Query);
//Prepara el Query para ejecutarlo
            CallableStatement cstmt = conexion.prepareCall(Query);
//Ejecuta el Query tipo PROCEDIMIENTO ALMACENADO y devuelve la cantidad de tuplas afectadas
            int rowsChanged = cstmt.executeUpdate();
//Cierra la conexion luego de ejecutar el Query
            conexion.close();
            cstmt.close();
//Si la cantidad de tuplas afectadas fue distinto de 0 quiere decir que si se ejecuto corretamente
            return rowsChanged != 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo ejecutar el Query (Revizar Consola Server)");
        }
    }

    public void validarCampo(Object objeto, String campo) throws Exception {
        String msj = null;
        if (objeto == null) {
            msj = "Sin cargar " + campo;
        } else {
            if (objeto instanceof Integer) {
                if ((Integer) objeto == 0) {
                    msj = "Sin cargar " + campo;
                }
            }
            if (objeto instanceof String) {
                if (((String) objeto).isBlank()) {
                    msj = "Sin cargar " + campo;
                }
            }
        }
        if (msj != null) {
            throw new Exception(msj);
        }
    }

}
//Fin clase Repositorio_Principal
