package api.modelos;

//Inicio clase ConstantesGlobales
public final class ConstantesGlobales {

    public static final int ESTUDIANTE = 1;
    public static final int FUNCIONARIO = 2;

    public static final int CREAR = 1; //Cuando se va a mandar vacio al front para crearlo y agregarle mas informacion
    public static final int GUARDAR = 2; //Cuando regresa del front con la informacion completa para guardarlo en la base
    public static final int ACTUALIZAR = 3;
    public static final int CARGAR = 4; //Cuando se carga toda informacion de la base de datos
    
    public static final int INSCRIPCION = 1;
    public static final int INFORME = 2;
    public static final int EQUIPOS = 3;

//    Roles del Sistema
    public static final int ADMINISTRADOR = 1;
    public static final int TECNICO = 2;
    public static final int PSICOPEDAGOGA = 3;
//    Tipos de Direccion Geografica
    public static final int DIR_PROCEDENCIA = 1;
    public static final int DIR_RESIDENCIA = 2;
//    Tipos de Direccion Telefono
    public static final int CON_ESTUDIANTE = 1;
    public static final int BEC_ESTUDIANTE = 2;
    public static final int CON_PERSONAL = 3;
    public static final int CON_FAMILIAR = 4;
//    Tipos de Correo Electronico
    public static final int COR_INSTITUCIONAL = 1;
    public static final int COR_CONTACTO = 2;
    public static final int COR_PERSONAL = 3;

//    Tipos de Estado
    public static final int NUEVO = 1;
    public static final int SEGUIMIENTO = 2;
    public static final int INACTIVO = 3;
//    Tipo de Nivel
    public static final int NIVEL_I = 1;
    public static final int NIVEL_II = 2;
    public static final int NIVEL_III = 3;
    public static final int NIVEL_IV = 4;
    public static final int NIVEL_V = 5;
//    Tipo de Ciclo
    public static final int CICLO_I = 1;
    public static final int CICLO_II = 2;
    public static final int CICLO_III = 3;
//    Tipo de Genero
    public static String MASCULINO = "M";
    public static String FEMENINO = "F";

}
//Fin clase ConstantesGlobales
