package api.modelos;

//Inicio clase Persona
public class Persona {

    // Atributos
    protected int spriden_pidm;
    protected String identificacion;
    protected String nombre;
    protected String apellidos;

    public Persona() {
        this.spriden_pidm = -1;
        this.identificacion = "";
        this.nombre = "";
        this.apellidos = "";
    }

    public Persona(int spriden_pidm, String identificacion, String nombre, String apellidos) {
        this.spriden_pidm = spriden_pidm;
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellidos = apellidos;
    }

    public void setSpriden_pidm(int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    @Override
    public String toString() {
        return "Persona{" + " spriden_pidm=" + spriden_pidm + ", identificacion=" + identificacion + ", nombre="
                + nombre + ", apellidos=" + apellidos + '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((apellidos == null) ? 0 : apellidos.hashCode());
        result = prime * result + ((identificacion == null) ? 0 : identificacion.hashCode());
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        result = prime * result + spriden_pidm;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Persona other = (Persona) obj;
        if (apellidos == null) {
            if (other.apellidos != null)
                return false;
        } else if (!apellidos.equals(other.apellidos))
            return false;
        if (identificacion == null) {
            if (other.identificacion != null)
                return false;
        } else if (!identificacion.equals(other.identificacion))
            return false;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        if (spriden_pidm != other.spriden_pidm)
            return false;
        return true;
    }

    public Boolean objetoIncompleto() throws Exception {
        if (spriden_pidm == -1) {
            throw new Exception("spriden_pidm default");
        }
        if (identificacion.isBlank()) {
            throw new Exception("identificacion default");
        }
        if (nombre.isBlank()) {
            throw new Exception("nombre default");
        }
        if (apellidos.isBlank()) {
            throw new Exception("apellidos default");
        }
        return false;
    }
}
// Fin clase Persona
