package api.API_combobox;

import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

//Anotacion @RestController indica a Spring Boot que la clase es un controlador web que maneja los Request HTTP
@RestController
@RequestMapping("/combobox")
//Inicio clase ComboControlador
public final class ComboControlador {

//Anotacion @Autowire indica a Spring Boot que debe inyectar una intancia cuando crea el controlador
    @Autowired
    ComboServicio comboService;

    // url: http://localhost:9898/combobox/getTiposRol
    //Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getRoles al metodo getAllTiposRol_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposRol")
    public HashMap<Integer, String> getAllTiposRol_JSON() {
        try {
            return comboService.getTiposRol();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposDireccion
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta
    // /getTiposDireccion al metodo getAllTiposDireccion_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposDireccion")
    public HashMap<Integer, String> getAllTiposDireccion_JSON() {
        try {
            return comboService.getTiposDireccion();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposTelefono
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta
    // /getTiposDireccion al metodo getAllTiposTelefono_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposTelefono")
    public HashMap<Integer, String> getAllTiposTelefono_JSON() {
        try {
            return comboService.getTiposTelefono();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposEquipoApoyo
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta
    // /getTiposEquipoApoyo al metodo getAllTiposEquipoApoyo_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposEquipoApoyo")
    public HashMap<Integer, String> getAllTiposEquipoApoyo_JSON() {
        try {
            return comboService.getTiposEquipoApoyo();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposEstadoFormulario
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta
    // /getTiposEstadoFormulario al metodo getAllTiposEstadoFormulario_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposEstadoFormulario")
    public HashMap<Integer, String> getAllTiposEstadoFormulario_JSON() {
        try {
            return comboService.getTiposEstadoFormulario();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposCorreo
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getTiposCorreo
    // al metodo getAllTiposCorreo_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposCorreo")
    public HashMap<Integer, String> getAllTiposCorreo_JSON() {
        try {
            return comboService.getTiposCorreo();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposAjusteMetodologico
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta
    // /getTiposAjusteMetodologico al metodo getAllTiposAjusteMetodologico_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposAjusteMetodologico")
    public HashMap<Integer, String> getAllTiposAjusteMetodologico_JSON() {
        try {
            return comboService.getTiposAjusteMetodologico();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposNivel
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getTiposNivel
    // al metodo getAllTiposNivel_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposNivel")
    public HashMap<Integer, String> getAllTiposNivel_JSON() {
        try {
            return comboService.getTiposNivel();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposCiclo
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getTiposCiclo
    // al metodo getAllTiposNivel_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposCiclo")
    public HashMap<Integer, String> getAllTiposCiclo_JSON() {
        try {
            return comboService.getTiposCiclo();
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/combobox/getTiposCondicionEquipo
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta
    // /getTiposCondicionEquipo al metodo getAllTiposCondicionEquipo_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getTiposCondicionEquipo")
    public HashMap<Integer, String> getAllTiposCondicionEquipo_JSON() {
        try {
            return comboService.getTiposCondicionEquipo();
        } catch (final Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.EXPECTATION_FAILED, "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }
    
    // url: http://localhost:9898/combobox/getDestinatariosCorreo
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta
    // /getTiposCondicionEquipo al metodo getDestinatariosCorreo_JSON
    @RequestMapping(method = RequestMethod.GET, value = "/getDestinatariosCorreo")
    public HashMap<Integer, String> getAllDestinatariosCorreo_JSON() {
        try {
            return comboService.getDestinatariosCorreo();
        } catch (final Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.EXPECTATION_FAILED, "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }
}
//Fin clase ComboControlador
