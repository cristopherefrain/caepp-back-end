package api.API_combobox;

import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
//Inicio clase ComboServicio
public final class ComboServicio {

//Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia cuando crea el servicio
    @Autowired
    ComboRepositorio comboRepo;

    public HashMap<Integer, String> getTiposRol() throws Exception {
        return comboRepo.getTiposRol();
    }

    public HashMap<Integer, String> getTiposDireccion() throws Exception {
        return comboRepo.getTiposDireccion();
    }

    public HashMap<Integer, String> getTiposTelefono() throws Exception {
        return comboRepo.getTiposTelefono();
    }

    public HashMap<Integer, String> getTiposEquipoApoyo() throws Exception {
        return comboRepo.getTiposEquipoApoyo();
    }

    public HashMap<Integer, String> getTiposEstadoFormulario() throws Exception {
        return comboRepo.getTiposEstadoFormulario();
    }

    public HashMap<Integer, String> getTiposCorreo() throws Exception {
        return comboRepo.getTiposCorreo();
    }

    public HashMap<Integer, String> getTiposAjusteMetodologico() throws Exception {
        return comboRepo.getTiposAjusteMetodologico();
    }

    public HashMap<Integer, String> getTiposNivel() throws Exception {
        return comboRepo.getTiposNivel();
    }

    public HashMap<Integer, String> getTiposCiclo() throws Exception {
        return comboRepo.getTiposCiclo();
    }

    public HashMap<Integer, String> getTiposCondicionEquipo() throws Exception {
        return comboRepo.getTiposCondicionEquipo();
    }
    
    public HashMap<Integer, String> getDestinatariosCorreo() throws Exception {
        return comboRepo.getDestinatariosCorreo();
    }
}
//Fin clase ComboServicio
