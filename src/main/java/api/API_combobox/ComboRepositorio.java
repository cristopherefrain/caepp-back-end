package api.API_combobox;

import api.modelos.Repositorio_Principal;
import java.util.HashMap;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion 
//(Capa de Persistencia)
@Repository
// Inicio clase ComboRepositorio
public class ComboRepositorio extends Repositorio_Principal {

    private HashMap<Integer, String> crearCombo(final CachedRowSet rowset, final String columnaPK,
            final String columnaDescripcion) throws Exception {
        final var combo = new HashMap<Integer, String>();
        Integer id_pk = null;
        String descripcion = null;
        while (rowset.next()) {
            try {
                id_pk = rowset.getInt(columnaPK);
                descripcion = rowset.getString(columnaDescripcion);
            } catch (final Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
            }
            validarCampo(id_pk, columnaPK);
            validarCampo(descripcion, columnaDescripcion);

            combo.put(id_pk, descripcion);// Agrega cada registro que devuelve la ejecucion del Query
        }
        if (combo.isEmpty()) {
            throw new Exception("Combo Box vacio");
        }
        return combo;
    }

    public HashMap<Integer, String> getTiposRol() throws Exception {
        String Query = "SELECT ID_ROL, DESCRIPCION_ROL " + "FROM ROL ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_ROL", "DESCRIPCION_ROL");
    }

    public HashMap<Integer, String> getTiposDireccion() throws Exception {
        String Query = "SELECT ID_TIPO_DIRECCION, DESCRIPCION_DIRECCION " + "FROM TIPO_DIRECCION ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_DIRECCION", "DESCRIPCION_DIRECCION");
    }

    public HashMap<Integer, String> getTiposTelefono() throws Exception {
        String Query = "SELECT ID_TIPO_TELEFONO, DESCRIPCION_TELEFONO " + "FROM TIPO_TELEFONO ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_TELEFONO", "DESCRIPCION_TELEFONO");
    }

    public HashMap<Integer, String> getTiposEquipoApoyo() throws Exception {
        String Query = "SELECT ID_TIPO_EQUIPO, DESCRIPCION_EQUIPO " + "FROM TIPO_EQUIPO_APOYO ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_EQUIPO", "DESCRIPCION_EQUIPO");
    }

    public HashMap<Integer, String> getTiposEstadoFormulario() throws Exception {
        String Query = "SELECT ID_TIPO_ESTADO, DESCRIPCION_ESTADO " + "FROM TIPO_ESTADO ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_ESTADO", "DESCRIPCION_ESTADO");
    }

    public HashMap<Integer, String> getTiposCorreo() throws Exception {
        String Query = "SELECT ID_TIPO_CORREO, DESCRIPCION_CORREO " + "FROM TIPO_CORREO ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_CORREO", "DESCRIPCION_CORREO");
    }

    public HashMap<Integer, String> getTiposAjusteMetodologico() throws Exception {
        String Query = "SELECT ID_TIPO_AJUSTE, DESCRIPCION_AJUSTE " + "FROM TIPO_AJUSTE_METODOLOGICO ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_AJUSTE", "DESCRIPCION_AJUSTE");
    }

    public HashMap<Integer, String> getTiposNivel() throws Exception {
        String Query = "SELECT ID_TIPO_NIVEL, DESCRIPCION_NIVEL " + "FROM TIPO_NIVEL ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_NIVEL", "DESCRIPCION_NIVEL");
    }

    public HashMap<Integer, String> getTiposCiclo() throws Exception {
        String Query = "SELECT ID_TIPO_CICLO, DESCRIPCION_CICLO " + "FROM TIPO_CICLO ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_CICLO", "DESCRIPCION_CICLO");
    }

    public HashMap<Integer, String> getTiposCondicionEquipo() throws Exception {
        String Query = "SELECT ID_TIPO_CONDICION, DESCRIPCION_CONDICION " + "FROM TIPO_CONDICION_EQUIPO ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_TIPO_CONDICION", "DESCRIPCION_CONDICION");
    }
    
    public HashMap<Integer, String> getDestinatariosCorreo() throws Exception {
        String Query = "SELECT ID_DESTINATARIO_CORREO, TITULO " + "FROM DESTINATARIO_CORREO ";
        // + "WHERE ESTADO = 'A'";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);
        return crearCombo(rowset, "ID_DESTINATARIO_CORREO", "TITULO");
    }
}
// Fin clase ComboRepositorio
