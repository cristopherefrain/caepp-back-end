package api.detalle_telefono;

import static api.modelos.ConstantesGlobales.BEC_ESTUDIANTE;
import static api.modelos.ConstantesGlobales.CON_ESTUDIANTE;
import static api.modelos.ConstantesGlobales.CON_FAMILIAR;
import static api.modelos.ConstantesGlobales.CON_PERSONAL;
import api.modelos.Repositorio_Principal;
import java.util.HashMap;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion
//(Capa de Persistencia)
@Repository
// Inicio clase TelefonoRepositorio
public class TelefonoRepositorio extends Repositorio_Principal {

    // Metodo para construir un objeto Correo_Electronico apartir de una tupla de la
    // base de datos
    private Telefono crearTelefono(final CachedRowSet rowset) throws Exception {
        Integer id_detalle_telefono = null;
        Integer numero = null;
        Integer spriden_pidm = null;
        String codigo = null;
        try {
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            id_detalle_telefono = rowset.getInt("ID_DETALLE_TELEFONO");
            codigo = rowset.getString("CODIGO");
            numero = rowset.getInt("NUMERO");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        validarCampo(id_detalle_telefono, "ID_DETALLE_TELEFONO");
        validarCampo(codigo, "CODIGO");
        validarCampo(numero, "NUMERO" + codigo);
        validarCampo(spriden_pidm, "SPRIDEN_PIDM" + codigo);

        final Telefono telefono = new Telefono(id_detalle_telefono, numero, spriden_pidm, 0);
        switch (codigo) {
            case "CE":
                telefono.setId_tipo_telefono(CON_ESTUDIANTE);
                break;
            case "MA":
                telefono.setId_tipo_telefono(BEC_ESTUDIANTE);
                break;
            case "CP":
                telefono.setId_tipo_telefono(CON_PERSONAL);
                break;
            case "CF":
                telefono.setId_tipo_telefono(CON_FAMILIAR);
                break;
            default:
                throw new Exception("Error al cargar Telefono, CODIGO desconocido: " + codigo);
        }
        return telefono;
    }

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    public HashMap<Integer, Telefono> getDetalleTelefonos_CGI(final int spridenEstudiante) throws Exception {
        String Query = "SELECT SPRTELE_PIDM SPRIDEN_PIDM, -1 ID_DETALLE_TELEFONO, SPRTELE_TELE_CODE CODIGO, SPRTELE_PHONE_NUMBER NUMERO "
                + "FROM SPRTELE " + "WHERE SPRTELE_PIDM = %d " + "AND SPRTELE_STATUS_IND IS NULL "
                + "AND TRIM(SPRTELE_PHONE_NUMBER) IS NOT NULL";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var telefonos = new HashMap<Integer, Telefono>();
        Telefono telefono;
        Integer key = -1;
        while (rowset.next()) {
            telefono = crearTelefono(rowset);
            telefonos.put(key--, telefono);
        }
        return telefonos;
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public HashMap<Integer, Telefono> getDetalleTelefonos_CAEPP(final int spridenEstudiante) throws Exception {
        String Query = "SELECT DT.ID_DETALLE_TELEFONO,DT.NUMERO_TELEFONO NUMERO,DT.SPRIDEN_PIDM,TT.NOMBRE_TELEFONO CODIGO "
                + "FROM DETALLE_TELEFONO DT, TIPO_TELEFONO TT " + "WHERE DT.SPRIDEN_PIDM = %d "
                + "AND DT.ID_TIPO_TELEFONO = TT.ID_TIPO_TELEFONO ";
        // + "AND DT.ESTADO = 'A' " + "AND TT.ESTADO = 'A'";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var telefonos = new HashMap<Integer, Telefono>();
        Telefono telefono;
        while (rowset.next()) {
            telefono = crearTelefono(rowset);
            telefonos.put(telefono.getId_detalle_telefono(), telefono);
        }
        return telefonos;
    }

    // CRUD Telefonos
    public Boolean agregarDetalleTelefono_CAEPP(final Telefono telefono) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_DET_TEL(PNUMERO_TELEFONO IN INT, PSPRIDEN_PIDM IN
        // INT, PID_FORMULARIO_INSCRIPCION IN INT, PID_TIPO_TELEFONO IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_INS_DET_TEL(%d,%d,FUN_ID_FOR_INS_CAEPP_SPR(%d),%d)}";
        Query = String.format(Query, telefono.getNumero_telefono(), telefono.getSpriden_pidm(),
                telefono.getSpriden_pidm(), telefono.getId_tipo_telefono());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarDetalleTelefono_CAEPP(final Telefono telefono) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_DET_TEL(PID_DETALLE_TELEFONO IN INT,
        // PNUMERO_TELEFONO IN INT, PSPRIDEN_PIDM IN INT, PID_FORMULARIO_INSCRIPCION IN
        // INT,
        // PID_TIPO_TELEFONO IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_ACT_DET_TEL(%d,%d,%d,FUN_ID_FOR_INS_CAEPP_SPR(%d),%d)}";
        Query = String.format(Query, telefono.getId_detalle_telefono(), telefono.getNumero_telefono(),
                telefono.getSpriden_pidm(), telefono.getSpriden_pidm(), telefono.getId_tipo_telefono());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleTelefono_CAEPP(final int id_detalle_telefono) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_DET_TEL(PID_DETALLE_TELEFONO IN INT)
        String Query = "{call PRC_CAEPP_ELI_DET_TEL(%d)}";
        Query = String.format(Query, id_detalle_telefono);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleTelefonosPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_TOD_DET_TEL(PSPRIDEN_PIDM IN INT)
        // UNA_PSICOL.FUN_SPR_EST_CAEPP_IDE(PIDENTIFICACION IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ELI_TOD_DET_TEL(FUN_SPR_EST_CAEPP_IDE('%s'))}";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Inicio clase TelefonoRepositorio
