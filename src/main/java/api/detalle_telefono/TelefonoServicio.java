package api.detalle_telefono;

import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase TelefonoServicio
public class TelefonoServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    TelefonoRepositorio telefonoRepo;
    HashMap<Integer, Telefono> modelo;

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    @Async
    public CompletableFuture<HashMap<Integer, Telefono>> getDetalleTelefonos_CGI(final int spridenEstudiante) throws Exception {
        HashMap<Integer, Telefono> telefonos = telefonoRepo.getDetalleTelefonos_CGI(spridenEstudiante);
        return CompletableFuture.completedFuture(telefonos);
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    @Async
    public CompletableFuture<HashMap<Integer, Telefono>> getDetalleTelefonos_CAEPP(final int spridenEstudiante) throws Exception {
        modelo = telefonoRepo.getDetalleTelefonos_CAEPP(spridenEstudiante);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Telefonos
    @Async
    public CompletableFuture<Boolean> agregarDetalleTelefonos_CAEPP(final HashMap<Integer, Telefono> telefonos) throws Exception {
        for (final Telefono telefono : telefonos.values()) {
            // Agrega todos los Objetos
            if (!telefonoRepo.agregarDetalleTelefono_CAEPP(telefono)) {
                return CompletableFuture.completedFuture(false);
            }
        }
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> actualizarDetalleTelefonos_CAEPP(final HashMap<Integer, Telefono> telefonos) throws Exception {
        Telefono value_telefonos;
        Telefono value_modelo;
        for (final Integer key : modelo.keySet()) {
            value_telefonos = telefonos.get(key);
            // Si no devuelve un Objeto con el id del modelo entonces fue eliminado y se
            // manda a eliminar de la base
            if (value_telefonos == null) {
                if (!telefonoRepo.eliminarDetalleTelefono_CAEPP(key)) {
                    return CompletableFuture.completedFuture(false);
                }
            } else 
            // Si devuelve un Objeto y es diferente al del modelo viejo se manda a
            // actualizar
            if (!value_telefonos.equals(modelo.get(key))) {
                if (!telefonoRepo.actualizarDetalleTelefono_CAEPP(value_telefonos)) {
                    return CompletableFuture.completedFuture(false);
                }
            }
        }
        for (final Integer key : telefonos.keySet()) {
            value_modelo = modelo.get(key);
            // Si no devuelve un Objeto con el id del nuevo modelo entonces fue agregado y
            // hay que insertarlo en la base
            if (value_modelo == null) {
                if (!telefonoRepo.agregarDetalleTelefono_CAEPP(telefonos.get(key))) {
                    return CompletableFuture.completedFuture(false);
                }
            }
        }
        modelo = telefonos;
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> eliminarDetalleTelefonosPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        if (!telefonoRepo.eliminarDetalleTelefonosPorIdentificacion_CAEPP(idenEstudiante)) {
            return CompletableFuture.completedFuture(false);
        }
        modelo = null;
        return CompletableFuture.completedFuture(true);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase TelefonoServicio
