package api.detalle_telefono;

//Inicio clase Telefono
public final class Telefono {

//Atributos
    private int id_detalle_telefono;
    private int numero_telefono;
    private int spriden_pidm;
    private int id_tipo_telefono;

    public Telefono() {
        this.id_detalle_telefono = 0;
        this.numero_telefono = 0;
        this.spriden_pidm = 0;
        this.id_tipo_telefono = 0;
    }

    public Telefono(final int id_detalle_telefono, final int numero_telefono, final int spriden_pidm,
            final int id_tipo_telefono) {
        this.id_detalle_telefono = id_detalle_telefono;
        this.numero_telefono = numero_telefono;
        this.spriden_pidm = spriden_pidm;
        this.id_tipo_telefono = id_tipo_telefono;
    }

    public void setId_detalle_telefono(final int id_detalle_telefono) {
        this.id_detalle_telefono = id_detalle_telefono;
    }

    public void setNumero_telefono(final int numero_telefono) {
        this.numero_telefono = numero_telefono;
    }

    public void setSpriden_pidm(final int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public void setId_tipo_telefono(final int id_tipo_telefono) {
        this.id_tipo_telefono = id_tipo_telefono;
    }

    public int getId_detalle_telefono() {
        return id_detalle_telefono;
    }

    public int getNumero_telefono() {
        return numero_telefono;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public int getId_tipo_telefono() {
        return id_tipo_telefono;
    }

    @Override
    public String toString() {
        return "Telefono{" + "id_detalle_telefono=" + id_detalle_telefono + ", numero_telefono=" + numero_telefono
                + ", spriden_pidm=" + spriden_pidm + ", id_tipo_telefono=" + id_tipo_telefono + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.id_detalle_telefono;
        hash = 41 * hash + this.numero_telefono;
        hash = 41 * hash + this.spriden_pidm;
        hash = 41 * hash + this.id_tipo_telefono;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Telefono other = (Telefono) obj;
        if (this.id_detalle_telefono != other.id_detalle_telefono) {
            return false;
        }
        if (this.numero_telefono != other.numero_telefono) {
            return false;
        }
        if (this.spriden_pidm != other.spriden_pidm) {
            return false;
        }
        if (this.id_tipo_telefono != other.id_tipo_telefono) {
            return false;
        }
        return true;
    }

    public Boolean objetoIncompleto() throws Exception {
        if (id_tipo_telefono == 0) {
            throw new Exception("id_tipo_telefono default");
        }
        if (numero_telefono == 0) {
            throw new Exception("numero_telefono default");
        }
        if (spriden_pidm == 0) {
            throw new Exception("spriden_pidm default");
        }
        if (id_tipo_telefono == 0) {
            throw new Exception("id_tipo_telefono default");
        }
        return false;
    }

}
//Fin clase Telefono
