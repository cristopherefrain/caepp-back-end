package api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public final class HomeController {
    @RequestMapping("/")
    public String home(){
        return "Server says: I'm running :v";
    }
}