package api.API_destinatario_correo;

import api.destinatario_correo.Destinatario_Correo;
import static api.modelos.ConstantesGlobales.CARGAR;
import static api.modelos.ConstantesGlobales.GUARDAR;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

//Anotacion @RestController indica a Spring Boot que la clase es un controlador web que maneja los Request HTTP
@RestController
@RequestMapping("/destinatario")
//Inicio clase DestinatariosControlador
public class DestinatariosControlador {

//Anotacion @Autowire indica a Spring Boot que debe inyectar una intancia cuando crea el controlador
    @Autowired
    DestinatariosServicio DestinatariosService;

    // =================================================== SISTEMA CAEPP
    // ======================================================
    // url: http://localhost:9898/destinatario/caepp/getAll
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getAll al
    // metodo getAllDestinatarios_JSON_CAEPP
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/getAll")
    public List<Destinatario_Correo> getAllDestinatarios_JSON_CAEPP() {
        try {
            final List<Destinatario_Correo> destinatarios = DestinatariosService.getAllDestinatarios_CAEPP();
            return destinatarios;
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 417 EXPECTATION FAILED
        }
    }

    // url: http://localhost:9898/destinatario/caepp/getFilter?titulo=
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /getFilter al
    // metodo getDestinatariosFiltro_JSON_CAEPP
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/getFilter")
    public List<Destinatario_Correo> getDestinatariosFiltro_JSON_CAEPP(@RequestParam final String titulo) {
        try {
            if (titulo != null && !titulo.isBlank()) {
                final List<Destinatario_Correo> destinatarios = DestinatariosService.getDestinatariosFiltro_CAEPP(titulo);
                return destinatarios;
            }
            throw new Exception("Titulo vacio");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/destinatario/caepp/{titulo}
    // Anotacion @RequestMapping mapea el metodo HTTP.GET y la ruta /{titulo}
    // al metodo getDestinatarioPorTitulo_JSON_CAEPP
    @RequestMapping(method = RequestMethod.GET, value = "/caepp/{titulo}")
    public Destinatario_Correo getDestinatarioPorTitulo_JSON_CAEPP(@PathVariable final String titulo) {
        try {
            if (titulo != null && !titulo.isBlank()) {
                final Destinatario_Correo destinatario = DestinatariosService.getDestinatarioPorTitulo_CAEPP(titulo);
                if (destinatario != null && !destinatario.objetoIncompleto(CARGAR)) {
                    return destinatario;
                }
            }
            throw new Exception("titulo vacio");
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }

    // CRUD Usuario
    // url: http://localhost:9898/destinatario/caepp
    // Anotacion @RequestMapping mapea el metodo HTTP.POST al metodo
    // agregarDestinatario_JSON_CAEPP
    @RequestMapping(method = RequestMethod.POST, value = "/caepp")
    public void agregarDestinatario_JSON_CAEPP(@RequestBody final Destinatario_Correo destinatario) {
        try {
            if (destinatario == null) {
                throw new Exception("Objeto null");
            }
            if (!destinatario.objetoIncompleto(GUARDAR)) {
                DestinatariosService.agregarDestinatario_CAEPP(destinatario);
            }
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/destinatario/caepp
    // Anotacion @RequestMapping mapea el metodo HTTP.PUT al metodo
    // actualizarDestinatario_JSON_CAEPP
    @RequestMapping(method = RequestMethod.PUT, value = "/caepp")
    public void actualizarDestinatario_JSON_CAEPP(@RequestBody final Destinatario_Correo destinatario) {
        try {
            if (destinatario == null) {
                throw new Exception("Objeto null");
            }
            if (!destinatario.objetoIncompleto(GUARDAR)) {
                DestinatariosService.actualizarDestinatario_CAEPP(destinatario);
            }
        } catch (final Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "A ocurrido un error inesperado: " + ex.getMessage()); // 400 BAD REQUEST
        }
    }

    // url: http://localhost:9898/destinatario/caepp/{id_destinatario}
    // Anotacion @RequestMapping mapea el metodo HTTP.DELETE al metodo
    // eliminarDestinatarioPorIdentificacion_JSON_CAEPP
    @RequestMapping(method = RequestMethod.DELETE, value = "/caepp/{id_destinatario}")
    public void eliminarDestinatarioPorIdentificacion_JSON_CAEPP(@PathVariable final int id_destinatario) {
        try {
            if (id_destinatario <= 0) {
                throw new Exception("Identificacion vacio");
            }
            DestinatariosService.eliminarDestinatarioPorIdentificacion_CAEPP(id_destinatario);
        } catch (final Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "A ocurrido un error inesperado: " + ex.getMessage()); // 404 NOT FOUND
        }
    }
    //===================================================   SISTEMA CAEPP   ======================================================
}
//Inicio clase DestinatariosControlador
