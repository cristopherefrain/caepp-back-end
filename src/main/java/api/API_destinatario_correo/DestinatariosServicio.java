package api.API_destinatario_correo;

import api.destinatario_correo.DestinatarioServicio;
import api.destinatario_correo.Destinatario_Correo;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase DestinatariosServicio
public class DestinatariosServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    DestinatarioServicio destinatarioService;

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public List<Destinatario_Correo> getAllDestinatarios_CAEPP() throws Exception {
        final List<Destinatario_Correo> destinatarios = destinatarioService.getAllDestinatarios_CAEPP().get();
        return destinatarios;
    }

    public List<Destinatario_Correo> getDestinatariosFiltro_CAEPP(final String titulo) throws Exception {
        final List<Destinatario_Correo> destinatarios = destinatarioService.getDestinatariosFiltro_CAEPP(titulo).get();
        return destinatarios;
    }

    public Destinatario_Correo getDestinatarioPorTitulo_CAEPP(final String titulo) throws Exception {
        final CompletableFuture<Destinatario_Correo> destinatarioASYNC = destinatarioService.getDestinatarioPorTitulo_CAEPP(titulo);

        CompletableFuture.allOf(destinatarioASYNC).join();

        final Destinatario_Correo destinatario = destinatarioASYNC.get();
        return destinatario;
    }

    // CRUD Destinatario_Correo
    public Boolean agregarDestinatario_CAEPP(final Destinatario_Correo destinatario) throws Exception {
        return destinatarioService.agregarDestinatario_CAEPP(destinatario).get();
    }

    public Boolean actualizarDestinatario_CAEPP(final Destinatario_Correo destinatario) throws Exception {
        return destinatarioService.actualizarDestinatario_CAEPP(destinatario).get();
    }

    public Boolean eliminarDestinatarioPorIdentificacion_CAEPP(final int id_destinatario) throws Exception {
        return destinatarioService.eliminarDestinatarioPorIdentificacion_CAEPP(id_destinatario).get();
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase DestinatariosServicio
