package api.destinatario_correo;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase DestinatarioServicio
public class DestinatarioServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    DestinatarioRepositorio destinatarioRepo;
    Destinatario_Correo modelo;

    // =================================================== SISTEMA CAEPP
    // ======================================================
    @Async
    public CompletableFuture<List<Destinatario_Correo>> getAllDestinatarios_CAEPP() throws Exception {
        final List<Destinatario_Correo> destinatarios = destinatarioRepo.getAllDestinatarios_CAEPP();
        return CompletableFuture.completedFuture(destinatarios);
    }

    @Async
    public CompletableFuture<List<Destinatario_Correo>> getDestinatariosFiltro_CAEPP(final String titulo) throws Exception {
        final List<Destinatario_Correo> destinatarios = destinatarioRepo.getDestinatariosFiltro_CAEPP(titulo);
        return CompletableFuture.completedFuture(destinatarios);
    }

    @Async
    public CompletableFuture<Destinatario_Correo> getDestinatarioPorTitulo_CAEPP(final String titulo) throws Exception {
        modelo = destinatarioRepo.getDestinatarioPorTitulo_CAEPP(titulo);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Usuario
    @Async
    public CompletableFuture<Boolean> agregarDestinatario_CAEPP(final Destinatario_Correo destinatario) throws Exception {
        final Boolean var = destinatarioRepo.agregarDestinatario_CAEPP(destinatario);
        return CompletableFuture.completedFuture(var);
    }

    @Async
    public CompletableFuture<Boolean> actualizarDestinatario_CAEPP(final Destinatario_Correo destinatario) throws Exception {
        Boolean var = true;
        if (modelo == null || !modelo.equals(destinatario)) {
            modelo = destinatario;
            var = destinatarioRepo.actualizarDestinatario_CAEPP(destinatario);
        }
        return CompletableFuture.completedFuture(var);
    }

    @Async
    public CompletableFuture<Boolean> eliminarDestinatarioPorIdentificacion_CAEPP(final int id_destinatario) throws Exception {
        modelo = null;
        final Boolean var = destinatarioRepo.eliminarDestinatarioPorIdentificacion_CAEPP(id_destinatario);
        return CompletableFuture.completedFuture(var);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase DestinatarioServicio
