package api.destinatario_correo;


import static api.modelos.ConstantesGlobales.CARGAR;
import static api.modelos.ConstantesGlobales.CREAR;
import static api.modelos.ConstantesGlobales.GUARDAR;
import java.util.Objects;

//Inicio de la clase Destinatario_Correo
public class Destinatario_Correo {

    // Atributos
    private int id_destinatario_correo;
    private String titulo;
    private String direccion_correo;

    public Destinatario_Correo() {
        this.id_destinatario_correo = 0;
        this.titulo = "";
        this.direccion_correo = "";
    }

    public Destinatario_Correo(int id_destinatario_correo, String titulo, String direccion_correo) {
        this.id_destinatario_correo = id_destinatario_correo;
        this.titulo = titulo;
        this.direccion_correo = direccion_correo;
    }

    public int getId_destinatario_correo() {
        return id_destinatario_correo;
    }

    public void setId_destinatario_correo(int id_destinatario_correo) {
        this.id_destinatario_correo = id_destinatario_correo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDireccion_correo() {
        return direccion_correo;
    }

    public void setDireccion_correo(String direccion_correo) {
        this.direccion_correo = direccion_correo;
    }

    @Override
    public String toString() {
        return "Destinatario_Correo{" + "id_destinatario_correo=" + id_destinatario_correo + ", titulo=" + titulo + ", direccion_correo=" + direccion_correo + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id_destinatario_correo;
        hash = 89 * hash + Objects.hashCode(this.titulo);
        hash = 89 * hash + Objects.hashCode(this.direccion_correo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Destinatario_Correo other = (Destinatario_Correo) obj;
        if (this.id_destinatario_correo != other.id_destinatario_correo) {
            return false;
        }
        if (!Objects.equals(this.titulo, other.titulo)) {
            return false;
        }
        if (!Objects.equals(this.direccion_correo, other.direccion_correo)) {
            return false;
        }
        return true;
    }

    public Boolean objetoIncompleto(final int opcion) throws Exception {
        switch (opcion) {
            case CARGAR: {
                if (id_destinatario_correo == 0) {
                    throw new Exception("id_destinatario_correo default");
                }
            }
            case GUARDAR: 
            case CREAR: {
                if (titulo.isBlank()) {
                    throw new Exception("identificacion default");
                }
                if (direccion_correo.isBlank()) {
                    throw new Exception("spriden_pidm default");
                }
            }
            break;
            default:
                return true;
        }
        return false;
    }
}
//Fin de la clase Destinatario_Correo