package api.destinatario_correo;

import api.modelos.Repositorio_Principal;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion 
//(Capa de Persistencia)
@Repository
// Inicio clase DestinatarioRepositorio
public class DestinatarioRepositorio extends Repositorio_Principal {

    // =================================================== SISTEMA CAEPP
    // ======================================================
    // Metodo para construir un objeto Destinatario_Correo apartir de una tupla de la base de
    // datos
    private Destinatario_Correo crearDestinatario_CAEPP(final CachedRowSet rowset) throws Exception {
        Integer id_destinatario_correo = null;
        String titulo = null;
        String direccion_correo = null;

        try {
            id_destinatario_correo = rowset.getInt("ID_DESTINATARIO_CORREO");
            titulo = rowset.getString("TITULO");
            direccion_correo = rowset.getString("DIRECCION_CORREO");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }

        validarCampo(id_destinatario_correo, "ID_DESTINATARIO_CORREO");
        validarCampo(titulo, "TITULO");
        validarCampo(direccion_correo, "DIRECCION_CORREO");
        return new Destinatario_Correo(id_destinatario_correo, titulo, direccion_correo);
    }

    public List<Destinatario_Correo> getAllDestinatarios_CAEPP() throws Exception {
        String Query = "SELECT ID_DESTINATARIO_CORREO, TITULO, DIRECCION_CORREO "
                + "FROM DESTINATARIO_CORREO ";
        Query = String.format(Query);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var destinatarios = new ArrayList<Destinatario_Correo>();
        Destinatario_Correo destinatario = null;
        while (rowset.next()) {
            destinatario = crearDestinatario_CAEPP(rowset);// Agrega cada registro que devuelve la ejecucion del Query
            destinatarios.add(destinatario);
        }
        return destinatarios;
    }

    public List<Destinatario_Correo> getDestinatariosFiltro_CAEPP(final String titulo) throws Exception {
        String Query = "SELECT ID_DESTINATARIO_CORREO, TITULO, DIRECCION_CORREO "
                + "FROM DESTINATARIO_CORREO "
                + "WHERE UPPER(TITULO) LIKE UPPER('%s%%') ";
        Query = String.format(Query, titulo);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var destinatarios = new ArrayList<Destinatario_Correo>();
        while (rowset.next()) {
            destinatarios.add(crearDestinatario_CAEPP(rowset));// Agrega cada registro que devuelve la ejecucion del Query
        }
        return destinatarios;
    }

    public Destinatario_Correo getDestinatarioPorTitulo_CAEPP(final String titulo) throws Exception {
        String Query = "SELECT ID_DESTINATARIO_CORREO, TITULO, DIRECCION_CORREO "
                + "FROM DESTINATARIO_CORREO "
                + "WHERE UPPER(TITULO) = UPPER('%s') ";
        Query = String.format(Query, titulo);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        Destinatario_Correo destinatario = null;
        if (rowset.next()) {
            destinatario = crearDestinatario_CAEPP(rowset);// Agrega el primer registro que devuelve la ejecucion del Query
        }
        if (destinatario == null) {
            throw new Exception("No existe un Destinatario_Correo con el titulo " + titulo + " registrado en el sistema");
        }
        return destinatario;
    }

    // CRUD Destinatario_Correo
    public Boolean agregarDestinatario_CAEPP(final Destinatario_Correo destinatario) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_DES_COR(PTITULO IN VARCHAR2, PDIRECCION_CORREO IN VARCHAR2) IS
        String Query = "{call PRC_CAEPP_INS_DES_COR('%s','%s')}";
        Query = String.format(Query, destinatario.getTitulo(), destinatario.getDireccion_correo());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarDestinatario_CAEPP(final Destinatario_Correo destinatario) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_DES_COR(PID_DESTINATARIO_CORREO IN INT, PTITULO IN VARCHAR2, PDIRECCION_CORREO IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ACT_DES_COR(%d,'%s','%s')}";
        Query = String.format(Query, destinatario.getId_destinatario_correo(), destinatario.getTitulo(), destinatario.getDireccion_correo());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDestinatarioPorIdentificacion_CAEPP(final int id_destinatario) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_DES_COR(PID_DESTINATARIO_CORREO IN INT)
        String Query = "{call PRC_CAEPP_ELI_DES_COR(%d)}";
        Query = String.format(Query, id_destinatario);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
        // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase DestinatarioRepositorio