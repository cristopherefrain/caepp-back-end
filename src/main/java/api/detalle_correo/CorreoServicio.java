package api.detalle_correo;

import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase CorreoServicio
public class CorreoServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    CorreoRepositorio correoRepo;
    HashMap<Integer, Correo_Electronico> modelo;

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    @Async
    public CompletableFuture<HashMap<Integer, Correo_Electronico>> getDetalleCorreos_CGI(final int spridenEstudiante) throws Exception {
        final HashMap<Integer, Correo_Electronico> correos = correoRepo.getDetalleCorreos_CGI(spridenEstudiante);
        return CompletableFuture.completedFuture(correos);
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    @Async
    public CompletableFuture<HashMap<Integer, Correo_Electronico>> getDetalleCorreos_CAEPP(final int spridenEstudiante) throws Exception {
        modelo = correoRepo.getDetalleCorreos_CAEPP(spridenEstudiante);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Correos
    @Async
    public CompletableFuture<Boolean> agregarDetalleCorreos_CAEPP(final HashMap<Integer, Correo_Electronico> correos) throws Exception {
        for (final Correo_Electronico correo : correos.values()) {
            // Agrega todos los Objetos
            if (!correoRepo.agregarDetalleCorreo_CAEPP(correo)) {
                return CompletableFuture.completedFuture(false);
            }
        }
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> actualizarDetalleCorreos_CAEPP(final HashMap<Integer, Correo_Electronico> correos) throws Exception {
        Correo_Electronico value_correos;
        Correo_Electronico value_modelo;
        for (final Integer key : modelo.keySet()) {
            value_correos = correos.get(key);
            // Si no devuelve un Objeto con el id del modelo entonces fue eliminado y se
            // manda a eliminar de la base
            if (value_correos == null) {
                if (!correoRepo.eliminarDetalleCorreo_CAEPP(key)) {
                    return CompletableFuture.completedFuture(false);
                }
            }else
            // Si devuelve un Objeto y es diferente al del modelo viejo se manda a
            // actualizar
            if (!value_correos.equals(modelo.get(key))) {
                if (!correoRepo.actualizarDetalleCorreo_CAEPP(value_correos)) {
                    return CompletableFuture.completedFuture(false);
                }
            }
        }
        if (modelo != null) {
            for (final Integer key : correos.keySet()) {
                value_modelo = modelo.get(key);
                // Si no devuelve un Objeto con el id del nuevo modelo entonces fue agregado y
                // hay que insertarlo en la base
                if (value_modelo == null) {
                    if (!correoRepo.agregarDetalleCorreo_CAEPP(correos.get(key))) {
                        return CompletableFuture.completedFuture(false);
                    }
                }
            }
        }
        modelo = correos;
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> eliminarDetalleCorreosPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        if (!correoRepo.eliminarDetalleCorreosPorIdentificacion_CAEPP(idenEstudiante)) {
            return CompletableFuture.completedFuture(false);
        }
        modelo = null;
        return CompletableFuture.completedFuture(true);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase CorreoServicio
