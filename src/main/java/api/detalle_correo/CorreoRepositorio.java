package api.detalle_correo;

import static api.modelos.ConstantesGlobales.COR_CONTACTO;
import static api.modelos.ConstantesGlobales.COR_INSTITUCIONAL;
import static api.modelos.ConstantesGlobales.COR_PERSONAL;
import api.modelos.Repositorio_Principal;
import java.util.HashMap;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion
//(Capa de Persistencia)
@Repository
// Inicio clase CorreoRepositorio
public class CorreoRepositorio extends Repositorio_Principal {

    // Metodo para construir un objeto Correo_Electronico apartir de una tupla de la
    // base de datos
    private Correo_Electronico crearCorreo(final CachedRowSet rowset) throws Exception {
        Integer id_detalle_correo = null;
        String codigo = null;
        String correo = null;
        Integer spriden_pidm = null;
        try {
            id_detalle_correo = rowset.getInt("ID_DETALLE_CORREO");
            codigo = rowset.getString("CODIGO");
            correo = rowset.getString("CORREO");
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        validarCampo(id_detalle_correo, "ID_DETALLE_CORREO");
        validarCampo(codigo, "CODIGO");
        validarCampo(correo, "CORREO " + codigo);
        validarCampo(spriden_pidm, "SPRIDEN_PIDM " + codigo);

        final Correo_Electronico email = new Correo_Electronico(id_detalle_correo, correo.toLowerCase(), spriden_pidm,
                0);
        switch (codigo) {
            case "CES":
                email.setId_tipo_correo(COR_INSTITUCIONAL);
                break;
            case "CON":
                email.setId_tipo_correo(COR_CONTACTO);
                break;
            case "COP":
                email.setId_tipo_correo(COR_PERSONAL);
                break;
            case "CER":
                email.setId_tipo_correo(COR_PERSONAL);
                break;
            default:
                throw new Exception("Error al cargar Correo, CODIGO desconocido: " + codigo);
        }
        return email;
    }

    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================
    public HashMap<Integer, Correo_Electronico> getDetalleCorreos_CGI(final int spridenEstudiante) throws Exception {
        String Query = "SELECT -1 ID_DETALLE_CORREO, GOREMAL_EMAL_CODE CODIGO, GOREMAL_EMAIL_ADDRESS CORREO, GOREMAL_PIDM SPRIDEN_PIDM "
                + "FROM GOREMAL " + "WHERE GOREMAL_PIDM = %d " + "AND GOREMAL_STATUS_IND = 'A' "
                + "AND TRIM(GOREMAL_EMAIL_ADDRESS) IS NOT NULL";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var correos = new HashMap<Integer, Correo_Electronico>();
        Correo_Electronico correo;
        Integer key = -1;
        while (rowset.next()) {
            correo = crearCorreo(rowset);
            correos.put(key--, correo);
        }
        return correos;
    }
    // =========================================== CENTRO DE GESTION INFORMATICA
    // ==============================================

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public HashMap<Integer, Correo_Electronico> getDetalleCorreos_CAEPP(final int spridenEstudiante)
            throws Exception {
        String Query = "SELECT DC.ID_DETALLE_CORREO,DC.DIRECCION_CORREO CORREO,DC.SPRIDEN_PIDM,TC.NOMBRE_CORREO CODIGO "
                + "FROM DETALLE_CORREO DC, TIPO_CORREO TC " + "WHERE DC.SPRIDEN_PIDM = %d "
                + "AND DC.ID_TIPO_CORREO = TC.ID_TIPO_CORREO ";
        // + "AND DC.ESTADO = 'A' " + "AND TC.ESTADO = 'A'";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var correos = new HashMap<Integer, Correo_Electronico>();
        Correo_Electronico correo;
        while (rowset.next()) {
            correo = crearCorreo(rowset);
            correos.put(correo.getId_detalle_correo(), correo);
        }
        return correos;
    }

    // CRUD Correos
    public Boolean agregarDetalleCorreo_CAEPP(final Correo_Electronico correo) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_DET_COR(DIRECCION_CORREO IN VARCHAR2, PSPRIDEN_PIDM
        // IN INT, PID_TIPO_CORREO IN INT, PID_FORMULARIO_INSCRIPCION IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_INS_DET_COR('%s',%d,%d,FUN_ID_FOR_INS_CAEPP_SPR(%d))}";
        Query = String.format(Query, correo.getDireccion_correo(), correo.getSpriden_pidm(), correo.getId_tipo_correo(),
                correo.getSpriden_pidm());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarDetalleCorreo_CAEPP(final Correo_Electronico correo) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_DET_COR(PID_DETALLE_CORREO IN INT, DIRECCION_CORREO
        // IN VARCHAR2, PSPRIDEN_PIDM IN INT, PID_TIPO_CORREO IN INT,
        // PID_FORMULARIO_INSCRIPCION IN INT)
        // UNA_PSICOL.FUN_ID_USU_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_ACT_DET_COR(%d,'%s',%d,%d,FUN_ID_FOR_INS_CAEPP_SPR(%d))}";
        Query = String.format(Query, correo.getId_detalle_correo(), correo.getDireccion_correo(),
                correo.getSpriden_pidm(), correo.getId_tipo_correo(), correo.getSpriden_pidm());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleCorreo_CAEPP(final int id_detalle_correo) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_DET_COR(PID_DETALLE_CORREO IN INT)
        String Query = "{call PRC_CAEPP_ELI_DET_COR(%d)}";
        Query = String.format(Query, id_detalle_correo);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleCorreosPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_TOD_DET_COR(PSPRIDEN_PIDM IN INT)
        // UNA_PSICOL.FUN_SPR_EST_CAEPP_IDE(PIDENTIFICACION IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ELI_TOD_DET_COR(FUN_SPR_EST_CAEPP_IDE('%s'))}";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase CorreoRepositorio
