package api.detalle_correo;

import java.util.Objects;

//Inicio clase Correo_Electronico
public final class Correo_Electronico {

//Atributos
    private int id_detalle_correo;
    private String direccion_correo;
    private int spriden_pidm;
    private int id_tipo_correo;

    public Correo_Electronico() {
        this.id_detalle_correo = 0;
        this.direccion_correo = "";
        this.spriden_pidm = 0;
        this.id_tipo_correo = 0;
    }

    public Correo_Electronico(final int id_detalle_correo, final String direccion_correo, final int spriden_pidm,
            final int id_tipo_correo) {
        this.id_detalle_correo = id_detalle_correo;
        this.direccion_correo = direccion_correo;
        this.spriden_pidm = spriden_pidm;
        this.id_tipo_correo = id_tipo_correo;
    }

    public void setId_detalle_correo(final int id_detalle_correo) {
        this.id_detalle_correo = id_detalle_correo;
    }

    public void setDireccion_correo(final String direccion_correo) {
        this.direccion_correo = direccion_correo;
    }

    public void setSpriden_pidm(final int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public void setId_tipo_correo(final int id_tipo_correo) {
        this.id_tipo_correo = id_tipo_correo;
    }

    public int getId_detalle_correo() {
        return id_detalle_correo;
    }

    public String getDireccion_correo() {
        return direccion_correo;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public int getId_tipo_correo() {
        return id_tipo_correo;
    }

    @Override
    public String toString() {
        return "Correo_Electronico{" + "id_detalle_correo=" + id_detalle_correo + ", direccion_correo="
                + direccion_correo + ", spriden_pidm=" + spriden_pidm + ", id_tipo_correo=" + id_tipo_correo + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id_detalle_correo;
        hash = 89 * hash + Objects.hashCode(this.direccion_correo);
        hash = 89 * hash + this.spriden_pidm;
        hash = 89 * hash + this.id_tipo_correo;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Correo_Electronico other = (Correo_Electronico) obj;
        if (this.id_detalle_correo != other.id_detalle_correo) {
            return false;
        }
        if (this.spriden_pidm != other.spriden_pidm) {
            return false;
        }
        if (this.id_tipo_correo != other.id_tipo_correo) {
            return false;
        }
        if (!Objects.equals(this.direccion_correo, other.direccion_correo)) {
            return false;
        }
        return true;
    }

    public Boolean objetoIncompleto() throws Exception {
        if (id_detalle_correo == 0) {
            throw new Exception("id_detalle_correo default");
        }
        if (direccion_correo.isBlank()) {
            throw new Exception("direccion_correo default");
        }
        if (spriden_pidm == 0) {
            throw new Exception("spriden_pidm default");
        }
        if (id_tipo_correo == 0) {
            throw new Exception("id_tipo_correo default");
        }
        return false;
    }

}
//Fin clase Correo_Electronico
