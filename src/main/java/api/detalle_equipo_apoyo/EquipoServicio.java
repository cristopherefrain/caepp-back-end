package api.detalle_equipo_apoyo;

import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase EquipoServicio
public class EquipoServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    EquipoRepositorio equipoRepo;
    HashMap<Integer, Equipo_Apoyo> modelo;

    // =================================================== SISTEMA CAEPP
    // ======================================================
    @Async
    public CompletableFuture<HashMap<Integer, Equipo_Apoyo>> getDetalleEquipos_CAEPP(final int spridenEstudiante) throws Exception {
        modelo = equipoRepo.getDetalleEquipo_CAEPP(spridenEstudiante);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Equipos
    @Async
    public CompletableFuture<Boolean> agregarDetalleEquipos_CAEPP(final HashMap<Integer, Equipo_Apoyo> equipos) throws Exception {
        for (final Equipo_Apoyo equipo : equipos.values()) {
            // Agrega todos los Objetos
            if (!equipoRepo.agregarDetalleEquipo_CAEPP(equipo)) {
                return CompletableFuture.completedFuture(false);
            }
        }
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> actualizarDetalleEquipos_CAEPP(final HashMap<Integer, Equipo_Apoyo> equipos) throws Exception {
        Equipo_Apoyo value_equipos;
        Equipo_Apoyo value_modelo;
        for (final Integer key : modelo.keySet()) {
            value_equipos = equipos.get(key);
            // Si no devuelve un Objeto con el id del modelo entonces fue eliminado y se
            // manda a eliminar de la base
            if (value_equipos == null) {
                if (!equipoRepo.eliminarDetalleEquipo_CAEPP(key)) {
                    return CompletableFuture.completedFuture(false);
                }
            } else 
            // Si devuelve un Objeto y es diferente al del modelo viejo se manda a
            // actualizar
            if (!value_equipos.equals(modelo.get(key))) {

                if (!equipoRepo.actualizarDetalleEquipo_CAEPP(value_equipos)) {
                    return CompletableFuture.completedFuture(false);
                }
            }
        }
        if (modelo != null) {
            for (final Integer key : equipos.keySet()) {
                value_modelo = modelo.get(key);
                // Si no devuelve un Objeto con el id del nuevo modelo entonces fue agregado y
                // hay que insertarlo en la base
                if (value_modelo == null) {
                    if (!equipoRepo.agregarDetalleEquipo_CAEPP(equipos.get(key))) {
                        return CompletableFuture.completedFuture(false);
                    }
                }
            }
        }
        modelo = equipos;
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> eliminarDetalleEquiposPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        if (!equipoRepo.eliminarDetalleEquiposPorIdentificacion_CAEPP(idenEstudiante)) {
            return CompletableFuture.completedFuture(false);
        }
        modelo = null;
        return CompletableFuture.completedFuture(true);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase EquipoServicio
