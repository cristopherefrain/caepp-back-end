package api.detalle_equipo_apoyo;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

//Inicio clase Equipo_Apoyo
public final class Equipo_Apoyo {

    // Atributos
    private int id_detalle_equipo;
    private Date fecha_prestamo;
    private Date fecha_devolucion;
    private int numero_activo;
    private String observaciones;
    private int spriden_pidm;
    private int id_tipo_equipo_apoyo;
    private int id_tipo_ciclo;
    private int id_tipo_condicion;

    private String titulo_tipo_equipo_apoyo;
    private String titulo_tipo_ciclo;
    private String titulo_tipo_condicion;

    public Equipo_Apoyo() {
        this.id_detalle_equipo = 0;
        this.fecha_prestamo = Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        this.fecha_devolucion = Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        this.numero_activo = 0;
        this.observaciones = "";
        this.spriden_pidm = 0;
        this.id_tipo_equipo_apoyo = 0;
        this.id_tipo_ciclo = 0;
        this.id_tipo_condicion = 0;

        this.titulo_tipo_equipo_apoyo = "";
        this.titulo_tipo_ciclo = "";
        this.titulo_tipo_condicion = "";
    }

    public Equipo_Apoyo(final int id_detalle_equipo, final Date fecha_prestamo, final Date fecha_devolucion,
            final int numero_activo, final String descripcion_equipo, final int spriden_pidm,
            final int id_tipo_equipo_apoyo, final int id_tipo_ciclo, final int id_tipo_estado,
            final String titulo_tipo_equipo_apoyo, final String titulo_tipo_ciclo, final String titulo_tipo_condicion) {
        this.id_detalle_equipo = id_detalle_equipo;
        this.fecha_prestamo = fecha_prestamo;
        this.fecha_devolucion = fecha_devolucion;
        this.numero_activo = numero_activo;
        this.observaciones = descripcion_equipo;
        this.spriden_pidm = spriden_pidm;
        this.id_tipo_equipo_apoyo = id_tipo_equipo_apoyo;
        this.id_tipo_ciclo = id_tipo_ciclo;
        this.id_tipo_condicion = id_tipo_estado;

        this.titulo_tipo_equipo_apoyo = titulo_tipo_equipo_apoyo;
        this.titulo_tipo_ciclo = titulo_tipo_ciclo;
        this.titulo_tipo_condicion = titulo_tipo_condicion;
    }

    public Equipo_Apoyo(final int id_detalle_equipo, final Date fecha_prestamo, final Date fecha_devolucion,
            final int numero_activo, final String descripcion_equipo, final int spriden_pidm,
            final int id_tipo_equipo_apoyo, final int id_tipo_ciclo, final int id_tipo_estado) {
        this.id_detalle_equipo = id_detalle_equipo;
        this.fecha_prestamo = fecha_prestamo;
        this.fecha_devolucion = fecha_devolucion;
        this.numero_activo = numero_activo;
        this.observaciones = descripcion_equipo;
        this.spriden_pidm = spriden_pidm;
        this.id_tipo_equipo_apoyo = id_tipo_equipo_apoyo;
        this.id_tipo_ciclo = id_tipo_ciclo;
        this.id_tipo_condicion = id_tipo_estado;

        this.titulo_tipo_equipo_apoyo = "";
        this.titulo_tipo_ciclo = "";
        this.titulo_tipo_condicion = "";
    }

    public void setId_detalle_equipo(final int id_detalle_equipo) {
        this.id_detalle_equipo = id_detalle_equipo;
    }

    public void setFecha_prestamo(final Date fecha_prestamo) {
        this.fecha_prestamo = fecha_prestamo;
    }

    public void setFecha_devolucion(final Date fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }

    public void setNumero_activo(final int numero_activo) {
        this.numero_activo = numero_activo;
    }

    public void setObservaciones(final String observaciones) {
        this.observaciones = observaciones;
    }

    public void setSpriden_pidm(final int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public void setId_tipo_equipo_apoyo(final int id_tipo_equipo_apoyo) {
        this.id_tipo_equipo_apoyo = id_tipo_equipo_apoyo;
    }

    public void setId_tipo_ciclo(final int id_tipo_ciclo) {
        this.id_tipo_ciclo = id_tipo_ciclo;
    }

    public void setId_tipo_condicion(final int id_tipo_condicion) {
        this.id_tipo_condicion = id_tipo_condicion;
    }

    public void setTitulo_tipo_equipo_apoyo(String titulo_tipo_equipo_apoyo) {
        this.titulo_tipo_equipo_apoyo = titulo_tipo_equipo_apoyo;
    }

    public void setTitulo_tipo_ciclo(String titulo_tipo_ciclo) {
        this.titulo_tipo_ciclo = titulo_tipo_ciclo;
    }

    public void setTitulo_tipo_condicion(String titulo_tipo_condicion) {
        this.titulo_tipo_condicion = titulo_tipo_condicion;
    }

    public int getId_detalle_equipo() {
        return id_detalle_equipo;
    }

    public Date getFecha_prestamo() {
        return fecha_prestamo;
    }

    public Date getFecha_devolucion() {
        return fecha_devolucion;
    }

    public int getNumero_activo() {
        return numero_activo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public int getId_tipo_equipo_apoyo() {
        return id_tipo_equipo_apoyo;
    }

    public int getId_tipo_ciclo() {
        return id_tipo_ciclo;
    }

    public int getId_tipo_condicion() {
        return id_tipo_condicion;
    }

    public String getTitulo_tipo_equipo_apoyo() {
        return titulo_tipo_equipo_apoyo;
    }

    public String getTitulo_tipo_ciclo() {
        return titulo_tipo_ciclo;
    }

    public String getTitulo_tipo_condicion() {
        return titulo_tipo_condicion;
    }

    @Override
    public String toString() {
        return "Equipo_Apoyo{" + "id_detalle_equipo=" + id_detalle_equipo + ", fecha_prestamo=" + fecha_prestamo + ", fecha_devolucion=" + fecha_devolucion + ", numero_activo=" + numero_activo + ", descripcion_equipo=" + observaciones + ", spriden_pidm=" + spriden_pidm + ", id_tipo_equipo_apoyo=" + id_tipo_equipo_apoyo + ", id_tipo_ciclo=" + id_tipo_ciclo + ", id_tipo_condicion=" + id_tipo_condicion + ", titulo_tipo_equipo_apoyo=" + titulo_tipo_equipo_apoyo + ", titulo_tipo_ciclo=" + titulo_tipo_ciclo + ", titulo_tipo_condicion=" + titulo_tipo_condicion + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id_detalle_equipo;
        hash = 29 * hash + Objects.hashCode(this.fecha_prestamo);
        hash = 29 * hash + Objects.hashCode(this.fecha_devolucion);
        hash = 29 * hash + this.numero_activo;
        hash = 29 * hash + Objects.hashCode(this.observaciones);
        hash = 29 * hash + this.spriden_pidm;
        hash = 29 * hash + this.id_tipo_equipo_apoyo;
        hash = 29 * hash + this.id_tipo_ciclo;
        hash = 29 * hash + this.id_tipo_condicion;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Equipo_Apoyo other = (Equipo_Apoyo) obj;
        if (this.id_detalle_equipo != other.id_detalle_equipo) {
            return false;
        }
        if (this.numero_activo != other.numero_activo) {
            return false;
        }
        if (this.spriden_pidm != other.spriden_pidm) {
            return false;
        }
        if (this.id_tipo_equipo_apoyo != other.id_tipo_equipo_apoyo) {
            return false;
        }
        if (this.id_tipo_ciclo != other.id_tipo_ciclo) {
            return false;
        }
        if (this.id_tipo_condicion != other.id_tipo_condicion) {
            return false;
        }
        if (!Objects.equals(this.observaciones, other.observaciones)) {
            return false;
        }
        // if (!Objects.equals(this.fecha_prestamo, other.fecha_prestamo) {
        //     return false;
        // }
        // if (!Objects.equals(this.fecha_devolucion, other.fecha_devolucion) {
        //     return false;
        // }
        return true;
    }

    public Boolean objetoIncompleto() throws Exception {
        if (id_detalle_equipo == 0) {
            throw new Exception("id_detalle_equipo default");
        }
        if (spriden_pidm == 0) {
            throw new Exception("spriden_pidm default");
        }
        if (id_tipo_equipo_apoyo == 0) {
            throw new Exception("id_tipo_equipo_apoyo default");
        }
        if (id_tipo_ciclo == 0) {
            throw new Exception("id_tipo_ciclo default");
        }
        if (id_tipo_condicion == 0) {
            throw new Exception("id_tipo_estado default");
        }
        return false;
    }
}
// Fin clase Equipo_Apoyo
