package api.detalle_equipo_apoyo;

import api.modelos.Repositorio_Principal;
import java.sql.Date;
import java.util.HashMap;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion
//(Capa de Persistencia)
@Repository
// Inicio clase EquipoRepositorio
public class EquipoRepositorio extends Repositorio_Principal {

    // Metodo para construir un objeto Ajuste_Metodologico apartir de una tupla de
    // la base de datos
    private Equipo_Apoyo crearEquipo(final CachedRowSet rowset) throws Exception {
        Integer id_detalle_equipo = null;
        Date fecha_prestamo = null;
        Date fecha_devolucion = null;
        Integer numero_activo = null;
        String observaciones = null;
        Integer spriden_pidm = null;
        Integer id_tipo_equipo_apoyo = null;
        Integer id_tipo_ciclo = null;
        Integer id_tipo_estado = null;

        String titulo_tipo_equipo_apoyo = null;
        String titulo_tipo_ciclo = null;
        String titulo_tipo_estado = null;
        try {
            id_detalle_equipo = rowset.getInt("ID_DETALLE_EQUIPO");
            fecha_prestamo = rowset.getDate("FECHA_PRESTAMO");
            fecha_devolucion = rowset.getDate("FECHA_DEVOLUCION");
            numero_activo = rowset.getInt("NUMERO_ACTIVO");
            observaciones = rowset.getString("OBSERVACIONES");
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            id_tipo_equipo_apoyo = rowset.getInt("ID_TIPO_EQUIPO");
            id_tipo_ciclo = rowset.getInt("ID_TIPO_CICLO");
            id_tipo_estado = rowset.getInt("ID_TIPO_CONDICION");

            titulo_tipo_equipo_apoyo = rowset.getString("DESCRIPCION_EQUIPO");
            titulo_tipo_ciclo = rowset.getString("DESCRIPCION_CICLO");
            titulo_tipo_estado = rowset.getString("DESCRIPCION_CONDICION");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        validarCampo(id_detalle_equipo, "ID_DETALLE_EQUIPO");
        validarCampo(fecha_prestamo, "FECHA_PRESTAMO");
        validarCampo(fecha_devolucion, "FECHA_DEVOLUCION");
//        validarCampo(numero_activo, "NUMERO_ACTIVO");
//        validarCampo(observaciones, "OBSERVACIONES");
        validarCampo(spriden_pidm, "SPRIDEN_PIDM");
        validarCampo(id_tipo_equipo_apoyo, "ID_TIPO_EQUIPO");
        validarCampo(id_tipo_ciclo, "ID_TIPO_CICLO");
        validarCampo(id_tipo_estado, "ID_TIPO_CONDICION");

        validarCampo(titulo_tipo_equipo_apoyo, "DESCRIPCION_EQUIPO");
        validarCampo(titulo_tipo_ciclo, "DESCRIPCION_CICLO");
        validarCampo(titulo_tipo_estado, "DESCRIPCION_CONDICION");

        final Equipo_Apoyo equipo_apoyo = new Equipo_Apoyo(id_detalle_equipo, fecha_prestamo, fecha_devolucion, numero_activo, observaciones,
                spriden_pidm, id_tipo_equipo_apoyo, id_tipo_ciclo, id_tipo_estado,
                titulo_tipo_equipo_apoyo, titulo_tipo_ciclo, titulo_tipo_estado);

        return equipo_apoyo;
    }

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public HashMap<Integer, Equipo_Apoyo> getDetalleEquipo_CAEPP(final int spridenEstudiante) throws Exception {
        String Query = "SELECT ID_DETALLE_EQUIPO, FECHA_PRESTAMO, SPRIDEN_PIDM, FECHA_DEVOLUCION, NUMERO_ACTIVO, OBSERVACIONES, ID_TIPO_EQUIPO, ID_TIPO_CICLO, ID_TIPO_CONDICION "
                + ",DESCRIPCION_EQUIPO, DESCRIPCION_CICLO, DESCRIPCION_CONDICION "
                + "FROM DETALLE_EQUIPO_APOYO "
                + "NATURAL JOIN TIPO_EQUIPO_APOYO NATURAL JOIN TIPO_CICLO NATURAL JOIN TIPO_CONDICION_EQUIPO "
                + "WHERE SPRIDEN_PIDM = %d ";
        // + "AND ESTADO = 'A'";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var equipos = new HashMap<Integer, Equipo_Apoyo>();
        Equipo_Apoyo equipo;
        while (rowset.next()) {
            equipo = crearEquipo(rowset);
            equipos.put(equipo.getId_detalle_equipo(), equipo);
        }
        return equipos;
    }

    // CRUD Equipos
    public Boolean agregarDetalleEquipo_CAEPP(final Equipo_Apoyo equipo) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_DET_EQU(PFECHA_PRESTAMO IN DATE, PSPRIDEN_PIDM IN
        // INT, PFECHA_DEVOLUCION IN DATE, PNUMERO_ACTIVO IN INT,
        // POBSERVACIONES IN VARCHAR2, PID_TIPO_EQUIPO IN INT,
        // PID_FORMULARIO_INSCRIPCION IN INT, PID_TIPO_CICLO IN INT,
        // PID_TIPO_CONDICION IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_INS_DET_EQU(to_date('%s','yyyy-mm-dd'),%d,to_date('%s','yyyy-mm-dd'),%d,'%s',%d,FUN_ID_FOR_INS_CAEPP_SPR(%d),%d,%d)}";
        Query = String.format(Query, equipo.getFecha_prestamo().toString(), equipo.getSpriden_pidm(),
                equipo.getFecha_devolucion().toString(), equipo.getNumero_activo(), equipo.getObservaciones(),
                equipo.getId_tipo_equipo_apoyo(), equipo.getSpriden_pidm(), equipo.getId_tipo_ciclo(),
                equipo.getId_tipo_condicion());

        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarDetalleEquipo_CAEPP(final Equipo_Apoyo equipo) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_DET_EQU(PID_DETALLE_EQUIPO IN INT,PFECHA_PRESTAMO IN
        // DATE, PSPRIDEN_PIDM IN INT, PFECHA_DEVOLUCION IN DATE, PNUMERO_ACTIVO IN INT,
        // POBSERVACIONES IN VARCHAR2, PID_TIPO_EQUIPO IN INT,
        // PID_FORMULARIO_INSCRIPCION IN INT, PID_TIPO_CICLO IN INT,
        // PID_TIPO_CONDICION IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_ACT_DET_EQU(%d,to_date('%s','yyyy-mm-dd'),%d,to_date('%s','yyyy-mm-dd'),%d,'%s',%d,FUN_ID_FOR_INS_CAEPP_SPR(%d),%d,%d)}";
        Query = String.format(Query, equipo.getId_detalle_equipo(), equipo.getFecha_prestamo().toString(),
                equipo.getSpriden_pidm(), equipo.getFecha_devolucion().toString(), equipo.getNumero_activo(),
                equipo.getObservaciones(), equipo.getId_tipo_equipo_apoyo(), equipo.getSpriden_pidm(),
                equipo.getId_tipo_ciclo(), equipo.getId_tipo_condicion());

        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleEquipo_CAEPP(final int id_detalle_equipo) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_DET_EQU(PID_DETALLE_AJUSTE IN INT)
        String Query = "{call PRC_CAEPP_ELI_DET_EQU(%d)}";
        Query = String.format(Query, id_detalle_equipo);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleEquiposPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_TOD_DET_EQU(PSPRIDEN_PIDM IN INT)
        // UNA_PSICOL.FUN_SPR_EST_CAEPP_IDE(PIDENTIFICACION IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ELI_TOD_DET_EQU(FUN_SPR_EST_CAEPP_IDE('%s'))}";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase EquipoRepositorio
