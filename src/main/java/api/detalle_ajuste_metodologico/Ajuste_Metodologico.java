package api.detalle_ajuste_metodologico;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

//Inicio clase Ajuste_Metodologico
public final class Ajuste_Metodologico {

    // Atributos
    private int id_detalle_ajuste;
    private Date fecha;
    private String observaciones;
    private int spriden_pidm;
    private int id_tipo_ajuste;

    private String titulo_tipo_ajuste;

    public Ajuste_Metodologico() {
        this.id_detalle_ajuste = 0;
        this.fecha = Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        this.observaciones = "";
        this.spriden_pidm = 0;
        this.id_tipo_ajuste = 0;
        this.titulo_tipo_ajuste = "";
    }

    public Ajuste_Metodologico(final int id_detalle_ajuste, final Date fecha, final String observaciones,
            final int spriden_pidm, final int id_tipo_ajuste, final String titulo_tipo_ajuste) {
        this.id_detalle_ajuste = id_detalle_ajuste;
        this.fecha = fecha;
        this.observaciones = observaciones;
        this.spriden_pidm = spriden_pidm;
        this.id_tipo_ajuste = id_tipo_ajuste;
        this.titulo_tipo_ajuste = titulo_tipo_ajuste;
    }

    public void setId_detalle_ajuste(final int id_detalle_ajuste) {
        this.id_detalle_ajuste = id_detalle_ajuste;
    }

    public void setFecha(final Date fecha) {
        this.fecha = fecha;
    }

    public void setObservaciones(final String observaciones) {
        this.observaciones = observaciones;
    }

    public void setSpriden_pidm(final int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public void setId_tipo_ajuste(final int id_tipo_ajuste) {
        this.id_tipo_ajuste = id_tipo_ajuste;
    }

    public void setTitulo_tipo_ajuste(String titulo_tipo_ajuste) {
        this.titulo_tipo_ajuste = titulo_tipo_ajuste;
    }

    public int getId_detalle_ajuste() {
        return id_detalle_ajuste;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public int getId_tipo_ajuste() {
        return id_tipo_ajuste;
    }

    public String getTitulo_tipo_ajuste() {
        return titulo_tipo_ajuste;
    }

    @Override
    public String toString() {
        return "Ajuste_Metodologico{" + "id_detalle_ajuste=" + id_detalle_ajuste + ", fecha=" + fecha + ", observaciones=" + observaciones + ", spriden_pidm=" + spriden_pidm + ", id_tipo_ajuste=" + id_tipo_ajuste + ", titulo_tipo_ajuste=" + titulo_tipo_ajuste + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id_detalle_ajuste;
        hash = 97 * hash + Objects.hashCode(this.fecha);
        hash = 97 * hash + Objects.hashCode(this.observaciones);
        hash = 97 * hash + this.spriden_pidm;
        hash = 97 * hash + this.id_tipo_ajuste;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ajuste_Metodologico other = (Ajuste_Metodologico) obj;
        if (this.id_detalle_ajuste != other.id_detalle_ajuste) {
            return false;
        }
        if (this.spriden_pidm != other.spriden_pidm) {
            return false;
        }
        if (this.id_tipo_ajuste != other.id_tipo_ajuste) {
            return false;
        }
        if (!Objects.equals(this.observaciones, other.observaciones)) {
            return false;
        }
        // if (!Objects.equals(this.fecha, other.fecha) {
        //     return false;
        // }
        return true;
    }

    public Boolean objetoIncompleto() throws Exception {
        if (id_detalle_ajuste == 0) {
            throw new Exception("id_detalle_ajuste default");
        }
        if (fecha.after(Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))) {
            throw new Exception("fecha default");
        }
        if (observaciones.isBlank()) {
            throw new Exception("observaciones default");
        }
        if (spriden_pidm == 0) {
            throw new Exception("spriden_pidm default");
        }
        if (id_tipo_ajuste == 0) {
            throw new Exception("id_tipo_ajuste default");
        }
        return false;
    }
}
// Fin clase Ajuste_Metodologico
