package api.detalle_ajuste_metodologico;

import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

//Anotacion @Service indica a Spring Boot que es una clase de servicio
@Service
// Inicio clase AjusteServicio
public class AjusteServicio {

    // Anotacion @Autowired indica a Spring Boot que debe inyectar una intancia
    // cuando crea el servicio
    @Autowired
    AjusteRepositorio ajusteRepo;
    HashMap<Integer, Ajuste_Metodologico> modelo;

    // =================================================== SISTEMA CAEPP
    // ======================================================
    @Async
    public CompletableFuture<HashMap<Integer, Ajuste_Metodologico>> getDetalleAjustes_CAEPP(final int spridenEstudiante) throws Exception {
        modelo = ajusteRepo.getDetalleAjustes_CAEPP(spridenEstudiante);
        return CompletableFuture.completedFuture(modelo);
    }

    // CRUD Ajustes
    @Async
    public CompletableFuture<Boolean> agregarDetalleAjustes_CAEPP(final HashMap<Integer, Ajuste_Metodologico> ajustes) throws Exception {
        for (final Ajuste_Metodologico ajuste : ajustes.values()) {
            // Agrega todos los Objetos
            if (!ajusteRepo.agregarDetalleAjuste_CAEPP(ajuste)) {
                return CompletableFuture.completedFuture(false);
            }
        }
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> actualizarDetalleAjustes_CAEPP(final HashMap<Integer, Ajuste_Metodologico> ajustes)
            throws Exception {
        Ajuste_Metodologico value_ajustes;
        Ajuste_Metodologico value_modelo;
        for (final Integer key : modelo.keySet()) {
            value_ajustes = ajustes.get(key);
            // Si no devuelve un Objeto con el id del modelo entonces fue eliminado y se
            // manda a eliminar de la base
            if (value_ajustes == null) {
                if (!ajusteRepo.eliminarDetalleAjuste_CAEPP(key)) {
                    return CompletableFuture.completedFuture(false);
                }
            } else 
            // Si devuelve un Objeto y es diferente al del modelo viejo se manda a
            // actualizar
            if (!value_ajustes.equals(modelo.get(key))) {
                if (!ajusteRepo.actualizarDetalleAjuste_CAEPP(value_ajustes)) {
                    return CompletableFuture.completedFuture(false);
                }
            }
        }
        if (modelo != null) {
            for (final Integer key : ajustes.keySet()) {
                value_modelo = modelo.get(key);
                // Si no devuelve un Objeto con el id del nuevo modelo entonces fue agregado y
                // hay que insertarlo en la base
                if (value_modelo == null) {
                    if (!ajusteRepo.agregarDetalleAjuste_CAEPP(ajustes.get(key))) {
                        return CompletableFuture.completedFuture(false);
                    }
                }
            }
        }
        modelo = ajustes;
        return CompletableFuture.completedFuture(true);
    }

    @Async
    public CompletableFuture<Boolean> eliminarDetalleAjustesPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        if (!ajusteRepo.eliminarDetalleAjustesPorIdentificacion_CAEPP(idenEstudiante)) {
            return CompletableFuture.completedFuture(false);
        }
        modelo = null;
        return CompletableFuture.completedFuture(true);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase AjusteServicio
