package api.detalle_ajuste_metodologico;

import api.modelos.Repositorio_Principal;
import java.sql.Date;
import java.util.HashMap;
import javax.sql.rowset.CachedRowSet;
import org.springframework.stereotype.Repository;

//Anotacion @Repository indica a Spring Boot que es una clase Repositorio para el acceso y almacenaje de informacion
//(Capa de Persistencia)
@Repository
// Inicio clase AjusteRepositorio
public class AjusteRepositorio extends Repositorio_Principal {

    // Metodo para construir un objeto Ajuste_Metodologico apartir de una tupla de
    // la base de datos
    private Ajuste_Metodologico crearAjuste(final CachedRowSet rowset) throws Exception {
        Integer id_detalle_ajuste = null;
        Date fecha = null;
        String observaciones = null;
        Integer spriden_pidm = null;
        Integer id_tipo_ajuste = null;

        String titulo_tipo_ajuste = null;
        try {
            id_detalle_ajuste = rowset.getInt("ID_DETALLE_AJUSTE");
            fecha = rowset.getDate("FECHA");
            observaciones = rowset.getString("OBSERVACIONES");
            spriden_pidm = rowset.getInt("SPRIDEN_PIDM");
            id_tipo_ajuste = rowset.getInt("ID_TIPO_AJUSTE");

            titulo_tipo_ajuste = rowset.getString("DESCRIPCION_AJUSTE");
        } catch (final Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Fallo al cargar dato de la base (Revizar Consola Server)");
        }
        validarCampo(id_detalle_ajuste, "ID_DETALLE_AJUSTE");
        validarCampo(fecha, "FECHA");
        validarCampo(observaciones, "OBSERVACIONES");
        validarCampo(spriden_pidm, "SPRIDEN_PIDM");
        validarCampo(id_tipo_ajuste, "ID_TIPO_AJUSTE");

        validarCampo(titulo_tipo_ajuste, "DESCRIPCION_AJUSTE");

        return new Ajuste_Metodologico(id_detalle_ajuste, fecha, observaciones, spriden_pidm, id_tipo_ajuste, titulo_tipo_ajuste);
    }

    // =================================================== SISTEMA CAEPP
    // ======================================================
    public HashMap<Integer, Ajuste_Metodologico> getDetalleAjustes_CAEPP(final int spridenEstudiante)
            throws Exception {
        String Query = "SELECT ID_DETALLE_AJUSTE,OBSERVACIONES,FECHA,SPRIDEN_PIDM,ID_TIPO_AJUSTE "
                + ",DESCRIPCION_AJUSTE  "
                + "FROM DETALLE_AJUSTE_METODOLOGICO "
                + "NATURAL JOIN TIPO_AJUSTE_METODOLOGICO "
                + "WHERE SPRIDEN_PIDM = %d ";
        // + "AND ESTADO = 'A'";
        Query = String.format(Query, spridenEstudiante);
        // Ejecuta el Query y devuelve las tuplas de la base de datos
        final CachedRowSet rowset = this.executeQuery(Query);

        final var ajustes = new HashMap<Integer, Ajuste_Metodologico>();
        Ajuste_Metodologico ajuste;
        while (rowset.next()) {
            ajuste = crearAjuste(rowset);
            ajustes.put(ajuste.getId_detalle_ajuste(), ajuste);
        }
        return ajustes;
    }

    // CRUD Ajustes
    public Boolean agregarDetalleAjuste_CAEPP(final Ajuste_Metodologico ajuste) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_INS_DET_AJU(POBSERVACIONES IN VARCHAR2, PFECHA IN DATE,
        // PSPRIDEN_PIDM IN INT, PID_FORMULARIO_INSCRIPCION IN INT,
        // PID_TIPO_AJUSTE IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_INS_DET_AJU('%s',to_date('%s','yyyy-mm-dd'),%d,FUN_ID_FOR_INS_CAEPP_SPR(%d),%d)}";
        Query = String.format(Query, ajuste.getObservaciones(), ajuste.getFecha().toString(), ajuste.getSpriden_pidm(),
                ajuste.getSpriden_pidm(), ajuste.getId_tipo_ajuste());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean actualizarDetalleAjuste_CAEPP(final Ajuste_Metodologico ajuste) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ACT_DET_AJU(PID_DETALLE_AJUSTE IN INT, POBSERVACIONES IN
        // VARCHAR2, PFECHA IN DATE, PSPRIDEN_PIDM IN INT,
        // PID_FORMULARIO_INSCRIPCION IN INT, PID_TIPO_AJUSTE IN INT)
        // UNA_PSICOL.FUN_ID_FOR_INS_CAEPP_SPR (PSPRIDEN_PIDM IN INT)
        String Query = "{call PRC_CAEPP_ACT_DET_AJU(%d,'%s',to_date('%s','yyyy-mm-dd'),%d,FUN_ID_FOR_INS_CAEPP_SPR(%d),%d)}";
        Query = String.format(Query, ajuste.getId_detalle_ajuste(), ajuste.getObservaciones(),
                ajuste.getFecha().toString(), ajuste.getSpriden_pidm(), ajuste.getSpriden_pidm(),
                ajuste.getId_tipo_ajuste());
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleAjuste_CAEPP(final int id_detalle_ajuste) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_DET_AJU(PID_DETALLE_AJUSTE IN INT)
        String Query = "{call PRC_CAEPP_ELI_DET_AJU(%d)}";
        Query = String.format(Query, id_detalle_ajuste);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }

    public Boolean eliminarDetalleAjustesPorIdentificacion_CAEPP(final String idenEstudiante) throws Exception {
        // UNA_PSICOL.PRC_CAEPP_ELI_TOD_DET_AJU(PSPRIDEN_PIDM IN INT)
        // UNA_PSICOL.FUN_SPR_EST_CAEPP_IDE(PIDENTIFICACION IN VARCHAR2)
        String Query = "{call PRC_CAEPP_ELI_TOD_DET_AJU(FUN_SPR_EST_CAEPP_IDE('%s'))}";
        Query = String.format(Query, idenEstudiante);
        // Ejecuta el Query y devuelve true si hubo cambios en las tuplas
        return this.callableQuery(Query);
    }
    // =================================================== SISTEMA CAEPP
    // ======================================================
}
// Fin clase AjusteRepositorio
