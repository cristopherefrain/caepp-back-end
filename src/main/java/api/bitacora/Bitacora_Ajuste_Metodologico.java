package api.bitacora;

import java.sql.Date;

//Inicio clase Bitacora_Ajuste_Metodologico
public class Bitacora_Ajuste_Metodologico extends Bitacoras {

    // Atributos
    private int id_detalle_ajuste;
    private Date fecha;
    private String observaciones;
    private int spriden_pidm;

    private String titulo_tipo_ajuste;
    private String estudiante;

    public Bitacora_Ajuste_Metodologico() {
        super();
    }

    public Bitacora_Ajuste_Metodologico(int id_bitacora, String operacion, Date fecha_operacion, String usuario, int id_detalle_ajuste, Date fecha, String observaciones, int spriden_pidm, String titulo_tipo_ajuste, String estudiante) {
        super(id_bitacora, operacion, fecha_operacion, usuario);
        this.id_detalle_ajuste = id_detalle_ajuste;
        this.fecha = fecha;
        this.observaciones = observaciones;
        this.spriden_pidm = spriden_pidm;
        this.titulo_tipo_ajuste = titulo_tipo_ajuste;
        this.estudiante = estudiante;
    }

    public int getId_detalle_ajuste() {
        return id_detalle_ajuste;
    }

    public void setId_detalle_ajuste(int id_detalle_ajuste) {
        this.id_detalle_ajuste = id_detalle_ajuste;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public void setSpriden_pidm(int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public String getTitulo_tipo_ajuste() {
        return titulo_tipo_ajuste;
    }

    public void setTitulo_tipo_ajuste(String titulo_tipo_ajuste) {
        this.titulo_tipo_ajuste = titulo_tipo_ajuste;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    @Override
    public String toString() {
        return "Bitacora_Ajuste_Metodologico{" + "id_detalle_ajuste=" + id_detalle_ajuste + ", fecha=" + fecha + ", observaciones=" + observaciones + ", spriden_pidm=" + spriden_pidm + ", titulo_tipo_ajuste=" + titulo_tipo_ajuste + ", estudiante=" + estudiante + '}';
    }

}
//Fin clase Bitacora_Ajuste_Metodologico
