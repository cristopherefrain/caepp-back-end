package api.bitacora;

//Inicio clase Bitacora_Equipo_Apoyo
import java.sql.Date;

public class Bitacora_Equipo_Apoyo extends Bitacoras {

    // Atributos
    private int id_detalle_equipo;
    private Date fecha_prestamo;
    private Date fecha_devolucion;
    private int numero_activo;
    private String observaciones;
    private int spriden_pidm;

    private String titulo_tipo_equipo_apoyo;
    private String titulo_tipo_ciclo;
    private String titulo_tipo_condicion;

    private String estudiante;

    public Bitacora_Equipo_Apoyo() {
        super();
    }

    public Bitacora_Equipo_Apoyo(int id_bitacora, String operacion, Date fecha_operacion, String usuario, int id_detalle_equipo, Date fecha_prestamo, Date fecha_devolucion, int numero_activo, String observaciones, int spriden_pidm, String titulo_tipo_equipo_apoyo, String titulo_tipo_ciclo, String titulo_tipo_condicion, String estudiante) {
        super(id_bitacora, operacion, fecha_operacion, usuario);
        this.id_detalle_equipo = id_detalle_equipo;
        this.fecha_prestamo = fecha_prestamo;
        this.fecha_devolucion = fecha_devolucion;
        this.numero_activo = numero_activo;
        this.observaciones = observaciones;
        this.spriden_pidm = spriden_pidm;
        this.titulo_tipo_equipo_apoyo = titulo_tipo_equipo_apoyo;
        this.titulo_tipo_ciclo = titulo_tipo_ciclo;
        this.titulo_tipo_condicion = titulo_tipo_condicion;
        this.estudiante = estudiante;
    }

    public int getId_detalle_equipo() {
        return id_detalle_equipo;
    }

    public void setId_detalle_equipo(int id_detalle_equipo) {
        this.id_detalle_equipo = id_detalle_equipo;
    }

    public Date getFecha_prestamo() {
        return fecha_prestamo;
    }

    public void setFecha_prestamo(Date fecha_prestamo) {
        this.fecha_prestamo = fecha_prestamo;
    }

    public Date getFecha_devolucion() {
        return fecha_devolucion;
    }

    public void setFecha_devolucion(Date fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }

    public int getNumero_activo() {
        return numero_activo;
    }

    public void setNumero_activo(int numero_activo) {
        this.numero_activo = numero_activo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public void setSpriden_pidm(int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public String getTitulo_tipo_equipo_apoyo() {
        return titulo_tipo_equipo_apoyo;
    }

    public void setTitulo_tipo_equipo_apoyo(String titulo_tipo_equipo_apoyo) {
        this.titulo_tipo_equipo_apoyo = titulo_tipo_equipo_apoyo;
    }

    public String getTitulo_tipo_ciclo() {
        return titulo_tipo_ciclo;
    }

    public void setTitulo_tipo_ciclo(String titulo_tipo_ciclo) {
        this.titulo_tipo_ciclo = titulo_tipo_ciclo;
    }

    public String getTitulo_tipo_condicion() {
        return titulo_tipo_condicion;
    }

    public void setTitulo_tipo_condicion(String titulo_tipo_condicion) {
        this.titulo_tipo_condicion = titulo_tipo_condicion;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

}
//Fin clase Bitacora_Equipo_Apoyo
