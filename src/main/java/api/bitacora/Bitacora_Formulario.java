package api.bitacora;

import java.sql.Date;

//Inicio clase Bitacora_Formulario
public class Bitacora_Formulario extends Bitacoras {

    // Atributos
    private int id_formulario_inscripcion;
    private int spriden_pidm;
    private Date fecha_inscripcion;
    private Date fecha_ingreso_una;
    private String campus;
    private String carrera;
    private String tipo_beca;

    private String titulo_tipo_estado;
    private String titulo_tipo_nivel;
    private String titulo_tipo_ciclo;

    private String estudiante;

    public Bitacora_Formulario() {
        super();
    }

    public Bitacora_Formulario(int id_bitacora, String operacion, Date fecha_operacion, String usuario,
            int id_formulario_inscripcion, int spriden_pidm, Date fecha_inscripcion, Date fecha_ingreso_una, String campus, String carrera, String tipo_beca,
            String titulo_tipo_estado, String titulo_tipo_nivel, String titulo_tipo_ciclo, String estudiante) {
        super(id_bitacora, operacion, fecha_operacion, usuario);
        this.id_formulario_inscripcion = id_formulario_inscripcion;
        this.spriden_pidm = spriden_pidm;
        this.fecha_inscripcion = fecha_inscripcion;
        this.fecha_ingreso_una = fecha_ingreso_una;
        this.campus = campus;
        this.carrera = carrera;
        this.tipo_beca = tipo_beca;
        this.titulo_tipo_estado = titulo_tipo_estado;
        this.titulo_tipo_nivel = titulo_tipo_nivel;
        this.titulo_tipo_ciclo = titulo_tipo_ciclo;
        this.estudiante = estudiante;
    }

    public int getId_formulario_inscripcion() {
        return id_formulario_inscripcion;
    }

    public void setId_formulario_inscripcion(int id_formulario_inscripcion) {
        this.id_formulario_inscripcion = id_formulario_inscripcion;
    }

    public int getSpriden_pidm() {
        return spriden_pidm;
    }

    public void setSpriden_pidm(int spriden_pidm) {
        this.spriden_pidm = spriden_pidm;
    }

    public Date getFecha_inscripcion() {
        return fecha_inscripcion;
    }

    public void setFecha_inscripcion(Date fecha_inscripcion) {
        this.fecha_inscripcion = fecha_inscripcion;
    }

    public Date getFecha_ingreso_una() {
        return fecha_ingreso_una;
    }

    public void setFecha_ingreso_una(Date fecha_ingreso_una) {
        this.fecha_ingreso_una = fecha_ingreso_una;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getTipo_beca() {
        return tipo_beca;
    }

    public void setTipo_beca(String tipo_beca) {
        this.tipo_beca = tipo_beca;
    }

    public String getTitulo_tipo_estado() {
        return titulo_tipo_estado;
    }

    public void setTitulo_tipo_estado(String titulo_tipo_estado) {
        this.titulo_tipo_estado = titulo_tipo_estado;
    }

    public String getTitulo_tipo_nivel() {
        return titulo_tipo_nivel;
    }

    public void setTitulo_tipo_nivel(String titulo_tipo_nivel) {
        this.titulo_tipo_nivel = titulo_tipo_nivel;
    }

    public String getTitulo_tipo_ciclo() {
        return titulo_tipo_ciclo;
    }

    public void setTitulo_tipo_ciclo(String titulo_tipo_ciclo) {
        this.titulo_tipo_ciclo = titulo_tipo_ciclo;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    @Override
    public String toString() {
        return "Bitacora_Formulario{" + "id_formulario_inscripcion=" + id_formulario_inscripcion + ", spriden_pidm=" + spriden_pidm + ", fecha_inscripcion=" + fecha_inscripcion + ", fecha_ingreso_una=" + fecha_ingreso_una + ", campus=" + campus + ", carrera=" + carrera + ", tipo_beca=" + tipo_beca + ", titulo_tipo_estado=" + titulo_tipo_estado + ", titulo_tipo_nivel=" + titulo_tipo_nivel + ", titulo_tipo_ciclo=" + titulo_tipo_ciclo + ", estudiante=" + estudiante + '}';
    }

}
//Fin clase Bitacora_Formulario
