package api.bitacora;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

//Inicio clase Bitacoras
public class Bitacoras {

    // Atributos
    private int id_bitacora;
    private String operacion;
    private Date fecha_operacion;
    private String usuario;

    public Bitacoras() {
        this.id_bitacora = -1;
        this.operacion = "";
        this.fecha_operacion = Date.valueOf(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));;
        this.usuario = "";
    }

    public Bitacoras(int id_bitacora, String operacion, Date fecha_operacion, String usuario) {
        this.id_bitacora = id_bitacora;
        this.operacion = operacion;
        this.fecha_operacion = fecha_operacion;
        this.usuario = usuario;
    }

    public int getId_bitacora() {
        return id_bitacora;
    }

    public void setId_bitacora(int id_bitacora) {
        this.id_bitacora = id_bitacora;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Date getFecha_operacion() {
        return fecha_operacion;
    }

    public void setFecha_operacion(Date fecha_operacion) {
        this.fecha_operacion = fecha_operacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Bitacora{" + "id_bitacora=" + id_bitacora + ", operacion=" + operacion + ", fecha_operacion=" + fecha_operacion + ", usuario=" + usuario + '}';
    }

}
//Fin clase Bitacoras
